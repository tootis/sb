$(document).ready(function () {
    
    var hoverHTMLDemoAjax = '<p>Loading ... <p>';
    $(".hover-trigger").hovercard({
        detailsHTML: hoverHTMLDemoAjax,
        width: "300%",
        onHoverIn: function () {
        	var user_id = $(this).find(".hover-trigger").attr('user_id');
            $.ajax({
                url: '/users/get_user_info/'+user_id,
                type: 'GET',
                dataType: 'html',
                success: function (data) {
                	$(".hc-details").html(data);
                }
            });
        }
    });
});
