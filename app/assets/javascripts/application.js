// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.min
//= require rails.min
//= require jquery.atwho
//
// Core Plugins
// require theme/plugins/bootstrap/js/bootstrap.min
// require theme/plugins/js.cookie.min
// require theme/plugins/jquery-slimscroll/jquery.slimscroll.min
// require theme/plugins/jquery.blockui.min
// require theme/plugins/bootstrap-switch/js/bootstrap-switch.min
// require theme/plugins/jquery-ui.min
//
//= require jquery_ujs
// require autocomplete-rails

// Page Level Plugins
// require theme/plugins/morris/morris.min
// require theme/plugins/morris/raphael-min
// require theme/plugins/bootstrap-toastr/toastr
// require theme/pages/scripts/ui-modals.min
//
// Theme global scripts
// require theme/scripts/app.min
// require theme/pages/scripts/ui-toastr
//
// Page Sctrips
//= require theme/pages/scripts/dashboard.min
//
// Layout Scripts
// require theme/layout4/scripts/layout.min
// require theme/layout4/scripts/demo.min
// require theme/scripts/quick-sidebar.min
//
// require notifyjs_rails
// ActionCable
//= require cable
//= stub landing
//= require_tree .
//= require_self

$(document).ready(function(){
    $(".titleinput").focus(function(){
        $(".newpost-body-rest").css("display", "block");
        $(".closebtn").css("display", "inline-block");
        $(".newpost").addClass("newpostSyler");

    });
    $(".closebtn").click(function(){
        $(".newpost-body-rest").css("display", "none");
        $(".closebtn").css("display", "none");
        $(".newpost").removeClass("newpostSyler");
    });
    var AccountMenu = false;
    var NotifMenu = false;
    $(".tab1").click(function(){
      if(NotifMenu){
        $(".NotifMenu").toggle( "fast", function() {});
        $(".AccountMenu").toggle( "fast", function() {});
        NotifMenu = false;
      }
      else {
        $(".page-container").toggle( "fast", function() {});
        $(".AccountMenu").toggle( "fast", function() {});
      }
      if(AccountMenu)
        AccountMenu = false;
      else
        AccountMenu = true;
      if(AccountMenu)
        $(".tab1").css("background-color", "#FFD121");
      else
        $(".tab1").css("background-color", "#E6E7E8");
      if(NotifMenu)
        $(".tab2").css("background-color", "#FFD121");
      else
        $(".tab2").css("background-color", "#E6E7E8");
    });
    $(".tab2").click(function(){
      if(AccountMenu){
        $(".AccountMenu").toggle( "fast", function() {});
        $(".NotifMenu").toggle( "fast", function() {});
        AccountMenu = false;
      }
      else{
        $(".page-container").toggle( "fast", function() {});
        $(".NotifMenu").toggle( "fast", function() {});
      }
      if(NotifMenu)
        NotifMenu = false;
      else
        NotifMenu = true;
      if(AccountMenu)
        $(".tab1").css("background-color", "#FFD121");
      else
        $(".tab1").css("background-color", "#F1F2F2");
      if(NotifMenu)
        $(".tab2").css("background-color", "#FFD121");
      else
        $(".tab2").css("background-color", "#E6E7E8");
    });

});
