jQuery(function() {
  $('select#post_type').change(function() {
    var selected;
    selected = $('#post_type option').filter(':selected').text();
    if (selected === 'knowledge') {
      return $('#post_specialty_group').addClass('hidden');
    } else {
      return $('#post_specialty_group').removeClass('hidden');
    }
  });


  return $(document).ready(function() {
    $('[data-toggle="popover"]').popover();
    $('#tags-input').tagsInput({
      'height': '50px',
      'width': '100%',
      'minChars': 3,
      'delimiter': [',', ';', ' '],
      'placeholderColor': '#999999'
    });
    return $('.tagsinput').addClass('form-control margin-top-10');
  });
});

