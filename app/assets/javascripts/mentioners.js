$(document).ready(function(){


research_team = $('.research_team');
rtId= research_team.length ? research_team.attr('id'):"";
url = '/users/get_mentionees/'+rtId;

var at_config = {
    at: '@',
    data: url,
    limit: 4,
 
}

CKEDITOR.on('instanceReady', function(event) {
		
	var editor = event.editor;
	
		
	// Switching from and to source mode
	editor.on('mode', function(e) {
		load_atwho(this, at_config);
	});
	
	// First load
	load_atwho(editor, at_config);
		
});

function load_atwho(editor, at_config) {
	
	// WYSIWYG mode when switching from source mode
	if (editor.mode != 'source') {

    	editor.document.getBody().$.contentEditable = true;
    	
		$(editor.document.getBody().$)
			.atwho('setIframe', editor.window.getFrame().$)
			.atwho(at_config);
			
	}
	// Source mode when switching from WYSIWYG
	else {
		$(editor.container.$).find(".cke_source").atwho(at_config);
	}
}
	


    $('.comment-content-area').focus(function() {
  	$(this).atwho(at_config);
});




});