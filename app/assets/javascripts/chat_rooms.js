$(document).ready(function(){

  $('#new_chat_room_button').click (function(event)
  {
    if(user_num_friends < min_allowed_friends)
    {
      var s = "You must have at least " + min_allowed_friends +" to create a chat room ." + " You Only Have " + user_num_friends + " . ";
      if($('.notes').length == 0)
      {
        $('#notices').append("<div class ='notes'>" + s + "</div>");
      }
     return false;
    }
  });

  $('#new_chat_room').submit(function(event) {

      if($(".notes"))
        $(".notes").remove();

    var numFriends = $(":checkbox:checked").length;


    // min_allowed_friends is a parameter set in views/chat_rooms/new.html.erb
    if(numFriends < min_allowed_friends) 
    {
      var s = "You Can't have less than " + min_allowed_friends +" friends in the chat room. You only checked " + numFriends + "."
      $('#new_chat_room').append("<div class ='notes'>" + s + "</div>");
      return false;
    }

    if ($.trim($("#chat_room_title").val()) === "")
    {
      var s = "Title Can't Be Blank."
      $('#new_chat_room').append("<div class = 'notes'>" + s + "</div>");
      return false;
    }

  });
});
