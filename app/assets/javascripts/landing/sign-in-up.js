// Toggle Function
$(document).ready(function() {
  $('.cta').click(function(){
    // Switches the Icon
    // Switches the forms
    $('.SignToggle').html($('.SignToggle').html() == 'Log in' ? 'Sign up' : 'Log in');
    $('.form').animate({
      height: "toggle",
      'padding-top': 'toggle',
      'padding-bottom': 'toggle',
      opacity: "toggle"
    }, "slow");
  });
});
