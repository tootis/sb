ActiveAdmin.register User do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  permit_params :specialty_id

  index do
    selectable_column
    id_column
    column :email
    column :created_at
    column :field do |u|
      if u.specialty.present?
        u.specialty.field.name
      else
        ""
      end
    end
    column :specialty
    column :country
    column :confirmed_at
    actions
  end

  filter :email
  filter :username
  filter :country
  filter :specialty

end
