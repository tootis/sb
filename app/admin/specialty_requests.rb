ActiveAdmin.register SpecialtyRequest do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  actions :index

  index do
    column :user do |sr|
      link_to sr.user.fullname, admin_user_path(sr.user)
    end

    column :field_name

    column :specialty_name

    column :status

    actions do |sr|
      item "Accept", accept_admin_specialty_request_path(sr), method: :post
      text_node " "
      item "Reject", reject_admin_specialty_request_path(sr), method: :post
    end
  end

  filter :status, as: :select, collection: SpecialtyRequest.statuses

  member_action :accept, method: :post do
    resource.accept
    redirect_to admin_specialty_requests_path
  end

  member_action :reject, method: :post do
    resource.reject
    redirect_to admin_specialty_requests_path
  end
end
