class CommentsController < ApplicationController
  include MentionersModule

  before_action :authenticate_user!
  before_action :set_comment, only: [:toggle_downvote, :toggle_upvote]
  before_action :set_post, only: [:paginate, :create]

  def create
    @comment = Comment.new(
      content: params[:comment][:content],
      post: @post,
      user: current_user
    )

    handle_mentions(@comment, @comment.post.research_team) # in MentionersModule
    @comment.save
  end

  def paginate
    page_number = params[:page]
    @comments = @post.comments.paginate(page: page_number).order(created_at: :desc).reverse
  end

  def destroy
  end

  def toggle_upvote
    if current_user.liked? @comment
      @comment.unliked_by current_user
    else
      @comment.upvote_by current_user
      #TODO  : Add localization
      NotificationSingleBroadcastJob.perform_later("upvoted your comment ", post_path(@comment.post.id), @comment.user, current_user, Time.now.to_s)
    end

    render 'vote_toggled'
  end

  def toggle_downvote
    if current_user.disliked? @comment
      @comment.undisliked_by current_user
    else
      if current_user.liked? @comment
        @comment.unliked_by current_user
      end

      @comment.disliked_by current_user
      #TODO  : Add localization
      NotificationSingleBroadcastJob.perform_later("downvoted your comment", post_path(@comment.post.id), @comment.user, current_user, Time.now.to_s)
    end

    render 'vote_toggled'
  end
  private

  def set_post
    @post = Post.find(params[:post_id])
  end

  def set_comment
    @comment = Comment.find(params[:id])
  end
end
