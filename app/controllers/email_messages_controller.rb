class EmailMessagesController < ApplicationController

  def create
    @email_message = current_user.sent_emails.new(email_message_params)
    @email_message.save
    SendEmailJob.perform_later(@email_message)
  end

private

def email_message_params
    params.require(:email_message).permit(:content, :title, :receiver_id)
end

end