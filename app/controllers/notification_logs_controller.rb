class NotificationLogsController < ApplicationController

before_action :authenticate_user!

def show 
	if(NotificationLog.find_by(id: params[:id]) == current_user.notification_log)
		@notifications = current_user.notification_log.notifications.order(created_at: :desc)
	else 
		render('notification_logs/error')	
	end	

end	

def clear_unread_count
	current_user.notification_log.update_attribute(:unread_count, 0)
	render :nothing => true, :status => 200, :content_type => 'text/html'
end

end
