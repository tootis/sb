module Votable
  extend ActiveSupport::Concern

  included do
    before_action :set_votable, only: [:toggle_upvote, :toggle_downvote]
  end

  def toggle_upvote
    if current_user.liked? @votable
      @votable.unliked_by current_user
    else
      @votable.upvote_by current_user
      #TODO  : Add localization
      #NotificationSingleBroadcastJob.perform_later("upvoted your post", post_path(@post.id), @post.author, current_user, Time.now.to_s)
    end

    render 'api/v1/shared/vote_toggled'
  end

  def toggle_downvote
    if current_user.disliked? @votable
      @votable.undisliked_by current_user
    else
      if current_user.liked? @votable
        @votable.unliked_by current_user
      end
      @votable.disliked_by current_user
      #TODO  : Add localization
      #NotificationSingleBroadcastJob.perform_later("downvoted your post", post_path(@post.id), @post.author, current_user, Time.now.to_s)
    end

    render 'api/v1/shared/vote_toggled'
  end

  private

  def set_votable
    @votable = controller_name.classify.constantize.find(params[:id])
  end
end
