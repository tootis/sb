module Authorable
  extend ActiveSupport::Concern

  included do
    before_action :set_authorable, only: [:update, :destroy]
    before_action :check_ownership, only: [:update, :destroy]
  end

  def update
    if @authorable.update(self.send("#{controller_name.singularize}_params"))
      render :update
    else
      render json: @authorable.errors.full_messages
    end
  end

  def destroy
    @authorable.deleted = true

    if @authorable.save
      head :ok
    else
      head :not_modified
    end
  end

  private

  def set_authorable
    @authorable = controller_name.classify.constantize.find(params[:id])
  end

  def check_ownership
    head :unauthorized if @authorable.author != current_user
  end
end
