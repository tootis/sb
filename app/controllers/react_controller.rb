class ReactController < ActionController::Base
  def bootstrapper
    render template: 'layouts/bootstrapper'
  end
end
