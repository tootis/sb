class ChatRoomsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_cache_headers

  def index
    @chat_rooms = current_user.chat_rooms.order(updated_at: :desc)
    @min_allowed_chat_participants = min_allowed_chat_participants
  end

  def new
    @chat_room = ChatRoom.new
    @friends = current_user.friends
    @min_allowed_chat_participants = min_allowed_chat_participants

    if(@friends.size < @min_allowed_chat_participants - 1)
      render partial: 'not_enough_participants', locals:{min_allowed_chat_participants: @min_allowed_chat_participants}
    end

  end	

  def create
    if(! params[:friends_to_add] || params[:friends_to_add][:ids].size < min_allowed_chat_participants - 1	)
      render partial: 'not_enough_participants', locals:{min_allowed_chat_participants: @min_allowed_chat_participants}
    else
      @chat_room = current_user.created_chat_rooms.build(chat_room_params)
      if ( @chat_room.save )
        flash[:success] = "Chat Room #{@chat_room.title} successfully created !"

        User.where(id: params[:friends_to_add][:ids]).each do|user|
          user.chat_rooms << @chat_room
        end

        NotificationMultipleBroadcastJob.perform_later(@chat_room.id,
                                                       "#C", params[:friends_to_add][:ids], current_user, Time.now.to_s)

        redirect_to(action: :show, id: @chat_room.id)
      else
        render 'new'
      end
    end
  end

  def show
    if(! ChatRoomActiveMembership.where(user_id: current_user.id, chat_room_id: params[:id]).exists?)
      current_user.currently_active_chat_rooms << ChatRoom.find_by(id: params[:id])
    end
    @chat_room = ChatRoom.includes(:messages).find_by(id: params[:id])

    if current_user.has_unseen_messages_in?(ChatRoom.find_by(id: params[:id]))
      new_unseen_chat_rooms = current_user.unseen_chat_rooms -1
      current_user.update_attribute(:unseen_chat_rooms, new_unseen_chat_rooms)
    end
    current_user.update_last_seen(@chat_room.id)
    @message   = Message.new
  end

  def show_friends
    @chat_room = ChatRoom.find_by(id: params[:id])
    @friends =[]
    current_user.friends.each do |f|
      if(! f.chat_rooms.include? @chat_room)
        puts f.fullname
        @friends << f
      end
    end
  end

  def add_participants
    if(params[:friends_to_add])

      User.where(id: params[:friends_to_add][:ids]).each do|user|
        user.chat_rooms << ChatRoom.where(id: params[:chat_room_id])
      end

      NotificationMultipleBroadcastJob.perform_later(@chat_room.id,
                                                     "#C", params[:friends_to_add][:ids], current_user, Time.now.to_s)
    end
    redirect_to(action: :show, id: params[:chat_room_id])
  end

  def render_new_room
    chat_room = ChatRoom.find(params['chat_room_id'])
    new_room = render_to_string :partial => "chat_rooms/chat_room", :locals => {:chat_room => chat_room}
    data = {:message => new_room}
    puts data["message"]
    puts "sad\nsad\nsad\nsad\nsad\nsad\nsad\nsad\nsad\n"
    render :json => data, :status => 200
  end

  private

  def chat_room_params
    params.require(:chat_room).permit(:title)
  end

  def min_allowed_chat_participants
    4
  end

  def set_cache_headers
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end
end
