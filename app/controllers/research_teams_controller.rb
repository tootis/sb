class ResearchTeamsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_research_team, except: [:new, :create, :index]
  before_action :check_admin, only: [:destroy, :edit, :accept_member, :show_requesting_members, :invite_friend, :accept_member, :cancel_invitation, :remove_member]
  before_action :check_not_soft_deleted, except: [:new, :create, :index]
  before_action :check_member, except: [:new, :create, :index, :request_membership]


  def new
    @no_title = true
    @rt = ResearchTeam.new
  end

  def index
    sql1 = "select research_team_id from research_team_memberships where research_team_memberships.user_id = #{current_user.id}"
    sql2 = "select Distinct research_teams.* from research_teams,research_team_memberships where research_teams.privacy = 0 AND research_teams.soft_deleted = false AND research_teams.id NOT IN (#{sql1})"
    @non_joined_nor_requested_nor_invited_teams = ActiveRecord::Base.connection.exec_query(sql2)

    sql3 = "select Distinct research_teams.* ,research_team_memberships.status from research_teams,research_team_memberships
      where research_teams.privacy = 0 AND research_teams.soft_deleted = false AND research_team_memberships.user_id = #{current_user.id} AND research_team_memberships.research_team_id=research_teams.id"
    @joined_or_requested_or_invited_teams = ActiveRecord::Base.connection.exec_query(sql3)
    @current_user = current_user

    respond_to do |format|
      format.js { render :js => "window.location.href = '#{research_teams_path}'" }
      format.html
    end


  end

  def create
    @rt = ResearchTeam.new(research_team_params)
    @rt.creator = current_user
    @rt.specialty = current_user.specialty

    if @rt.save
      invitees_ids = params[:invitees_ids]
      invite_multiple_friends_method(invitees_ids)
      redirect_to @rt
    else
      render :new
    end
  end

  def edit
  end

  def show
    @title = @rt.name
    if(@limited)
      @membership = ResearchTeamMembership.where(user_id: current_user.id, research_team_id: @rt.id).first
    end
    respond_to do |format|
      format.js { render :js => " window.location.href = '#{research_team_path(@rt.id)}' "}
      format.html 
    end
  end

  def destroy
   rt =  ResearchTeam.find(params[:id])
   rt.update_attribute(:soft_deleted, true)
   rt.posts.each do|post|
    post.update_attribute(:deleted, true)
   end
    redirect_to root_path
  end

  def available_friends
    @friends = current_user.friends - @rt.members
    render layout: false
  end

  def team_members
    @memberships = @rt.research_team_memberships
    @current_user  = current_user
    render layout: false
  end

  def invite_friend
    friend_id = params[:friend_id]
    friend = User.find(friend_id)
    @name = friend.fullname
    if invite_friend_helper(friend_id)

      if(params[:from_auto_complete] == "true")
        render js: "{$('#feedback_div').html('#{@name} Successfully Invited !');$('#friend_invite_autocomplete')[0].value = '';};"
      else
        render js: "{ $('#rt-add-friend-#{friend_id}').fadeOut().remove() ; $('#rt-members-count').text(#{@rt.enrolled_members.count});}"
      end

    elsif @rt.invited_members.include? friend and params[:from_auto_complete] == "true"

      render js: "{$('#feedback_div').html('#{@name} is already invited to this team !');$('#friend_invite_autocomplete')[0].value = '';};"

    elsif @rt.enrolled_members.include? friend and params[:from_auto_complete] == "true"

      render js: "{$('#feedback_div').html('#{@name} is already a member of this team !');$('#friend_invite_autocomplete')[0].value = '';};"

    elsif @rt.requesting_members.include? friend and params[:from_auto_complete] == "true"

      team_membership = ResearchTeamMembership.where(user_id: friend_id, research_team_id: @rt.id).first
      if !team_membership.enrolled?
          team_membership.enrolled!
      end
      render js: "{$('#feedback_div').html('#{@name} was accepted as a member in this team. !');$('#friend_invite_autocomplete')[0].value = '';};"

    else
      render js: "{;}"
    end
  end

  def accept_member
    if User.exists? params[:requesting_member_id]
        @memberId = params[:requesting_member_id]
        @research_team_id = @rt.id
        team_membership = ResearchTeamMembership.where(user_id: params[:requesting_member_id], research_team_id: @rt.id).first
        if !team_membership.enrolled?
          team_membership.enrolled!
        end
        render js: "{$('#rt-#{@rt.id}-view-member-#{@memberId}').fadeOut().remove();  $('#rt-members-count').text(#{@rt.enrolled_members.count});}"
   else 
    render js: "{;}"
   end
 end

  def cancel_join_request
    rtm = ResearchTeamMembership.where(research_team_id: @rt.id, user_id: current_user.id).first
    if rtm && rtm.requested?
      rtm.destroy
      @request_membership_research_team_path = request_membership_research_team_path(@rt.id)
      @user_id = current_user.id
      @research_team_id = @rt.id
    else
      render js: "{;}"
    end
  end

  def accept_invitation
    rtm = ResearchTeamMembership.where(research_team_id: @rt.id, user_id: current_user.id).first
    if rtm && rtm.invited?
       rtm.enrolled!
       @research_team_path = research_team_path(@rt.id)
       @user_id = current_user.id
       @research_team_id = @rt.id
    else
      render js: "{;}"
    end
  end

  def cancel_invitation
    rtm = ResearchTeamMembership.where(research_team_id: @rt.id, user_id: params[:invited_member_id]).first
    if rtm && rtm.invited?
       rtm.destroy
       @research_team_path = research_team_path(@rt.id)
       @user_id = params[:invited_member_id]
       @research_team_id = @rt.id
       render js: "{$('#rt-#{@rt.id}-member-#{@user_id}-button').fadeOut().remove();}"
    else
      render js: "{;}"
    end
  end

  def remove_member
    member_to_remove = User.find(params[:enrolled_member_id])

    if member_to_remove
      if  @rt.members.include? member_to_remove
        ResearchTeamMembership.where(user_id: member_to_remove.id, research_team_id: @rt.id).first.destroy
        render js: "{$('#rt-#{@rt.id}-view-member-#{member_to_remove.id}').fadeOut().remove();  $('#rt-members-count').text(#{@rt.enrolled_members.count});}"
      else
        render js: "{;}"
      end
    else 
      render js: "{;}"  
    end


  end


  def request_membership
    rtm = ResearchTeamMembership.where(research_team_id: @rt.id, user_id: current_user.id).first
    if rtm
      render js: "{;}"
    else
      rtm = ResearchTeamMembership.new
      rtm.user_id = current_user.id
      rtm.research_team_id = @rt.id
      rtm.requested!
      rtm.save # not necessary as rtm.requested! will automatically save
      @cancel_request_research_team_path = cancel_join_request_research_team_path(@rt.id)
      @user_id = current_user.id
      @research_team_id = @rt.id
      NotificationMultipleBroadcastJob.perform_later("Asked to join research team #{@rt.name} that you manage.",
      show_requesting_members_research_team_path(@rt.id), @rt.admins.pluck(:id), current_user, Time.now.to_s)
    end
  end  

  def show_requesting_members
    requester_ids = ResearchTeamMembership.where(status: :requested, research_team_id: @rt.id).pluck(:user_id)
    @requesters = User.where(id: requester_ids)
    @rt_id = @rt.id
  end

  private

  def set_research_team
    @rt = ResearchTeam.find(params[:id])
  end

  def check_admin
    unless @rt.admins.include? current_user
      flash[:notice] = "You are not allowed to perfrm this operation."
      redirect_to root_path
    end
  end


  def check_member
    if ( (@rt.members.exclude? current_user) && @rt.secret?)
      flash[:notice] = "Sorry, You are not allowed to perform this action."
      redirect_to request.referer
    elsif @rt.enrolled_members.include? current_user
      @limited = false
    else
      @limited = true
    end
  end

  def research_team_params
    params.require(:research_team).permit(:name, :description)
  end

  def check_not_soft_deleted
    if(@rt.soft_deleted)
      redirect_to root_path
    end
  end

  def invite_friend_helper(friend_id)
    if User.exists? friend_id
      friend = User.find(friend_id)
      if current_user.friends.include?(friend) && @rt.members.exclude?(friend)
        @rt.invited_members << friend
        NotificationSingleBroadcastJob.perform_later("invited you to join his research team : #{@rt.name}", research_team_path(@rt.id), friend, current_user, Time.now.to_s)      
      else
        return false
      end
    else 
      return false
    end

    return true
  end



  def invite_multiple_friends_method(invitees_ids)
    if(invitees_ids != nil)
        invitees_ids.uniq.each do |friend_id|
          invite_friend_helper(friend_id)
      end
    end
  end

end
