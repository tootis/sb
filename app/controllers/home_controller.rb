class HomeController  < ApplicationController
  before_action :authenticate_user!
  before_action :check_profile_completeness

  def index
      case params[:beehive]
      when 'local'
        @title = 'Local Beehive'
          @posts = Post.joins(:author).where(
          users: { country: current_user.country }
        )
      when 'private'
        @title = 'Private Beehive'
        @posts = Post.where(
          author: current_user.friends << current_user
        )
      when 'specialty'
        @title = 'Specialty Beehive'
        @posts = Post.where(
          specialty: current_user.specialty
        )
      when 'search'
        @title = 'Custom Search'
        tags_list = params[:tags].split(/[\s,]/)

        @posts = Post.joins(:tags).where(
          specialty_id: params[:specialty_id],
          tags: { name: tags_list }
        )
      else # Global
        @title = 'Global Beehive'
        @posts = Post.all
      end
      page_number = params[:page] || 1

      @posts = @posts.where(
        deleted: false
      ).order(created_at: :desc).paginate(page: page_number)
  end
end
