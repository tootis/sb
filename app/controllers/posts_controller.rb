class PostsController < ApplicationController
  include MentionersModule
  before_action :authenticate_user!
  before_action :set_post, only: [:show, :toggle_upvote, :toggle_downvote]

  def create
    rt = nil
    if post_params[:research_team_id].present?
      rt = ResearchTeam.find_by_id(post_params[:research_team_id])
      redirect_to root_path, error: 'Not Allowed' if
        rt.nil? || !rt.members.include?(current_user)

      post_params.delete post_params[:research_team_id]
    end

    @post = current_user.posts.new(post_params)
    @post.research_team = rt
    handle_mentions(@post, rt) # in MentionersModule
    @post.save
  end

  def show
    if(@post == nil || @post.deleted)
      flash[:error] = "Sorry, The post you requested cannot be found"
      redirect_to root_path
    end
  end

  def toggle_upvote
    if current_user.liked? @post
      @post.unliked_by current_user
    else
      @post.upvote_by current_user
      #TODO  : Add localization
      NotificationSingleBroadcastJob.perform_later("upvoted your post", post_path(@post.id), @post.author, current_user, Time.now.to_s)

    end

    render 'vote_toggled'
  end

  def toggle_downvote
    if current_user.disliked? @post
      @post.undisliked_by current_user
    else
      if current_user.liked? @post
        @post.unliked_by current_user
      end
      @post.disliked_by current_user
      #TODO  : Add localization
      NotificationSingleBroadcastJob.perform_later("downvoted your post", post_path(@post.id), @post.author, current_user, Time.now.to_s)

    end

    render 'vote_toggled'
  end

  private

  def set_post
    @post = Post.find_by(id:params[:id])
  end

  def post_params
    params.require(:post).permit(:content, :title, :type, :specialty_id,
                                 :tags_string, :research_team_id)
  end
end
