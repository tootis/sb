class UsersController < ApplicationController
  include ActionView::Helpers::UrlHelper

  #auto complete
  before_action :set_user, except: [:relationships, :index, :active_chat_rooms,
                                    :remove_inactive_chat_room, :get_mentionees,:autocomplete_user_username, :update_last_seen, :increment_no_unseen_rooms]

  autocomplete :user, :username,
    full: true, limit: 20, extra_data:[:first_name, :last_name],
    display_value: :search_display_form

  def get_autocomplete_items(parameters)
    items = active_record_get_autocomplete_items(parameters)

    if(params[:scope]=="friends_only")
      items = items.where("users.id IN (select friendships.user_id from friendships where friendships.friend_id = #{current_user.id}) OR users.id IN (select friendships.friend_id  from friendships where friendships.user_id = #{current_user.id})")
    end

    items
  end

  def index
    @members = User.all.where.not(id: current_user.id)
    respond_to do |format|
      format.html {}
    end
  end

  def show
    @no_title = true
    @posts = @user.posts
  end

  %i(add_friend unfriend cancel_friend_request
     accept_friend_request).each do |m|
     define_method m do
       if current_user.send(m, @user)
         render :friendship_action
       else
         render nothing: true, status: 304
       end
     end
   end

  %i(follow unfollow).each do |m|
    define_method m do
      if current_user.send(m, @user)
        render :follow_action
      else
        render nothing: true, status: 304
      end
    end
  end

  def relationships
    @title = 'Relationships'
    @friends = current_user.friends
    # render partial: "relationships", locals:{people: people}
    #@followers = current_user.followers.pluck(:id, :name, :specialty, :country)
    #@followers = current_user.followees.pluck(:id, :name, :specialty, :country)
  end

  def active_chat_rooms
    rooms = current_user.currently_active_chat_rooms.pluck(:id)
    data = {:message => rooms}
    render :json => data, :status => :ok
  end

  def remove_inactive_chat_room
    current_user.update_last_seen(params['chat_room_id'])
    membership = ChatRoomActiveMembership.find_by(chat_room_id: params['chat_room_id'], user_id: current_user.id)
    if membership
      membership.destroy
      data = {:message => "Successfully deactivated room #{params['chat_room_id']} for user #{current_user.id}"}
      render :json => data, :status => :ok
    else
      data = {:message => "Error"}
      render :json => data, :status => :ok
    end
  end

  def increment_no_unseen_rooms
    if ! current_user.has_unseen_messages_in?(ChatRoom.find(params[:chat_room_id]))
      current_user.update_attribute(:unseen_chat_rooms , 1 + current_user.unseen_chat_rooms )
      data = {:increment_no => "true"}
    elsif  current_user.has_unseen_messages_in?(ChatRoom.find(params[:chat_room_id]))
      data = {:increment_no => "false"}
    end

    render :json => data, :status => :ok
  end

  def get_mentionees
    if(params[:id])
      @membersUsernames = ResearchTeam.find(params[:id]).members.pluck(:username)
      @membersUsernames.delete(current_user.username)
    else
      @membersUsernames = current_user.friends.pluck(:username)
    end
    render json: @membersUsernames
  end

  def get_user_info
    render partial: "get_user_simplified_info", locals: {user: @user}
  end

  def request_specialty
    SpecialtyRequest.create(user: current_user, field_name: params[:field], specialty_name: params[:specialty])
  end

  private

  def set_user
    @user = User.find(params[:id])
  end
end
