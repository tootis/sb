class LandingController < ApplicationController
  layout 'landing'

  def index
    redirect_to controller: :home if user_signed_in?
  end
end
