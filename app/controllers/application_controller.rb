class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception, unless: :devise_token_auth_controller?
  before_action :configure_permitted_parameters, if: :devise_controller?
  layout :layout_by_session
  before_action :set_cache_headers
  add_flash_types :success, :info, :warning, :error

  protected

  def check_profile_completeness
    return if current_user.sb_member?
    unless current_user && current_user.country? && current_user.specialty
      redirect_to edit_user_registration_path, warning: 'Please complete your profile'
    end
  end

  private

  def devise_token_auth_controller?
    params[:controller].split("/")[0] == "devise_token_auth"
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(
      :sign_up,
      keys: [
        :email,
        :password,
        :password_confirmation,
        :first_name,
        :last_name,
        :username
      ]
    )

    devise_parameter_sanitizer.permit(
      :account_update,
      keys: [
        :first_name,
        :last_name,
        :username,
        :country,
        :specialty_id,
        :description,
        :title
      ]
    )
  end

  def layout_by_session
    if user_signed_in?
      'application'
    else
      'landing'
    end
  end


  def set_cache_headers
    response.headers["Cache-Control"] = "no-cache, no-store"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

end
