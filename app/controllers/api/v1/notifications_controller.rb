class Api::V1::NotificationsController < Api::BaseController
  before_action :authenticate_user!

  def index
    @notifications = PublicActivity::Activity.where(
      recipient: current_user
    ).order(created_at: :desc).page(params[:page] || 1)
  end
end
