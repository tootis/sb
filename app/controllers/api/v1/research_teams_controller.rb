class Api::V1::ResearchTeamsController < Api::BaseController
  before_action :authenticate_user!
  before_action :set_research_team, except: [:create, :index, :search]
  before_action :check_adminstration, only: [:update, :destroy, :remove_member]
  before_action :check_membership, only: [:add_member, :leave]

  def index
    @research_teams = current_user.research_teams.paginate(page: params[:page] || 1)
  end

  def search
    @research_teams = ResearchTeam.search(
      name_cont: params[:q]
    ).result(distict: true).paginate(page: params[:page] || 1)

    render "index"
  end

  def show
  end

  def create
    @research_team = current_user.owned_research_teams.new(research_team_params)

    unless @research_team.save
      render json: { "errors": @research_team.errors.messages }
    end
  end

  def update
    unless @research_team.update(research_team_params)
      render json: { "errors": @research_team.errors.messages }
    end
  end

  def destroy
    @research_team.deleted = true

    unless @research_team.save
      render json: { "errors": @research_team.errors.messages }
    end
  end

  def posts
    @posts = @research_team.posts.paginate(page: params[:page] || 1)
  end

  def list_members
    if params.has_key? :awaiting_members
      @members = @research_team.requesting_members
    else
      @members = @research_team.enrolled_members
    end
    @members.paginate(page: params[:page])
  end

  def add_member
    @user = User.find(params[:user_id])

    if @research_team.enrolled_members.include?(@user) || !current_user.friends.include?(@user)
      head :not_modified
    else
      if @research_team.requesting_members.include?(@user)
        @research_team.requesting_members.delete(@user)
      end
      head :ok if @research_team.enrolled_members << @user
    end
  end

  def join
    unless @research_team.members.include? current_user
      @research_team.requesting_members << current_user
      head :ok
    else
      head :not_modified
    end
  end

  def leave
    if (@research_team.admins - [current_user]).empty?
      render json: { "error": "Admins can't be empty" }, status: :not_modified
    else
      head :ok if @research_team.enrolled_members.delete(current_user)
    end
  end

  def remove_member
    user = User.find_by(id: params[:user_id])
    head :not_modified if user.nil?
    if (@research_team.admins - [user]).empty?
      render json: { "error": "Admins can't be empty" }, status: :not_modified
    else
      head :ok if @research_team.enrolled_members.delete(user)
    end
  end

  private

  def research_team_params
    params.require(:research_team).permit(:specialty_id, :name, :description)
  end

  def set_research_team
    @research_team = ResearchTeam.where(id: params[:id], deleted: false).first
    head :not_found if @research_team.nil?
  end

  def check_adminstration
    head :unauthorized unless @research_team.admins.include? current_user
  end

  def check_membership
    head :unauthorized unless @research_team.enrolled_members.include? current_user
  end
end
