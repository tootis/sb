class Api::V1::CountriesController < Api::BaseController
  def index
    render json: ISO3166::Country.all.map {|c| {value: c.alpha2, name: c.name}}
  end
end
