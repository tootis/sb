class Api::V1::SpecialtyRequestsController < Api::BaseController
  before_action :authenticate_user!

  def create
    sr = SpecialtyRequest.new(specialty_request_params)
    sr.user = current_user
    if sr.save
      head :ok
    else
      head :not_acceptable
    end
  end

  private

  def specialty_request_params
    params.require(:specialty_request).permit(:field_name, :specialty_name)
  end
end
