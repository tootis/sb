class Api::V1::PostsController < Api::BaseController
  include Authorable
  include Votable

  before_action :authenticate_user!
  before_action :paginate, only: [
    :index, :global_beehive, :local_beehive, :my_beehive, :specialty_beehive
  ]

  # TODO move this shit to users controller, or not?
  def index
    @user = User.find(params[:user_id])
    @posts = @posts.where(author: @user)
  end

  def global_beehive
    @posts = @posts.all
    render :index
  end

  def local_beehive
    @posts = @posts.joins(:author).where(
      users: { country: current_user.country }
    )
    render :index
  end

  def my_beehive
    @posts = @posts.where(
      author: current_user.friends << current_user
    )
    render :index
  end

  def specialty_beehive
    @posts = @posts.where(
      specialty: current_user.specialty
    )
    render :index
  end

  def show
    @post = Post.find(params[:id])
  end

  def create_post_for_research_team
    @post = instantiate_for_current_user
    @post.research_team_id = params[:research_team_id]

    unless @post.save
      render json: { "erros": @post.errors.full_messages }, status: :not_acceptable
      return
    end

    render :create
  end

  def create
    @post = instantiate_for_current_user
    unless @post.save
      render json: @post.errors.full_messages
    end
  end

  private

  def instantiate_for_current_user
    current_user.posts.new(post_params)
  end

  def paginate
    @posts = Post.where(research_team: nil).paginate(page: params[:page] || 1)
  end

  def post_params
    params.require(:post).permit(:content, :title, :type, :specialty_id,
                                 :tags_string, :research_team_id)
  end
end
