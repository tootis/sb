class Api::V1::UsersController < Api::BaseController
  before_action :authenticate_user!
  before_action :set_user, except: [:search]
  after_action :create_notification, only: [:add_friend, :accept_friend_request, :follow]

  def search
    friends = current_user.friends.pluck(:id)

    results = User.where.not(
      id: current_user.id
    ).search(
      first_name_or_last_name_cont: params[:q]
    ).result(
      distict: true
    ).paginate(page: params[:page] || 1).group_by { |u| friends.include? u.id }

    @friends = results[true]
    @others = results[false]
  end

  def show
  end

  %i(add_friend unfriend cancel_friend_request accept_friend_request).each do |m|
    define_method m do
      if current_user.send(m, @user)
        @success = true
        render :friendship_action
      else
        head :not_modified
      end
    end
  end

  %i(follow unfollow).each do |m|
    define_method m do
      if current_user.send(m, @user)
        @success = true
        render :follow_action
      else
        head :not_modified
      end
    end
  end

  private

  def create_notification
    return unless @success

    Api::V1::NotificationJob.perform_later(current_user, {
      key: "user.#{action_name}",
      owner: current_user,
      recipient: @user,
      parameters: {
        sender: current_user,
        path: api_v1_user_path(current_user),
        resource: { type: @user.class.name, id: current_user.id }
      }
    })
  end

  def set_user
    @user = User.find(params[:id])
  end
end
