class Api::V1::CommentsController < Api::BaseController
  before_action :authenticate_user!
  before_action :set_post, only: [:index, :create]
  after_action :create_notification, only: [:create]

  include MentionersModule
  include Votable
  include Authorable

  def index
    @comments = @post.comments.paginate(page: params[:page] || 1)
  end

  def create
    @comment = @post.comments.new(user: current_user, content: params[:comment][:content])
    if @comment.save
      handle_mentions(@comment, @comment.post.research_team) # in MentionersModule
    else
      render json: @comment.errors.full_messages, status: 422
    end
  end

  private

  def create_notification
    return if current_user == @comment.post.author

    Api::V1::NotificationJob.perform_later(@comment, {
      key: "comment.created",
      owner: current_user,
      recipient: @comment.post.author,
      parameters: {
        author_name: @comment.author.fullname,
        path: api_v1_post_path(@comment.post),
        resource: { type: @comment.post.class.name, id: @comment.post.id }
      }
    })
  end

  def comment_params
    params.require(:comment).permit(:content)
  end

  def set_post
    @post = Post.find(params[:post_id])
  end
end
