class Api::V1::FieldsController < Api::BaseController
  def index
    @fields = Field.includes(:specialties)
  end
end
