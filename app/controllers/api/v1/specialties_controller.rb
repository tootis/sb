class Api::V1::SpecialtiesController < Api::BaseController
  def index
    @specialties = Specialty.all
  end
end
