class Api::V1::ChatRoomsController < Api::BaseController
  before_action :authenticate_user!
  before_action :set_chat_room, except: [:index, :create]
  before_action :check_ownership, only: [:destroy]
  before_action :remove_chat_room_info_from_message_json, only: [:index, :show, :create, :update]

  def index
    @chat_rooms = current_user.chat_rooms.paginate(page: params[:page] || 1)
  end

  def create
    @chat_room = current_user.created_chat_rooms.new(chat_room_params)
    unless @chat_room.save
      render json: { "errors": @chat_room.errors.messages }, status: :not_acceptable
    end
  end

  def show
    @messages = @chat_room.messages.paginate(page: params[:page] || 1)
  end

  def update
    unless @chat_room.update(chat_room_update_params)
      render json: { "errors": @chat_room.errors.messages }, status: :not_acceptable
    end
    render "show"
  end

  def destroy
    head :ok if @chat_room.destroy
  end

  def add_participant
    user = User.find(params[:user_id])

    if current_user.friend_with? user
      @chat_room.add_participant user
      head :ok
    else
      render json: { "error": "Must be friend" }, status: :not_modified
    end
  end

  def remove_participant
    user = User.find(params[:user_id])

    if @chat_room.participants.include? user
      @chat_room.remove_participant user
      head :ok
    else
      render :not_modified
    end
  end

  def leave
    @chat_room.participants.delete(current_user)
    head :ok
  end

  private

  def set_chat_room
    @chat_room = current_user.chat_rooms.find(params[:id])
  end

  def chat_room_params
    params.require(:chat_room).permit(:title, participants_ids: [])
  end

  def chat_room_update_params
    params.require(:chat_room).permit(:title)
  end

  def check_ownership
    head :unauthorized unless @chat_room.creator == current_user
  end

  def remove_chat_room_info_from_message_json
    @dont_include_chat_room_in_message_json = true
  end
end
