class Api::BaseController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken

  respond_to :json
  rescue_from ActiveRecord::RecordNotFound, with: :render_404

  def render_404
    render json: { "error": "Record not found" }, status: :not_found
  end
end
