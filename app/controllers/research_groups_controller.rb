class ResearchGroupsController < ApplicationController

  before_action :authenticate_user!

  def search
   	stringToSearch = params[:search_string]
   	@groups = ResearchGroup.where("research_groups.name LIKE '%#{stringToSearch}%'")
   	render('research_groups/show')
  end

end
