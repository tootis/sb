module ApplicationHelper

  def fields
    Field.includes(:specialties).order(name: :asc).map do |f|
      [
        f.name.humanize,
        f.specialties.map do |s|
          [s.to_s, s.id]
        end
      ]
    end
  end
end
