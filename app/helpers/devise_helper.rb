module DeviseHelper
  def devise_error_messages!
    return {} unless devise_error_messages?
    resource.errors.messages.map { |k, v| [k, v.first] }.to_h
  end

  def devise_error_messages?
    !resource.errors.empty?
  end
end
