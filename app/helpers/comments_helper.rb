module CommentsHelper
  %w(up down).each do |dir|
    define_method "comment_thumbs_#{dir}_icon_class" do |comment|
      if current_user.send("voted_#{dir}_for?", comment)
        "fa fa-thumbs-#{dir}"
      else
        "fa fa-thumbs-o-#{dir}"
      end
    end
  end
end
