module UsersHelper

  def friend_btn_stuff(user)
    if current_user.friend_with?(user)
      [unfriend_user_path(user), 'Unfriend', 'fa fa-minus']
    elsif current_user.sent_friend_request_to?(user)
      [cancel_friend_request_user_path(user), 'Cancel Friend Request', 'fa fa-close']
    elsif current_user.received_friend_request_from?(user)
      [accept_friend_request_user_path(user), 'Accept Friend Request', 'fa fa-check']
    else
      [add_friend_user_path(user), 'Add Friend', 'fa fa-plus']
    end
  end

  def follow_btn_stuff(followee)
    if current_user.following?(followee)
      [unfollow_user_path(followee), 'Unfollow', 'fa fa-user']
    else
      [follow_user_path(followee), 'follow', 'fa fa-group']
    end
  end
end
