module PostsHelper
  def post_portlet_icon_class(post)
    if post.type == 'question'
      'icon-question'
    else
      'icon-paper-plane'
    end
  end

  %w(up down).each do |dir|
    define_method "post_thumbs_#{dir}_icon_class" do |post|
      if current_user.send("voted_#{dir}_for?", post)
        "fa fa-thumbs-#{dir}"
      else
        "fa fa-thumbs-o-#{dir}"
      end
    end
  end
end
