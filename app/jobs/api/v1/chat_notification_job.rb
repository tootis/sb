class Api::V1::ChatNotificationJob < ApplicationJob
  queue_as :default

  def perform(participants_ids, message_json)
    participants_ids.each do |id|
      ActionCable.server.broadcast(
        "chat_notifications_#{id}_channel",
        { message: message_json }
      )
    end
  end
end
