class Api::V1::NotificationJob < ApplicationJob
  queue_as :default

  def perform(instance, activity_params_hash)
    notification = instance.create_activity(activity_params_hash)
    puts "sending to recipient: #{notification.recipient} with id #{notification.recipient_id}"
    puts render_notification(notification)
    ActionCable.server.broadcast(
      "notifications_#{notification.recipient_id}_channel",
      { notification: render_notification(notification) }
    )
  end

  private

  def render_notification(notification)
    Api::V1::NotificationsController.render(
      partial: 'notification',
      locals: { notification: notification }
    )
  end
end
