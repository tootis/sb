class MessageBroadcastJob < ApplicationJob
  queue_as :default

 after_perform do |job|
		job.arguments.first.chat_room.touch
end

  def perform(message)
   ActionCable.server.broadcast "chat_rooms_#{message.chat_room.id}_channel",
                                 message: render_message(message), chat_room_id: message.chat_room_id

   receivers_ids = ChatRoomMembership.where(chat_room_id: message.chat_room.id).pluck(:user_id)
   NotificationMultipleBroadcastJob.perform_later(message.chat_room_id,
    "#M", receivers_ids, message.user, Time.now.to_s)
  end

  private

  def render_message(message)
    MessagesController.render partial: 'messages/message', locals: {message: message}
  end
end