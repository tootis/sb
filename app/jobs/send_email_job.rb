class SendEmailJob < ApplicationJob
  queue_as :default

 after_perform do |job|
    email = job.arguments.first
    NotificationSingleBroadcastJob.perform_later("Sent you an email",   Rails.application.routes.url_helpers.user_path(email.sender.id), email.receiver, email.sender, Time.now.to_s)
    NotificationSingleBroadcastJob.perform_later("was successfully sent your mail", Rails.application.routes.url_helpers.user_path(email.receiver.id), email.sender, email.receiver, Time.now.to_s)
end

def perform(email)
    SendMessageMailer.message_mail(email).deliver
end

end