class NotificationSingleBroadcastJob < ApplicationJob
  queue_as :default

  def perform(message, url, receiver, source, notification_time)
    if(receiver == source)
      return
    end

    notification_time_parsed = Time.parse(notification_time)
    if(url != "#M" && url != "#C")
      notification = Notification.new({content: "#{source.fullname} #{message}",
                                       url: url, 
                                       created_at: notification_time_parsed})

      receiver.notification_log.notifications << notification

      ActionCable.server.broadcast "notifications_#{receiver.notification_log.id}_channel",
        {notification: render_notification(notification) , 
         unread: receiver.notification_log.unread_count}

    elsif (url == "#M" || url == "#C")
      # here the actual message sent is the chat room id
      notification = Notification.new({content: "#",
                                       url: url,
                                       created_at: notification_time_parsed})
      ActionCable.server.broadcast(
        "notifications_#{receiver.notification_log.id}_channel",
        {
          notification: render_notification(notification),
          chat_room_id: message
        }
      )
    end
  end

  private

  def render_notification(notification)
    NotificationsController.render partial: 'notifications/notification', locals: {notification: notification}
  end
end
