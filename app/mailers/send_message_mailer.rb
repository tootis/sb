class SendMessageMailer < ApplicationMailer
	default from: "science.beehive.test@gmail.com"

 def message_mail(email_message)
 	@email_message = email_message
    mail(to: email_message.receiver.email, subject: email_message.title)
  end
end
