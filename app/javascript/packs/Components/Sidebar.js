import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import ReactTooltip from 'react-tooltip'
import axios from 'axios';

import './../Assets/css/Components/Sidebar.css';
import UserHoverCard from './UserHoverCard'

class Sidebar extends Component {
  constructor(){
    super();
    this.state = {
      sidebarOpened : false,
      width: window.innerWidth
    }
    this.toggleSidebar = this.toggleSidebar.bind(this);
    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);
  }
  toggleSidebar(){
    var status = this.state.sidebarOpened;
    this.setState({
      sidebarOpened: !status
    })
  }

  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };
  logout(){
    axios.delete('/api/v1/auth/sign_out/')
    .then((response) => {
      localStorage.clear();
      setTimeout(() => {
        window.location.href = '/'
      },500)
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  render() {
    const { width } = this.state;
    const isMobile = width <= 768;
    if(isMobile){
      return (
        <div id="responsiveSidebar" className="SidebarResp">
          <ul className="sb-text-brown">
            <li className={window.location.href.split('/').pop() === 'global_beehive'? 'active':''}><Link to="/global_beehive"><i className="icon-global" aria-hidden="true"></i><h6>Global Beehive</h6></Link></li>
            <li className={window.location.href.split('/').pop() === 'local_beehive'? 'active':''}><Link to="/local_beehive"><i className="icon-local" aria-hidden="true"></i><h6>Local Beehive</h6></Link></li>
            <li className={window.location.href.split('/').pop() === 'specialty_beehive'? 'active':''}><Link to="/specialty_beehive"><i className="icon-speciality" aria-hidden="true"></i><h6>Specialty Beehive</h6></Link></li>
            <li className={window.location.href.split('/').pop() === 'my_beehive'? 'active':''}><Link to="/my_beehive"><i className="icon-mybeehive" aria-hidden="true"></i><h6>My Beehive</h6></Link></li>
            <li className={window.location.href.split('/').pop() === 'research_teams'? 'active':''}><Link to="/research_teams"><i className="icon-research" aria-hidden="true"></i><h6>Research Teams</h6></Link></li>
            <hr />
            <li onClick={this.logout}><h6>Logout</h6></li>
            <li><Link to="/editprofile"><h6>Edit Profile</h6></Link></li>               
          </ul>
        </div>
      )
    }
    else{
      return (
        <div className={this.state.sidebarOpened? "Sidebar" : "Sidebar closed"}>
          <ul className="sb-text-brown">
            <li className={window.location.href.split('/').pop() === 'global_beehive'? 'active':''}>
              <Link to="/global_beehive">
                <i data-tip={this.state.sidebarOpened? "": 'Global Beehive'} className="icon-global" aria-hidden="true"></i>
                <span>Global Beehive</span>
              </Link>
            </li>
            <li className={window.location.href.split('/').pop() === 'local_beehive'? 'active':''}>
              <Link to="/local_beehive">
                <i data-tip={this.state.sidebarOpened? "": "Local Beehive"} className="icon-local" aria-hidden="true"></i>
                <span>Local Beehive</span>
              </Link>
            </li>
            <li className={window.location.href.split('/').pop() === 'specialty_beehive'? 'active':''}>
              <Link to="/specialty_beehive">
                <i data-tip={this.state.sidebarOpened? "":"Specialty Beehive"} className="icon-speciality" aria-hidden="true"></i>
                <span>Specialty Beehive</span>
              </Link>
            </li>
            <li className={window.location.href.split('/').pop() === 'my_beehive'? 'active':''}>
              <Link to="/my_beehive">
                <i data-tip={this.state.sidebarOpened? "":"My Beehive"} className="icon-mybeehive" aria-hidden="true"></i>
                <span>My Beehive</span>
              </Link>
            </li>
            <li className={window.location.href.split('/').pop() === 'research_teams'? 'active':''}>
              <Link to="/research_teams">
                <i data-tip={this.state.sidebarOpened? "":"Research Teams"} className="icon-research" aria-hidden="true"></i>
                <span>Research Teams</span>
              </Link>
            </li>
          </ul>
          <i data-tip={this.state.sidebarOpened? "Close Sidebar":"Open Sidebar"} onClick={this.toggleSidebar} className="icon-resize" aria-hidden="true"></i>
          <ReactTooltip />
        </div>
      );
    }
  }
}

export default Sidebar;
