import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios'
import { confirmAlert } from 'react-confirm-alert';
import './../Assets/css/Components/ProfileDetails.css';
import 'react-confirm-alert/src/react-confirm-alert.css'
import BeePP from './../Assets/images/beePP.png'

class ProfileDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      is_following:false,
      is_friend: false,
      is_friend_request_received: false,
      is_friend_request_sent: false
    };
    this.addFriend = this.addFriend.bind(this)
    this.follow = this.follow.bind(this)
    this.unfollow = this.unfollow.bind(this)
    this.acceptRequest = this.acceptRequest.bind(this)
    this.removeFriend = this.removeFriend.bind(this)
    this.cancelRequest = this.cancelRequest.bind(this)
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      is_following:nextProps.data.is_following,
      is_friend: nextProps.data.is_friend,
      is_friend_request_received: nextProps.data.is_friend_request_received,
      is_friend_request_sent: nextProps.data.is_friend_request_sent
    })
  }
  addFriend(){
    axios.post('/api/v1/users/'+ window.location.href.split('/').pop() +'/add_friend')
    .then((response) => {
      this.setState({
        is_following:true,
        is_friend: false,
        is_friend_request_received: false,
        is_friend_request_sent: true
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  follow(){
    axios.post('/api/v1/users/'+ window.location.href.split('/').pop() +'/follow')
    .then((response) => {
      this.setState({
        is_following:true
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  unfollow(){
    confirmAlert({
      title: 'Unfollow',
      message: 'Are you sure to unfollow ' + this.props.data.first_name + " ?",
      cancelLabel: 'Cancel',
      confirmLabel: 'Confirm',
      onConfirm: () => {
        axios.delete('/api/v1/users/'+ window.location.href.split('/').pop() +'/unfollow')
        .then((response) => {
          this.setState({
            is_following:false
          })
        })
        .catch(function (error) {
          console.log(error);
        });
      }
    })

  }
  acceptRequest(){
    axios.put('/api/v1/users/'+ window.location.href.split('/').pop() +'/accept_friend_request')
    .then((response) => {
      this.setState({
        is_friend: true,
        is_friend_request_sent: false,
        is_friend_request_received: false,
        is_following: true
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  removeFriend(){
    confirmAlert({
      title: 'Remove Friend',
      message: 'Are you sure to remove ' + this.props.data.first_name + " ?",
      cancelLabel: 'Cancel',
      confirmLabel: 'Confirm',
      onConfirm: () => {
      axios.delete('/api/v1/users/'+ window.location.href.split('/').pop() +'/unfriend')
      .then((response) => {
        this.setState({
          is_friend: false,
          is_friend_request_sent: false,
          is_friend_request_received: false,
          is_following: false
        })
      })
      .catch(function (error) {
        console.log(error);
      });
    }
  })
  }

  cancelRequest(){
    confirmAlert({
      title: 'Cancel',
      message: 'Are you sure to cancel request with ' + this.props.data.first_name + " ?",
      cancelLabel: 'Cancel',
      confirmLabel: 'Confirm',
      onConfirm: () => {
      axios.delete('/api/v1/users/'+ window.location.href.split('/').pop() +'/cancel_friend_request')
      .then((response) => {
        this.setState({
          is_friend: false,
          is_friend_request_sent: false,
          is_friend_request_received: false,
          is_following: true
        })
      })
      .catch(function (error) {
        console.log(error);
      });
      }
    })
  }
  render() {
    return (
      <div className="ProfileDetails">
        <div className="limitContainer">
          <div className="userDetails sb-text-brown">
            <img src={BeePP} alt="man"/>
            <div className="accountInfo">
            <h1>{this.props.data.first_name} {this.props.data.last_name}</h1>
            <h6 className="sb-text-yellow">{this.props.title === null?  this.props.title : ''}</h6>
            <div className="rowCenter">
              <i className="icon-location" aria-hidden="true"></i>
              <h6>{this.props.data.country === null ? 'Add your location': this.props.data.country}</h6>
            </div>
            <div className="rowCenter">
              <i className="icon-box" aria-hidden="true"></i>
              {this.props.data.specialty? <h6>{this.props.data.specialty.name} ({this.props.data.specialty.field})</h6> : null}
            </div>
            </div>
          </div>
          <div>
            <div className="accountDetails">
              <div className="votes sb-text-brown">
                <i className="icon-thumbs-up" aria-hidden="true"></i>
                <h6>{this.props.data.votes_count} Votes</h6>
              </div>
              <div className="followers sb-text-brown">
                <i className="icon-followers" aria-hidden="true"></i>
                <h6>{this.props.data.followers_count} Followers</h6>
              </div>
            </div>
            {
              localStorage.getItem("userId") === window.location.href.split('/').pop() ? '' :
              <div className="accountButtons">
                { this.state.is_friend_request_sent?
                  <button onClick={this.cancelRequest}>Cancel Request</button> :
                  this.state.is_friend_request_received?
                  <button onClick={this.acceptRequest}>Accept Request</button> :
                  this.state.is_friend?
                  <button onClick={this.removeFriend} className="active">Remove {this.props.data.first_name}</button> :
                  <button onClick={this.addFriend}>Add {this.props.data.first_name}</button>
                }
                {
                  this.state.is_following ?
                  <button onClick={this.unfollow} className="active">Unfollow {this.props.data.first_name}</button> :
                  <button onClick={this.follow}>Follow {this.props.data.first_name}</button>
                }

                {/* <button>Send A Message</button> */}
              </div>
            }
          </div>
        </div>
      </div>
    );
  }
}

ProfileDetails.propTypes = {
  data: PropTypes.object,
}

export default ProfileDetails;
