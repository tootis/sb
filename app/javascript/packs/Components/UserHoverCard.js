import React, { Component } from 'react';
import axios from 'axios'

import './../Assets/css/Components/UserHoverCard.css';

const str = "You need to enable JavaScript to run this app.You need to enable JavaScrYou need to enable JavaScript to run this app.You need to enable JavaScrYou need to enable JavaScript to run this app.You need to enable JavaScript to run this app.You need to enable JavaScript to run this app. "
class UserHoverCard extends Component {
  constructor(){
    super();
    this.state = {
      userData: {},
    };
    
  }
  componentDidMount(){
    axios.defaults.headers.common['client'] = localStorage.getItem("client")
    axios.defaults.headers.common['access-token'] = localStorage.getItem("access-token")
    axios.defaults.headers.common['uid'] = localStorage.getItem("uid")
    axios.get('/api/v1/users/' + this.props.authorId)
    .then((response) => {
      if(response.data.description.length > 150){
        response.data.description =  response.data.description.substring(0, 150) + "..."
      }
      this.setState({userData: response.data})
    })
    .catch(function (error) {
    });

  }
  render() {
    let commentCard = this.props.comment? "commentCard" : ""
    return (
      <div className={"UserHoverCard " + commentCard}>
        <div>
          <h5 className="sb-text-brown">{this.state.userData.first_name} {this.state.userData.last_name}</h5>
          {this.state.userData.specialty ? <h6 className="sb-text-yellow">{this.state.userData.title} - {this.state.userData.specialty.name} ({this.state.userData.specialty.field})</h6> : null}
          <h6 className="sb-text-brown">{this.state.userData.country}</h6>
        </div>
        <hr/>
        <p>{this.state.userData.description}</p>
        <div className="sb-text-brown">
          <span> <b>{this.state.userData.votes_count || 0}</b> Votes</span>
          <span> -  <b>{this.state.userData.followers_count || 0}</b> Followers</span>
        </div>
      </div>
    );
  }
}

export default UserHoverCard;
