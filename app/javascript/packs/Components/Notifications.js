import React, { Component } from 'react';
import Moment from 'react-moment';
import {Link} from 'react-router-dom';
import './../Assets/css/Components/Notifications.css';

class Notifications extends Component {
  getPathFromResource = (resource) => {
    let path = '';

    if(!resource)
      return path;

    if(resource.type === "Post") {
      path = "/posts/" + resource.id;
    } else if(resource.type == "User") {
      path = "/profile/" + resource.id;
    }

    return path;
  }

  render() {
    return (
      <div className={this.props.opened?"Notifications active":"Notifications"}>
        {!this.props.notifications.length? '' :
        <ul>
          {this.props.notifications.map(notification =>{
            if (notification.path != null)
              return(
                <Link onClick={(e) => this.props.selectionHandler(notification.id)} key={notification.id} to={this.getPathFromResource(notification.resource)}>
                <li className={notification.seen ? "" : "not-seen"}>{notification.text} <span><Moment format="DD MMMM - hh:mma">{notification.created_at}</Moment></span></li>
                <hr/>
                </Link>
              )
          })}
        </ul>
        }
      </div>
    );
  }
}
export default Notifications;
