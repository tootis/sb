import React, { Component } from 'react';
import './../Assets/css/Components/News.css';
import NewsCard from './NewsCard';
class News extends Component {
  constructor(){
    super();
    this.state = {
      width: window.innerWidth
    }
    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);
  }
  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };
  render() {
    const { width } = this.state;
    const isMobile = width <= 768;
    if(isMobile){
      return (
        <div>

        </div>
      );
    }
    else{
      return (
        <div className="News">
          <NewsCard />
        </div>
      );
    }
  }
}

export default News;
