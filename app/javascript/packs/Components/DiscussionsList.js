import React, { Component } from 'react';
import Moment from 'react-moment';
import { Link } from 'react-router-dom'
import axios from 'axios'
import './../Assets/css/Components/DiscussionsList.css';

class DiscussionsListItem extends Component {
    render() {
        return (
            <Link to={"/discussion/" + this.props.id}>
                <div className={this.props.active ? "DiscussionsListItem active" : "DiscussionsListItem"}>
                    <div className="heading">
                        <label className="title">{this.props.title}</label>
                        <p className="timestamp"><Moment format="hh:mma">{this.props.created_at}</Moment></p>
                    </div>
                    {this.props.messages.body &&
                        <label className="last-message">
                        {
                            this.props.messages.sender.id == localStorage.getItem("userId") ?
                                "You" : this.props.messages.sender.first_name
                        }
                        {": " }
                        {this.props.messages.body}
                        </label>
                    }
                </div>
            </Link>
        );
    }
}

class DiscussionsList extends Component {
    state = {
        predicate: ""
    }

    didSelectItem = (id) => {
        this.props.onChange(id);
    }

    predicateChangeHandler = (e) => {
        this.setState({predicate: e.target.value});
    }

    render() {
        let predicate = ".*" + this.state.predicate + ".*";
        let re = new RegExp(predicate, 'i');
        let filteredDiscussions = this.props.discussions.filter((item) => {
            return item.title.search(re) > -1;
        });
        
        return (
            <div className="DiscussionsList">
                <div className="searchBar">
                    <div className="searchInput">
                        <span>
                            <i className="icon-search" aria-hidden="true"></i>
                        </span>
                        <input type="text" placeholder="Search" value={this.state.predicate} onChange={this.predicateChangeHandler} />
                    </div>

                    <button className="sb-text-brown" onClick={this.props.onNewDiscussion}>
                        <i className="icon-discussion-new" aria-hidden="true"></i>
                    </button>
                </div>

            
                {filteredDiscussions.map((item) => (
                    <DiscussionsListItem {...item} handleSelect={this.didSelectItem} active={item.id === this.props.active.id} key={item.id} />
                ))}
            </div>
        );
    }
}

export default DiscussionsList;