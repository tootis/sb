import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import BeePP from './../Assets/images/beePP.png'
import PropTypes from 'prop-types';
import './../Assets/css/Components/PostOwner.css';
import UserHoverCard from './UserHoverCard'

class PostOwner extends Component {
  constructor(){
    super();
    this.state = {
      showDetails : false,
    }
  }

  clickHandler = (e) => {
    if(this.props.onClick)
      this.props.onClick(e, this.props.author);
  }

  render() {
    return (
      <div className="PostOwner" onClick={this.clickHandler}>
        <div className="profilePic">
          <img src={BeePP} alt="man"/>
        </div>
        <div className="personTitles">
        <a
          onMouseOver={() => this.setState({showDetails:true})}
          onMouseLeave={() => this.setState({showDetails:false})} 
           href={"/profile/" + this.props.author.id}><h5 className="sb-text-brown">{this.props.author.first_name} {this.props.author.last_name}</h5></a>
          {this.props.specialty?<h6 className="sb-text-yellow">{this.props.author.title} - {this.props.specialty.name} ({this.props.specialty.field})</h6>:
          <h6 className="sb-text-yellow"> {this.props.author.title}</h6>}
        </div>
        {this.state.showDetails && !this.props.popup ? <UserHoverCard  authorId={this.props.author.id}/>:null}
      </div>
    );
  }
}

PostOwner.propTypes = {
  author: PropTypes.object,
}

export default PostOwner;
