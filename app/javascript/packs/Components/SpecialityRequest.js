import React, { Component } from 'react';
import Popup from './../Components/Popup'
import axios from 'axios'
import '../Assets/css/Components/SpecialityRequest.css'

class SpecialityRequest extends Component {
    state = {
        field_name: "",
        specialty_name: ""
    }

    handleChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    submitRequest = () => {
        axios.post("/api/v1/specialty_requests", {
            specialty_request: this.state
        })
        .then(response => {
            if(response.status === 200) {
                this.props.onSuccess("Request submitted successfully");
                this.props.onClose();
            } else {
                this.props.onError(["Couldn't submit request right now, please try again later"])
            }
        })
        .catch(error => {
            if(error.response && error.response.data) {
                this.props.onError(error.response.data)
            }
            else {
                this.props.onError();
            }
        });
    }
    
    render() {
        let canSave = this.state.field_name.length > 0 && this.state.specialty_name.length > 0;

        return (
            <Popup>
                <div className="SpecialityRequestPopup">
                    <div className="titlePop">
                        <div className='titleWx'>
                        <h3>Request Speciality</h3>
                        <i onClick={this.props.onClose} className="icon-cancel" aria-hidden="true"></i>
                        </div>
                        <hr/>
                    </div>
                    <div className="bodyPop">
                        <label>Field</label>
                        <input type="text" name="field_name" value={this.state.field_name} onChange={this.handleChange} placeholder="Field name" />

                        <label>Speciality</label>
                        <input type="text" name="specialty_name" value={this.state.specialty_name} onChange={this.handleChange} placeholder="Speciality name" />
                    </div>

                    {canSave &&
                        <div className="btnPop">
                            <button onClick={this.submitRequest}>Request</button>
                        </div>
                    }
                </div>
            </Popup>
        );
    }
}

export default SpecialityRequest;
