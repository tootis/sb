import React, { Component } from 'react';
import axios from 'axios'
import ReactTooltip from 'react-tooltip'
import SpecialtySelect from './SpecialtySelect';

import './../Assets/css/Components/PostingTools.css'; 
class PostingTools extends Component {
  constructor(){
        super();
        this.state = {
            selectedPostButton:'',
            title:'',
            content:'',
            tags:'',
            specialties: [],
            specialtyId:0
        };
        this.selectPostButton = this.selectPostButton.bind(this)
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleChangeContent = this.handleChangeContent.bind(this);
        this.handleChangeTags = this.handleChangeTags.bind(this);
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.onPost = this.onPost.bind(this)
        
    };
  componentDidMount() {
    axios.get('/api/v1/fields/')
    .then((response) => {
      let specialityID = 0;
      if(Array.isArray(response.data) && response.data.length > 0 && Array.isArray(response.data[0].specialties) && response.data[0].specialties.length > 0)
        specialityID = response.data[0].specialties[0].id;
      this.setState({specialties: response.data, specialtyId: specialityID});
    })
    .catch((error) => {
      console.log(error)
    });
  }
  
  selectPostButton(e,postingButton){
      if(this.state.selectedPostButton === postingButton){
        this.setState({
          selectedPostButton:''
        })
      }
      else{
        this.setState({
          selectedPostButton:postingButton
        })
      }
  }
  handleChangeTitle(event) {
    this.setState({title: event.target.value});
  }
  handleChangeContent(event) {
    this.setState({content: event.target.value});
  }
  handleChangeTags(event) {
    this.setState({tags: event.target.value});
  }

  validate = () => {
    let errors = [];
    if(this.state.title.length < 4)
      errors.push("Title should be at least 4 characters.");
    if(this.state.content.length < 15)
      errors.push("Content should be at least 15 characters.");

    let re  = new RegExp(/^[0-9a-zA-Z ]+$/);
    if(this.state.title.length >= 4 && !re.test(this.state.title))
      errors.push("Only characters, spaces and numbers are allowed in post title.");      

    return errors;
  }

  onPost(type){
    let errors = this.validate();
    if(errors.length > 0) {
      this.props.onError(errors);
      return;
    }
    
    this.props.onPost(this.state.title, type, this.state.content, this.state.tags, this.state.specialtyId);
    this.setState({
      selectedPostButton:'',
      title:'',
      content:'',
      tags:''
    })
  }
  handleFieldChange(event){
    this.setState({ specialtyId:  event.target.value });
  }
  render() {

    var postingButtons = [{text:'Share Knowledge',icon:'icon-spread'},{text:'Ask Question',icon:'icon-question'}] //{text:'Create Event',icon:'icon-event'},{text:'Make Competition',icon:'icon-competition'}

    let email = localStorage.getItem("uid");
    let re = new RegExp(/^.*@sciencebeehive.com$/);
    const showKnowledgeSpecialities = re.test(email);

    return (
      <div className="PostingTools">
        <div className="postingButtonsContainer">
          {postingButtons.map((postingButton,index) =>{
            return(
              <div key={index}>
                <i  data-tip={postingButton.text} onClick={(e) => this.selectPostButton(e,postingButton.text)} className={this.state.selectedPostButton === postingButton.text? postingButton.icon + ' active' : postingButton.icon} aria-hidden="true"></i>
              </div>
            )})}
          </div>
          <div className={this.state.selectedPostButton === 'Share Knowledge'? "shareKnowlegdeInput active" : "shareKnowlegdeInput"}>
            <h5 className="sb-text-yellow">Spread Knowledge</h5>
            <input placeholder="Knowledge title" type="text" value={this.state.title} onChange={this.handleChangeTitle}/>
            <textarea placeholder="Spread to the world..." value={this.state.content} onChange={this.handleChangeContent}/>
            {showKnowledgeSpecialities &&
              <SpecialtySelect onChange={this.handleFieldChange} selectedSpecialty={this.state.specialtyId} />
              
            }
            {/* <div className="addPhoto">
              <i className="fa fa-picture-o" aria-hidden="true"></i>
              <span>Add Photo</span>
            </div> */}
            <input placeholder="Add Tags.." type="text" value={this.state.tags} onChange={this.handleChangeTags}/>
            <div className="spread">
              <button onClick={(e) => this.onPost('knowledge')}>Spread</button>
            </div>
          </div>
          <div className={this.state.selectedPostButton === 'Ask Question'? "askQuestionInput active" : "askQuestionInput"}>
            <h5 className="sb-text-yellow">Ask Question</h5>
            <input placeholder="Question title" type="text" value={this.state.title} onChange={this.handleChangeTitle}/>
            <textarea placeholder="Ask whatever you want..." value={this.state.content} onChange={this.handleChangeContent}/>

            <SpecialtySelect onChange={this.handleFieldChange} selectedSpecialty={this.state.specialtyId} />

            {/* <div className="addPhoto">
              <i className="fa fa-picture-o" aria-hidden="true"></i>
              <span>Add Photo</span>
            </div> */}
            <input placeholder="Add Tags.." type="text" value={this.state.tags} onChange={this.handleChangeTags}/>
            <div className="spread">
              <button onClick={(e) => this.onPost('question')}>Ask</button>
            </div>
          </div>
          <ReactTooltip />
      </div>
    );
  }
}

export default PostingTools;
