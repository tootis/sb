import React, { Component } from 'react';
import axios from 'axios'
import PostOwner from './../Components/PostOwner'
import '../Assets/css/Components/SearchUsers.css'

class SearchUsers extends Component {
    state = {
        searchPredicate: "",
        searchResults: {
            friends: [],
            others: []
        },
    }

    handleChange = (e) => {
        axios.get("/api/v1/users/search", {
            params: {
                q: e.target.value
            }
        })
        .then(response => {
            this.setState({searchResults: response.data});
        })
        .catch(error => {
            console.log(error.response.data);
        })

        this.setState({[e.target.name]: e.target.value});
    }

    handleSelect = (e, member) => {
        this.props.selectionHandler(member);
        this.setState({
            searchPredicate: "",
            searchResults: {friends: [], others: []}
        });
    }

    render() {
        let searchFriends = this.state.searchResults.friends.filter((member) => !this.props.exclude.find(m => m.id === member.id));

        let searchOthers = this.state.searchResults.others.filter((member) => !this.props.exclude.find(m => m.id === member.id));

        return (
            <div className="SearchUsers">
                <input disabled={this.props.disabled} name="searchPredicate" onChange={this.handleChange} value={this.state.searchPredicate} placeholder={this.props.placeholder}/>

                {this.state.searchPredicate.length > 0 &&
                    <div className="searchResult">
                        <label className="heading">Friends</label>
                            {searchFriends.length === 0 && 
                                <p className="notFound">No results found.</p>
                            }
                            {searchFriends.map((member) => (
                                <PostOwner onClick={this.handleSelect} author={member} key={member.id}/>
                            ))}
                        <label className="heading">Others</label>
                            {searchOthers.length === 0 && 
                                <p className="notFound">No results found.</p>
                            }
                            {searchOthers.map((member) => (
                                <PostOwner onClick={this.handleSelect} popup author={member} key={member.id}/>
                            ))}
                    </div>
                }
            </div>
        );
    }
}

export default SearchUsers;