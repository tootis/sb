import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './../Assets/css/Components/ProfileDescription.css';

class ProfileDescription extends Component {
  render() {
    return (
      <div className="ProfileDescription sb-text-brown">
        <p>{this.props.description}</p>
      </div>
    );
  }
}

ProfileDescription.propTypes = {
  description: PropTypes.string,
}

export default ProfileDescription;
