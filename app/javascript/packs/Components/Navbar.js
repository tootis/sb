import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom'
import AlertContainer from 'react-alert'
import Notifications from './Notifications';
import './../Assets/css/Components/Navbar.css';
import NavbarLogo from './../Assets/images/Components/Navbar-logo.svg'
import NavbarLogoResponsive from './../Assets/images/Components/Navbar-logo-resp.svg'
import ActionCable from './../socket'
import BeePP from './../Assets/images/beePP.png'


const Dropdown = ({
  logout
}) =>
  <ul>
    <li><Link to="/editprofile">Edit Profile</Link></li>
    <li onClick={logout}>Logout</li>
  </ul>

const NotifCount = (props) => {
  return props.count ? <span className="notificationNumber">{props.count}</span> : null;
}

class Navbar extends Component {
  constructor(){
    super();
    this.state = {
      width: window.innerWidth,
      dropdownOpened:false,
      notificationsOpened:false,
      notifications: [],      
    }
    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);
    this.openDropdown = this.openDropdown.bind(this);
    this.openNotifications = this.openNotifications.bind(this);
  }
  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    this.notificationChannel.unsubscribe();
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  componentDidMount() {
    this.fetchNotifications();
    this.connectToSocket();
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };
  toggleSidebar(){
    document.getElementById('responsiveSidebar').classList.toggle("open");
    document.getElementById('menuToggle').classList.toggle("active");
  }
  openDropdown(event){
    event.preventDefault();
    this.setState({
      notificationsOpened:false
    })
    this.setState((prevState) => {
      return {dropdownOpened: !prevState.dropdownOpened};
    });
  }
  openNotifications(event){
    event.preventDefault();
    this.setState({
      dropdownOpened:false
    })
    this.setState((prevState) => {
      return {notificationsOpened: !prevState.notificationsOpened};
    });
  }
  logout(){
    axios.delete('/api/v1/auth/sign_out/')
    .then((response) => {
      localStorage.clear();
      setTimeout(() => {
        window.location.href = '/'
      },500)
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  fetchNotifications = () => {
    axios.defaults.headers.common['client'] = localStorage.getItem("client")
    axios.defaults.headers.common['access-token'] = localStorage.getItem("access-token")
    axios.defaults.headers.common['uid'] = localStorage.getItem("uid")
    axios.get('/api/v1/notifications')
    .then((response) => {
      this.setState({
        notifications: response.data
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  connectToSocket = () => {
    this.notificationChannel = ActionCable.subscribeToChannel({channel: "Api::V1::NotificationsChannel"}, {
      received: this.didReceiveNotification,
      disconnected: () => this.notificationChannel.unsubscribe()
    })
  }

  didReceiveNotification = (data) => {
    const notification = JSON.parse(data.notification);

    this.setState((prevState) => {
      return {
        notifications: [notification, ...prevState.notifications]
      };
    })

    this.successAlert(notification.text);
  }

  didSelectNotification = (id) => {
    this.notificationChannel.perform('mark_as_read', {
      notification_id: id
    });

    this.setState(prevState => {
      let notifications = prevState.notifications.slice();
      let notification = notifications.find(n => n.id === id);
      notification.seen = true;

      return {notifications: notifications};
    })
  }

  alertOptions = {
    offset: 14,
    position: 'bottom left',
    theme: 'light',
    time: 5000,
    transition: 'scale'
  }
  errorAlert = (errors) => {
    if(Array.isArray(errors)){
      for(let error of errors){
        this.msg.show(error, {
          time: 5000,
          type: 'error',
          icon: <i className="icon-error errorRed" aria-hidden="true"></i>
        })
      }
    }
    else{
      this.msg.show('Something goes wrong!', {
        time: 5000,
        type: 'error',
        icon: <i className="icon-error errorRed" aria-hidden="true"></i>
      })
    }
  }
  successAlert = (message) => {
    this.msg.show(message , {
      time: 5000,
      type: 'success',
      icon: <i className="icon-success successGreen" aria-hidden="true"></i>
    })
  }
  
  render() {
    const { width } = this.state;
    const isMobile = width <= 768;

    let notifications_length = this.state.notifications.filter(n => !n.seen).length;
    if(isMobile){
      return (
        <div>
          <div className="responsiveNav sb-text-brown">
            <Link to="/my_beehive">
              <img src={NavbarLogoResponsive} alt="SB-Logo"/>
            </Link>
            <Link to="/discussion">
              <i className="icon-discussion" aria-hidden="true"></i>
            </Link>            
            <i onClick={this.openNotifications} className="icon-notification" aria-hidden="true"></i>
            {/* <i className="icon-search" aria-hidden="true"></i> */}
            <Link to={"/profile/" + localStorage.getItem("userId")}><img className="profile" src={BeePP} alt="profile"/></Link>
            <i onClick={this.toggleSidebar} id="menuToggle" className="icon-menu" aria-hidden="true"></i>
          </div>
          <Notifications notifications={this.state.notifications} opened={this.state.notificationsOpened} selectionHandler={this.didSelectNotification} />
          <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />          
        </div>
      );
    }
    else{
      return (
        <div className="Navbar">
          <div className="logo-nav">
            <Link to="/my_beehive">
              <img src={NavbarLogo} alt="SB-Logo"/>
            </Link>
          </div>
          {/* <div className="searchbar">
            <i className="icon-search" aria-hidden="true"></i>
            <input type="text" id="search-bar" placeholder="Search Beehive..." />
          </div> */}
          <div className="navIcons" style={{justifyContent:'flex-end'}}>
            <div className="icon">
              <Link to="/discussion">
                <i className="icon-discussion" aria-hidden="true"></i>
                <span>Discussion</span>
              </Link>
            </div>
            <div onClick={this.openNotifications} className="icon">
              <i className="icon-notification" aria-hidden="true"></i>
              <span>Notifications <NotifCount count={notifications_length}/> </span>
            </div>
            <div className="icon profile" >
              <Link to={"/profile/" + localStorage.getItem("userId")}><img src={BeePP} alt="profile"/></Link>
              <i onClick={this.openDropdown} className={this.state.dropdownOpened? "arrowDropdown active icon-down-open" : "arrowDropdown icon-down-open"} aria-hidden="true"></i>
            </div>
          </div>
          <div className={this.state.dropdownOpened?"dropdownMenu active":"dropdownMenu"}>
            <Dropdown logout={this.logout}/>
          </div>
          <Notifications notifications={this.state.notifications} opened={this.state.notificationsOpened} selectionHandler={this.didSelectNotification} />

          <AlertContainer ref={a => {console.log(a); this.msg = a}} {...this.alertOptions} />
        </div>
      );
    }
  }
}
export default Navbar;
