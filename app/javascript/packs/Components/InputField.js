import React, { Component } from 'react';
import './../Assets/css/Components/InputField.css';
import PropTypes from 'prop-types';

class InputField extends Component {

  render() {
    return (
      <div className="InputField">
        <div className="group">
         <input type={this.props.inputType} required value={this.props.value} onChange={this.props.onChange} name={this.props.name}/>
         <span className="highlight"></span>
         <span className="bar"></span>
         <label>{this.props.label}</label>
       </div>
      </div>
    );
  }
}

InputField.propTypes = {
  label: PropTypes.string,
  inputType: PropTypes.string
}
export default InputField;
