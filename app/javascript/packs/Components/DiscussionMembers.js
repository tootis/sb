import React, { Component } from 'react';
import PostOwner from './PostOwner';
import '../Assets/css/Components/DiscussionMembers.css'
import BeePP from './../Assets/images/beePP.png'

class DiscussionMemberItem extends Component {

    removeMember = (e, id) => {
        this.props.removeMemberHandler(id);
    }

    render() {
        const { id, first_name, last_name, title} = this.props;
        
        return (
            <div key={id} className="DiscussionMemberItem">
                <PostOwner popup author={this.props}/>
                <i onClick={(e) => this.removeMember(e, id)} className="icon-user-delete" aria-hidden="true"></i>
            </div>
        );
    }
}

class DiscussionMembers extends Component {
    render() {
        return (
            <div className="DiscussionMembers">
                {this.props.showBack &&
                    <i className="backBtn icon-down-open" onClick={this.props.onBack} />
                }
                
                <label className="heading">Members</label>

                <div className="member-add" onClick={this.props.newMemberHandler}>
                    <i className="icon-user-add sb-text-yellow" aria-hidden="true" />
                    <label>Add members</label>
                </div>

                <div className="members-list">
                    {this.props.discussion.participants && this.props.discussion.participants.map((member) => (
                        <DiscussionMemberItem {...member} removeMemberHandler={this.props.removeMemberHandler} key={member.id} />
                    ))}
                </div>
            </div>
        );
    }
}

export default DiscussionMembers;