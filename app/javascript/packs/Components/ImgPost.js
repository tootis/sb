import React, { Component } from 'react';
import PostOwner from './PostOwner';
import PostIcons from './PostIcons';
import Comments from './Comments';
import './../Assets/css/Components/ImgPost.css';

class ImgPost extends Component {
  render() {
    return (
      <div className="ImgPost">
        <PostOwner />
        <div className="postImg">
          <img src="http://www.nbn.org.il/wp-content/uploads/2014/11/scientificresearch_29824805_cropped.jpg" alt=""/>
        </div>
        <p className="sb-text-brown">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus rhoncus condimentum felis quis molestie. Vivamus sem purus, convallis id magna vel, luctus tincidunt ante. Mauris congue tristique pulvinar. Proin vitae arcu orci. Aliquam ultrices nunc sed nunc ornare, eget facilisis leo tempor. Nulla sagittis condimentum augue eget volutpat. In hac habitasse platea dictumst. Proin dapibus lacus quis vulputate semper. Nulla commodo odio placerat, scelerisque magna quis, ornare nisl. Vestibulum bibendum efficitur dolor eu faucibus. Integer fermentum lorem et nibh rutrum, a faucibus lacus bibendum. Sed sem risus, maximus quis quam nec, molestie feugiat quam. Morbi vitae massa ac mauris pellentesque consectetur. Nullam quis ex ac orci gravida consequat. Vestibulum eget eros ac ante vulputate iaculis. Curabitur commodo convallis tortor in malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent ut nisl quis nibh euismod hendrerit. Pellentesque nibh lacus, tristique in dapibus nec, commodo sed tellus. Nullam massa purus, suscipit sed est vitae, fermentum placerat leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam eu accumsan neque. Aliquam erat volutpat. Sed aliquam a urna at vehicula.
        </p>
        <PostIcons />
        <Comments/>
      </div>
    );
  }
}

export default ImgPost;
