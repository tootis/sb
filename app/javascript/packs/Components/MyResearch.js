import React, { Component } from 'react';
import axios from 'axios'
import Popup from './Popup';
import './../Assets/css/Components/MyResearch.css';
import {Link} from 'react-router-dom';

class MyResearch extends Component {
  constructor(){
    super();
    this.state = {
      teams:[],
      specialties:[],
      createTeamPop:false,
      createTeamInfo:{specialty_id:1},
      joinTeamPop:false,
      joinTeamId:null
    }
    this.openCreateTeam = this.openCreateTeam.bind(this)
    this.createTeam = this.createTeam.bind(this)
    this.openJoinTeam = this.openJoinTeam.bind(this)
    this.joinTeam = this.joinTeam.bind(this)
    this.handleCreateTeamInfo = this.handleCreateTeamInfo.bind(this)
    this.closePopup = this.closePopup.bind(this)
    this.handleJoinTeamId = this.handleJoinTeamId.bind(this)
    this.updateTeams = this.updateTeams.bind(this)
  }
  componentDidMount(){
    axios.get('/api/v1/fields/')
    .then((response) => {
      this.setState({
        specialties:response.data
      })
    })
    .catch(function (error) {
      console.log(error);
    });

    axios.get('/api/v1/research_teams/')
    .then((response) => {
      this.setState({
        teams: response.data
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  openCreateTeam(){
    this.setState({
      createTeamPop:true
    })
  }
  createTeam(){
    axios.post('/api/v1/research_teams/' ,{
      research_team:{
        name: this.state.createTeamInfo.name,
        description: this.state.createTeamInfo.description,
        specialty_id: this.state.createTeamInfo.specialty_id
      }
    })
    .then((response) => {
      this.updateTeams();
      this.setState({
        createTeamPop: false
      })
      window.location.href = '/research_teams/' + response.data.id
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  openJoinTeam(){
    this.setState({
      joinTeamPop:true
    })
  }
  joinTeam(){
    axios.post('/api/v1/research_teams/' + this.state.joinTeamId + "/join")
    .then((response) => {
      this.updateTeams();
      this.setState({
        joinTeamPop: false
      })

    })
    .catch(function (error) {
      console.log(error);
    });
  }
  handleCreateTeamInfo(event) {
    let temp = this.state.createTeamInfo
    temp[event.target.name] = event.target.value
    this.setState({createTeamInfo: temp});
  }
  handleJoinTeamId(event){
    this.setState({
      joinTeamId:event.target.value
    })
  }
  closePopup(){
    this.setState({
      createTeamPop:false,
      joinTeamPop:false
    })
    this.props.closePopup();
  }
  updateTeams(){
    axios.get('/api/v1/research_teams/')
    .then((response) => {
      this.setState({
        teams: response.data
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  render() {
    return (
      <div className={this.props.active? "MyResearch active" : "MyResearch"}>
          <div>
            <h4>My Research Beehives</h4>
            <hr/>
            <ul>
              {this.state.teams.map(team =>{
                return(
                <li className="MyTeam-Item" key={team.id}>
                  <Link to={"/research_teams/" + team.id}>
                    <i className="icon-research" aria-hidden="true"></i>
                    {team.name}
                  </Link>
                </li>
            )})}
            </ul>
          </div>
          <div className="researchBtns">
            <button onClick={this.openJoinTeam}>Join Team</button>
            {this.state.joinTeamPop?
            <Popup>
              <div className='PopResearch'>
                <div className="titlePop">
                  <div className='titleWx'>
                    <h3>Join Team</h3>
                    <i onClick={this.closePopup} className="icon-cancel" aria-hidden="true"></i>
                  </div>
                  <hr/>
                </div>
                <div className="bodyPop">
                  <label>Enter Team id</label>
                  <input onChange={this.handleJoinTeamId} type="number" name='name'/>
                </div>
                <div className="btnPop">
                  <button onClick={this.joinTeam}>Join Team</button>
                </div>
              </div>
            </Popup>:null}
            <button onClick={this.openCreateTeam}>Create Team</button>
            {this.state.createTeamPop?
            <Popup>
              <div className='PopResearch'>
                <div className="titlePop">
                  <div className='titleWx'>
                    <h3>Create New Team</h3>
                    <i onClick={this.closePopup} className="icon-cancel" aria-hidden="true"></i>
                  </div>
                  <hr/>
                </div>
                <div className="bodyPop">
                  <label>Team Title</label>
                  <input onChange={this.handleCreateTeamInfo} name='name'/>
                  <label>Team Description</label>
                  <textarea onChange={this.handleCreateTeamInfo} name='description'/>
                  <label>Specialty</label>
                  <select defaultValue='1' onChange={this.handleCreateTeamInfo} name='specialty_id'>
                    {this.state.specialties.map(specialtyField =>{
                      return(
                      <optgroup key={specialtyField.name} label={specialtyField.name}>
                        {specialtyField.specialties.map(specialty =>{
                          return(
                            <option key={specialty.id} value={specialty.id}>{specialty.name}</option>
                          )
                        })}
                      </optgroup>
                    )})}
                  </select>
                </div>
                <div className="btnPop">
                  <button onClick={this.createTeam}>Create Team</button>
                </div>
              </div>
            </Popup>:null}
          </div>
      </div>
    );
  }
}

export default MyResearch;
