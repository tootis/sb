import React, { Component } from 'react';
import './../Assets/css/Components/Tags.css';

class Tags extends Component {
  render() {
    return (
      <div className="Tags">
        {this.props.tags.map(tag => {
          return(
          <p key={tag.id}>{tag.name}</p>
        )
        })}
      </div>
    );
  }
}


export default Tags;
