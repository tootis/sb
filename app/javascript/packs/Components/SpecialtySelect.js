import React, { Component } from 'react';
import axios from 'axios'

class SpecialtySelect extends Component {
    state = {
        field: '',
        specialties: {}      
    }
    
    handleChange = (e) => {
        if(e.target.name === "field") {
            this.setState({field: e.target.value});
        } else if(e.target.name === "specialty") {
            this.props.onChange(e, 'specialty');
        }
    }

    componentDidMount() {
        axios.get('/api/v1/fields/')
        .then((response) => {
            let fields = {};
            response.data.forEach(field => {
                fields[field.name] = field.specialties;

                field.specialties.forEach(specialty => {
                    if(specialty.id === this.props.selectedSpecialty)
                        this.setState({field: field.name});
                })
            });

            if(this.state.field === '' && response.data.length > 0)
                this.setState({field: response.data[0].name});

            this.setState({specialties: fields})
        })  
        .catch((error) => {
            if(this.props.errorAlert) {
                if (error.response) {
                    this.props.errorAlert(error.response.data.errors.full_messages);
                }
                else {
                    this.props.errorAlert(error.response.data.errors.full_messages);
                }
            }
        });
    }

    componentWillReceiveProps(nextProps) {
        Object.keys(this.state.specialties).forEach(field => {
            this.state.specialties[field].forEach(specialty => {
                if(specialty.id == nextProps.selectedSpecialty.id || specialty.id == nextProps.selectedSpecialty)
                    this.setState({field: field});
            });
        });
    }
    
    render() {
        const showLabels = this.props.showLabels;
        
        return (
            <div>
                <div className='dataLine'>
                    {showLabels && <label>Field:</label>}
                    <select name='field' value={this.state.field} onChange={this.handleChange} >
                    {Object.keys(this.state.specialties).map(field => {
                        return(
                            <option key={field} value={field}>{field}</option>
                        );
                    })}
                    </select>
                </div>
                
                <div className='dataLine'>
                    {showLabels && <label>Specialty:</label>}
                    <select name='specialty' value={this.props.selectedSpecialty.id || this.props.selectedSpecialty} onChange={this.handleChange} >
                    {this.state.specialties[this.state.field] && this.state.specialties[this.state.field].map(specialty =>{
                        return(
                        <option key={specialty.id} value={specialty.id}>{specialty.name}</option>
                        )
                    })}
                    </select>
                </div>
            </div>
        );
    }
}

export default SpecialtySelect;