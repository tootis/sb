import React, { Component } from 'react';
import { confirmAlert } from 'react-confirm-alert';
import PropTypes from 'prop-types';
import axios from 'axios'
import Moment from 'react-moment';
import './../Assets/css/Components/PostIcons.css';

class PostIcons extends Component {
  constructor(props){
    super(props)
    this.state = {
      upvotes:0,
      downvotes:0,
      hasVoted:null,
      activeEdits:null,
      editPost:null
    }
    this.upvotes = this.upvotes.bind(this)
    this.downvotes = this.downvotes.bind(this)
    this.activateEdits = this.activateEdits.bind(this)
    this.deletePost = this.deletePost.bind(this)
    this.editPost = this.editPost.bind(this)
  }
  componentWillMount(){
    this.setState({
      upvotes:this.props.upvotes,
      downvotes: this.props.downvotes,
      hasVoted: this.props.hasVoted
    })
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      upvotes: nextProps.upvotes,
      downvotes: nextProps.downvotes,
      hasVoted: nextProps.hasVoted
    })
  }
  upvotes(){
    axios.post('/api/v1/posts/'+ this.props.postId +'/toggle_upvote')
    .then((response) => {
      this.props.updatePosts();
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  downvotes(){
    axios.post('/api/v1/posts/'+ this.props.postId +'/toggle_downvote')
    .then((response) => {
      this.props.updatePosts();
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  activateEdits(){
    if(this.state.activeEdits === this.props.postId){
      this.setState({activeEdits : null})
    }
    else{
      this.setState({activeEdits : this.props.postId})
    }
  }
  deletePost(){
    confirmAlert({
      title: 'Delete Post',
      message: 'Are you sure you want to delete this post ?',
      cancelLabel: 'Cancel',
      confirmLabel: 'Confirm',
      onConfirm: () => {
        axios.delete('/api/v1/posts/'+ this.props.postId)
        .then((response) => {
          this.props.updatePosts();
        })
        .catch(function (error) {
          console.log(error);
        });
      }
    })
  }
  editPost(){
    this.setState({
      editPost:this.props.postId,
      activeEdits:null
    })

    this.props.onEdit();
  }
  render() {
    return (
      <div className="PostIcons">
        <div className="buttons">
          <div onClick={this.upvotes} className={this.state.hasVoted && this.state.hasVoted !== null? "iconWText active" : "iconWText"}>
            <i className="icon-thumbs-up" aria-hidden="true"></i>
            <p>{this.state.upvotes}</p>
          </div>
          <div onClick={this.downvotes} className={!this.state.hasVoted && this.state.hasVoted !== null? "iconWText active" : "iconWText"}>
            <i className="icon-thumbs-down" aria-hidden="true"></i>
            <p>{this.state.downvotes}</p>
          </div>
          {/*<div className="iconWText">
            <i className="fa fa-retweet" aria-hidden="true"></i>
            <p>72,660</p>
          </div>*/
          }
        </div>
        <div className='optionAndTime'>
          <p><Moment format="DD MMMM - hh:mma">{this.props.postTime}</Moment> </p>
          <div className={Number(this.props.authorId) === Number(localStorage.getItem("userId"))? "editsContainer active" : "editsContainer"}>
            <i onClick={this.activateEdits} className="icon-ellipsis" aria-hidden="true"></i>
            <ul className={this.state.activeEdits === this.props.postId? "edits active" : "edits"}>
              <li onClick={this.editPost}>Edit</li>
              <li onClick={this.deletePost}>Delete</li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

PostIcons.propTypes = {
  upvotes: PropTypes.number,
  downvotes: PropTypes.number,
  postId: PropTypes.number
}

export default PostIcons;
