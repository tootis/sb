import React, { Component } from 'react';
import axios from 'axios'
import './../Assets/css/Components/ResearchTools.css';
import Popup from './Popup';
import PostOwner from './PostOwner';
import MyResearch from './MyResearch';

const TeamOptions = ({
  teamMembersPop,
  saveTeamInfo,
  specialties,
  openEditTeam,
  closePopup,
  handleChangeTeamInfo,
  editTeamPop,
  selectedTeam,
  openTeamMembers,
  leaveTeam,
  removeUser,
  inviteMember,
  handleChangeInviteMemeber,
  inviteMemberId,
  active
}) =>
<div className={active? "teamOptions active": "teamOptions "}>
<div>
  <h4>{selectedTeam.name}</h4>
  <hr/>
</div>
<p>{selectedTeam.description}</p>
<strong><p>{selectedTeam.specialty? selectedTeam.specialty.name +' (' + selectedTeam.specialty.field + ')' : null}</p></strong>
<div className="researchBtns">
  <button onClick={openEditTeam}>Edit Team</button>
  {
    editTeamPop ?
    <Popup>
      <div className='PopResearch'>
        <div className="titlePop">
          <div className='titleWx'>
            <h3>Edit Team</h3>
            <i onClick={closePopup} className="icon-cancel" aria-hidden="true"></i>
          </div>
          <hr/>
          <button onClick={leaveTeam} className="leaveBtn">Leave {selectedTeam.name}</button>
        </div>
        <div className="bodyPop">
          <label>Team Title</label>
          <input onChange={handleChangeTeamInfo} name='name' defaultValue={selectedTeam.name}/>
          <label>Team Description</label>
          <textarea onChange={handleChangeTeamInfo} name='description' defaultValue={selectedTeam.description}/>
          <label>Specialty</label>
          <select onChange={handleChangeTeamInfo} name='specialty_id' defaultValue={selectedTeam.specialty.id}>
            {specialties.map(specialty =>{
              return(
                <option key={specialty.id} value={specialty.id}>{specialty.name}</option>
              )
            })}
          </select>
        </div>
        <div className="btnPop">
          <button onClick={saveTeamInfo}>Save</button>
        </div>
      </div>
    </Popup>:null
  }

  <button onClick={openTeamMembers}>Team Members</button>
  {
    teamMembersPop ?
  <Popup>
    <div className='PopResearch membersPop'>
      <div className="titlePop">
        <div className='titleWx'>
          <h3>Team Members</h3>
          <i onClick={closePopup} className="icon-cancel" aria-hidden="true"></i>
        </div>
        <hr/>
      </div>
      <div >
        <input onChange={handleChangeTeamInfo} value={inviteMemberId} placeholder='Invite a member by enter user id...'/>
        <button onClick={inviteMember}>Add</button>
      </div>
      <div className="memberCards">
        {selectedTeam.members.map(member=>
          <div key={member.id} className="card">
            <PostOwner popup author={member}/>
            <i onClick={(e) => removeUser(e, member.id)} className="icon-user-delete" aria-hidden="true"></i>
          </div>
        )}
      </div>
    </div>
  </Popup>:null
}
</div>
</div>

class ResearchTools extends Component {
  constructor(){
    super();
    this.state = {
      width: window.innerWidth,
      teams: [],
      selectedTeam:{},
      editTeamPop: false,
      editTeamInfo:{},
      inviteMemberId:null,
      specialties:[],
      teamMembersPop: false,
      teamOptionsOpened:false,
      myResearcOpened:false
    }
    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);
    this.openEditTeam = this.openEditTeam.bind(this);
    this.openTeamMembers = this.openTeamMembers.bind(this)
    this.handleChangeTeamInfo = this.handleChangeTeamInfo.bind(this)
    this.saveTeamInfo = this.saveTeamInfo.bind(this)
    this.closePopup = this.closePopup.bind(this)
    this.leaveTeam = this.leaveTeam.bind(this)
    this.removeUser = this.removeUser.bind(this)
    this.toggleMyResearch = this.toggleMyResearch.bind(this)
    this.toggleTeamOptions = this.toggleTeamOptions.bind(this)
    this.inviteMember = this.inviteMember.bind(this)
  }
  componentDidMount(){
    axios.get('/api/v1/specialties/')
    .then((response) => {
      this.setState({
        specialties:response.data
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
    this.setState({
      teams:this.props.teams,
      selectedTeam: this.props.selectedTeam,
    })
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.selectedTeam && nextProps.selectedTeam.specialty){
      let TeamInfo = {
        name:nextProps.selectedTeam? nextProps.selectedTeam.name:'',
        description: nextProps.selectedTeam? nextProps.selectedTeam.description:'',
        specialty_id: nextProps.selectedTeam? nextProps.selectedTeam.specialty.id:null}
      this.setState({
        teams:nextProps.teams,
        selectedTeam:nextProps.selectedTeam,
        editTeamInfo:TeamInfo
      })
    }
  }
  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };
  openEditTeam(){
    this.setState({
      editTeamPop:true,
    })
  }
  openTeamMembers(){
    this.setState({
      teamMembersPop:true
    })
  }
  handleChangeTeamInfo(event) {
    this.setState({inviteMemberId: event.target.value});
  }
  handleChangeInviteMemeber(event){
    let temp = this.state.editTeamInfo
    temp[event.target.name] = event.target.value
    this.setState({editTeamInfo: temp});
  }
  saveTeamInfo(){
    axios.put('/api/v1/research_teams/' + window.location.href.split('/').pop() ,{
      research_team:{
        name: this.state.editTeamInfo.name,
        description: this.state.editTeamInfo.description,
        specialty_id: this.state.editTeamInfo.specialty_id
      }
    })
    .then((response) => {
      this.props.updateTeam();
      this.setState({
        editTeamPop: false
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  closePopup(){
    this.setState({
      teamMembersPop:false,
      editTeamPop:false,
      createTeamPop:false
    })
  }
  leaveTeam(){
    axios.delete('/api/v1/research_teams/' + this.state.selectedTeam.id + '/leave')
    .then((response) => {
      this.props.updateTeam();
      this.setState({
        editTeamPop: false
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  removeUser(event, userId){
    axios.delete('/api/v1/research_teams/' + this.state.selectedTeam.id + '/members',{
      user_id:userId
    })
    .then((response) => {
      this.props.updateTeam();
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  inviteMember(){
    axios.post('/api/v1/research_teams/' + this.state.selectedTeam.id + '/members',{
      user_id:this.state.inviteMemberId
    })
    .then((response) => {
      this.props.updateTeam();
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  toggleMyResearch(event){
    event.preventDefault();
    this.setState({
      teamOptionsOpened:false
    })
    this.setState((prevState) => {
      return {myResearcOpened: !prevState.myResearcOpened};
    });
  }
  toggleTeamOptions(event){
    event.preventDefault();
    this.setState({
      myResearcOpened:false
    })
    this.setState((prevState) => {
      return {teamOptionsOpened: !prevState.teamOptionsOpened};
    });
  }
  render() {
    const { width } = this.state;
    const isMobile = width <= 768;
    if(isMobile){
      return (
        <div className="ResearchTools">
          <div className="responsiveBtns">
            <button className={this.state.myResearcOpened? "active" : null} onClick={this.toggleMyResearch}>My Research</button>
            <button className={this.state.teamOptionsOpened? "active" : null} onClick={this.toggleTeamOptions}>Team Options</button>
          </div>
          { this.props.selectedTeam?
            <TeamOptions
              teamMembersPop={this.state.teamMembersPop}
              openTeamMembers={this.openTeamMembers}
              saveTeamInfo={this.saveTeamInfo}
              specialties={this.state.specialties}
              openEditTeam={this.openEditTeam}
              closePopup={this.closePopup}
              handleChangeTeamInfo={this.handleChangeTeamInfo}
              editTeamPop={this.state.editTeamPop}
              selectedTeam={this.state.selectedTeam}
              leaveTeam={this.leaveTeam}
              removeUser={this.removeUser}
              inviteMember={this.inviteMember}
              inviteMemberId={this.inviteMemberId}
              active={this.state.teamOptionsOpened}
            /> : null
          }
          <MyResearch active={this.state.myResearcOpened} closePopup={this.closePopup}/>
        </div>
      );
    }
    else{
      return (
        <div className="ResearchTools">
          { this.props.selectedTeam?
            <TeamOptions
              teamMembersPop={this.state.teamMembersPop}
              openTeamMembers={this.openTeamMembers}
              saveTeamInfo={this.saveTeamInfo}
              specialties={this.state.specialties}
              openEditTeam={this.openEditTeam}
              closePopup={this.closePopup}
              handleChangeTeamInfo={this.handleChangeTeamInfo}
              editTeamPop={this.state.editTeamPop}
              selectedTeam={this.state.selectedTeam}
              leaveTeam={this.leaveTeam}
              removeUser={this.removeUser}
              inviteMember={this.inviteMember}
              inviteMemberId={this.inviteMemberId}
              active={true}/> :null
          }
          <MyResearch active={true} closePopup={this.closePopup}/>
        </div>
      );
    }
  }
}

export default ResearchTools;
