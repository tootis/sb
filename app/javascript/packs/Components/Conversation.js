import React, { Component } from 'react';
import axios from 'axios'
import './../Assets/css/Components/Conversation.css';
import BeePP from './../Assets/images/beePP.png'

class Message extends Component {
    render() {
        let { body, sender } = this.props;
        let mine = sender.id == localStorage.getItem("userId");
        
        return (
            <div className={mine ? "Message self" : "Message"}>
                {!mine &&
                    <img className="avatar" alt="avatar" src={BeePP} />
                }
                <span className="MessageText sb-text-brown">
                    {body}
                </span>
            </div>
        );
    }
}

class Conversation extends Component {
    state = {
        message: "",
    }

    handleChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    handleKeyPress = (e) => {
        if(e.key === 'Enter') {
            e.preventDefault();
            this.onSend();
        }
    }

    onSend = () => {
        this.props.newMessageHandler(this.state.message);
        this.setState({message: ""});
    }
    
    render() {
        return (
            <div className="discussionContainer">
                <div className="Conversation">
                    {this.props.discussion.messages && this.props.discussion.messages.map((msg) => (
                        <Message {...msg} key={msg.id} />
                    ))}

                </div>

                <div className="DiscussionInput">
                    <textarea
                        type="text"
                        name="message"
                        placeholder="Type a message ..."
                        value={this.state.message}
                        onChange={this.handleChange}
                        onKeyPress={this.handleKeyPress}
                    />

                    <button className="send" onClick={this.onSend} disabled={this.state.message.length === 0}>Send</button>
                </div>
            </div>
        );
    }
}

export default Conversation;