import React, { Component } from 'react';
import { confirmAlert } from 'react-confirm-alert';
import axios from 'axios'
import PropTypes from 'prop-types';
import UserHoverCard from './UserHoverCard'
import './../Assets/css/Components/Comments.css';

class Comments extends Component {
  constructor(props){
    super(props)
    this.state = {
      comments:[],
      commentText:'',
      activeEdits:null,
      editComment:null,
      showDetails : false,
      hoverId:null
    }
    this.handleCommentChange = this.handleCommentChange.bind(this)
    this.postComment = this.postComment.bind(this)
    this.upvotes = this.upvotes.bind(this)
    this.downvotes = this.downvotes.bind(this)
    this.activateEdits = this.activateEdits.bind(this)
    this.deleteComment = this.deleteComment.bind(this)
    this.editComment = this.editComment.bind(this)
    this.submitCommentEdit = this.submitCommentEdit.bind(this)
  }
  componentWillMount(){
    this.setState({
      comments:this.props.comments.reverse(),
    })
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      comments:nextProps.comments.reverse(),
    })
  }
  postComment(event){
    if(event.key === 'Enter'){
      axios.post('/api/v1/posts/'+ this.props.postId +'/comments',{
        content: this.state.commentText
      })
      .then((response) => {
        this.props.updatePosts();
        this.setState({commentText:''})
      })
      .catch(function (error) {
        console.log(error);
      });
    }
  }
  upvotes(event,id){
    axios.post('/api/v1/comments/'+ id +'/toggle_upvote')
    .then((response) => {
      this.props.updatePosts();
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  downvotes(event,id){
    axios.post('/api/v1/comments/'+ id +'/toggle_downvote')
    .then((response) => {
      this.props.updatePosts();
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  handleCommentChange(event){
    this.setState({commentText:event.target.value})
  }
  activateEdits(event, id){
    if(this.state.activeEdits === id){
      this.setState({activeEdits : null})
    }
    else{
      this.setState({activeEdits : id})
    }
  }
  deleteComment(event,id){
    confirmAlert({
      title: 'Delete Comment',
      message: 'Are you sure you want to delete this comment ?',
      cancelLabel: 'Cancel',
      confirmLabel: 'Confirm',
      onConfirm: () => {
        axios.delete('/api/v1/comments/'+ id)
        .then((response) => {
          this.props.updatePosts();
        })
        .catch(function (error) {
          console.log(error);
        });
      }
    })
  }
  editComment(event, id){
    this.setState({
      editComment:id,
      activeEdits:null
    })
  }
  submitCommentEdit(event, id){
    if(event.key === 'Enter'){
      axios.put('/api/v1/comments/' + id,{
        comment: {
                content: event.target.value
        }
      })
      .then((response) => {
        this.props.updatePosts();
        this.setState({editComment:null})
      })
      .catch(function (error) {
        console.log(error);
      });
    }
  }
  render() {
    return (
      <div className="Comments">
        <div className="textComments">
          <ul>
            {this.state.comments.map(comment =>{
              return(
                <li key={comment.id}>
                  <div className="commentContainer">
                    <div className={this.state.editComment === comment.id? "comment activeEdit" : "comment"}>
                      <a onMouseOver={() => this.setState({showDetails:true, hoverId:comment.id})} onMouseLeave={() => this.setState({showDetails:false, hoverId: null})} href={"/profile/" + comment.author.id}>{comment.author.first_name} {comment.author.last_name}</a>
                      {this.state.showDetails && this.state.hoverId == comment.id ? <UserHoverCard comment authorId={comment.author.id}/>:null}
                      <span>  {comment.content}</span>
                      <textarea defaultValue={comment.content} onKeyPress={(e) => this.submitCommentEdit(e, comment.id)}/>
                    </div>
                    <div className="commentVotes">
                      <div onClick={(e) => this.upvotes(e,comment.id)} className={comment.has_voted && comment.has_voted !== null? "vote active" : "vote"}>
                        <i className="icon-thumbs-up" aria-hidden="true"></i>
                        <p>{comment.votes_up}</p>
                      </div>
                      <div onClick={(e) => this.downvotes(e,comment.id)} className={!comment.has_voted && comment.has_voted !== null? "vote active" : "vote"}>
                        <i className="icon-thumbs-down" aria-hidden="true"></i>
                        <p>{comment.votes_down}</p>
                      </div>
                    </div>
                  </div>
                  <div className={Number(comment.author.id) === Number(localStorage.getItem("userId"))? "editsContainer active" : "editsContainer"}>
                    <i onClick={(e) => this.activateEdits(e, comment.id)} className="icon-ellipsis" aria-hidden="true"></i>
                    <ul className={this.state.activeEdits === comment.id? "edits active" : "edits"}>
                      <li onClick={(e) => this.editComment(e,comment.id)}>Edit</li>
                      <li onClick={(e) => this.deleteComment(e, comment.id)}>Delete</li>
                    </ul>
                  </div>
                </li>
              )
            })}
          </ul>
        </div>
        <div className="inputComment InputField">
          <i className="icon-comment-alt2" aria-hidden="true"></i>
          <textarea type="text" value={this.state.commentText} onKeyPress={this.postComment} onChange={this.handleCommentChange} placeholder="Write a comment..."/>
        </div>
      </div>
    );
  }
}
Comments.propTypes = {
  comments: PropTypes.array,
  postId: PropTypes.number
}
export default Comments;
