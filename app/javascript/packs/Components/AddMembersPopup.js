import React, { Component } from 'react';
import Popup from './../Components/Popup'
import PostOwner from './../Components/PostOwner'
import SearchUsers from './../Components/SearchUsers'
import '../Assets/css/Components/AddMembersPopup.css'


class AddMembersPopup extends Component {
    state = {
        newMembers: []
    }

    addMember = (member) => {
        this.setState((prevState, props) => {
            return {
                newMembers: [member, ...prevState.newMembers]
            };
        });
    }

    removeMember = (e, id) => {
        this.setState((prevState, props) => {
            let newMembers = prevState.newMembers.filter((member) => member.id != id);

            return {
                newMembers: newMembers
            }
        })
    }

    save = () => {
        this.props.onSave(this.state.newMembers);
        this.props.onClose();
    }

    render() {
        let excludeMembers = [...this.state.newMembers];
        if(this.props.members)
        excludeMembers = [...excludeMembers, ...this.props.members];
        
        const minAllowed = this.props.min ? this.props.min : 0;
        const maxAllowed = this.props.max ? this.props.max : Number.MAX_SAFE_INTEGER;
        const canSave = this.state.newMembers.length > minAllowed;

        return (
            <Popup>
                <div className='AddMembersPopup'>
                    <div className="titlePop">
                        <div className='titleWx'>
                        <h3>Add Members</h3>
                        <i onClick={this.props.onClose} className="icon-cancel" aria-hidden="true"></i>
                        </div>
                        <hr/>
                    </div>
                    <div className="bodyPop">
                        <SearchUsers
                            disabled={this.state.newMembers.length >= maxAllowed}
                            placeholder="Add members"
                            selectionHandler={this.addMember}
                            exclude={excludeMembers}
                        />
                        
                        <div className="memberCards">
                            {this.state.newMembers.map(member=>
                            <div key={member.id} className="card">
                                <PostOwner popup author={member}/>
                                <i onClick={(e) => this.removeMember(e, member.id)} className="icon-user-delete" aria-hidden="true"></i>
                            </div>
                            )}
                        </div>
                    </div>

                    {canSave &&
                        <div className="btnPop">
                            <button onClick={this.save}>Add</button>
                        </div>
                    }
                </div>
            </Popup>
        );
    }
}

export default AddMembersPopup;