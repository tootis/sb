import React, { Component } from 'react';
import axios from 'axios'
import Popup from './../Components/Popup'
import PostOwner from './../Components/PostOwner'
import SearchUsers from './../Components/SearchUsers'
import '../Assets/css/Components/NewDiscussionPopup.css'

const minMembersCount = 3;

class NewDiscussionPopup extends Component {
    state = {
        title: "",
        members: []
    }

    handleChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    addMember = (member) => {
        this.setState((prevState, props) => {
            return {
                members: [member, ...prevState.members]
            };
        });
    }

    removeMember = (e, id) => {
        this.setState((prevState, props) => {
            let newMembers = prevState.members.filter((member) => member.id != id);

            return {
                members: newMembers
            }
        })
    }

    createDiscussion = () => {
        axios.post('/api/v1/chat_rooms/', {
            chat_room: {
                title: this.state.title,
                participants_ids: this.state.members.map(member => member.id)
            }
        })
        .then(response => {
            if(response.data.errors) {
                response.data.errors.forEach(error => {
                    this.props.showAlert(error, "error");
                });
            } else {
                this.props.onNewDiscussionAdded(response.data);
                this.props.showAlert("Discussion created successfully", "success");
                this.props.onClose();
            }
        })
        .catch(error => {
            console.log(error);
        })
    }

    render() {
        let canCreate = this.state.members.length >= minMembersCount && this.state.title.length > 0;

        return (
            <Popup>
                <div className='PopDiscussion membersPop'>
                    <div className="titlePop">
                        <div className='titleWx'>
                        <h3>Create Discussion</h3>
                        <i onClick={this.props.onClose} className="icon-cancel" aria-hidden="true"></i>
                        </div>
                        <hr/>
                    </div>
                    <div className="bodyPop">
                        <label>Discussion Title</label>
                        <input type="text" name="title" value={this.state.title} onChange={this.handleChange} />
                        
                        <label>Please invite at least {minMembersCount} members</label>
                        
                        <SearchUsers
                            placeholder="Add to discussion"
                            selectionHandler={this.addMember}
                            exclude={this.state.members}
                        />
                        
                        <div className="memberCards">
                            {this.state.members.map(member=>
                            <div key={member.id} className="card">
                                <PostOwner popup author={member}/>
                                <i onClick={(e) => this.removeMember(e, member.id)} className="icon-user-delete" aria-hidden="true"></i>
                            </div>
                            )}
                        </div>
                    </div>
                    {canCreate &&
                        <div className="btnPop">
                            <button onClick={this.createDiscussion}>Create</button>
                        </div>
                    }
                </div>
            </Popup>
        );
    }
}

export default NewDiscussionPopup;