import React, { Component } from 'react';
import './../Assets/css/Components/NewsfeedArea.css';
import TextPost from './TextPost';
class NewsfeedArea extends Component {
  constructor(props){
    super(props);
    this.state = {
      posts: []
    }
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      posts:nextProps.posts,
    })
  }
  render() {
    return (
      <div className="NewsfeedArea">
        <div className="postsArea">
          {this.state.posts.map((post,index) =>
            <TextPost updatePosts={this.props.updatePosts} key={index} postData={post} onError={this.props.onError} />
          )}
        </div>
      </div>
    );
  }
}

export default NewsfeedArea;
