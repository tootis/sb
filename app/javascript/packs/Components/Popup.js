import React, { Component } from 'react';
import './../Assets/css/Components/Popup.css';

class Popup extends Component {
  render() {
      return (
        <div className= "Popup">
            {this.props.children}
        </div>
      );
  }
}

export default Popup;
