import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import PostOwner from './PostOwner';
import PostIcons from './PostIcons';
import Comments from './Comments';
import Tags from './Tags';
import './../Assets/css/Components/TextPost.css';

class TextPost extends Component {
  constructor(props){
    super(props);
    this.state = {
      editPostMode: false,
      editablePostData: {},
      specialties: [],
      specialtyId:0
    }
  }

  componentDidMount() {
    this.updateStatePost(this.props.postData);

    axios.get('/api/v1/fields/')
    .then((response) => {
      let specialityID = 0;
      if(Array.isArray(response.data) && response.data.length > 0 && Array.isArray(response.data[0].specialties) && response.data[0].specialties.length > 0)
        specialityID = response.data[0].specialties[0].id;
      this.setState({specialties: response.data, specialtyId: specialityID});
    })
    .catch((error) => {
      console.log(error)
    });
  }

  componentWillReceiveProps(nextProps) {
    this.updateStatePost(nextProps.postData)
  }
  
  updateStatePost = (oldPost) => {
    let post = Object.assign({}, oldPost);
    post['tags_string'] = post.tags.map(tag => tag.name).join();

    this.setState({
      editablePostData: post,
    });
  }

  toggleEditPost  = () => {
    this.setState({editPostMode: true});
  }

  handleChange = (e) => {
    let newData = Object.assign({}, this.state.editablePostData);
    newData[e.target.name] = e.target.value;

    this.setState({editablePostData: newData});

    if(e.target.name === "specialty_id")
      this.setState({specialtyId: e.target.value});
  }

  validate = () => {
    let errors = [];
    if(this.state.editablePostData.title.length < 4)
      errors.push("Title should be at least 4 characters.");
    if(this.state.editablePostData.content.length < 15)
      errors.push("Content should be at least 15 characters.");

    let re  = new RegExp(/^[0-9a-zA-Z ]+$/);
    if(this.state.editablePostData.title.length >= 4 && !re.test(this.state.editablePostData.title))
      errors.push("Only characters, spaces and numbers are allowed in post title.");      

    return errors;
  }

  savePost = () => {
    let errors = this.validate();
    if(errors.length > 0) {
      this.props.onError(errors);
      return;
    }

    axios.put("/api/v1/posts/" + this.props.postData.id, {
      post: this.state.editablePostData
    })
    .then(response => {
      this.props.updatePosts();
      this.setState({editPostMode: false});
    })
    .catch(error => {
      console.log(error)
    })
  }
  
  render() {
    let postType = function (type){
      switch (type) {
        case 'knowledge':
          return 'icon-spread'
        case 'question':
          return 'icon-question'
        default:
          return 'icon-question'
      }
    }

    let email = localStorage.getItem("uid");
    let re = new RegExp(/^.*@sciencebeehive.com$/);
    const showKnowledgeSpecialities = this.props.postData.type === "question" || re.test(email);

    return (
      <div className="TextPost">
        <PostOwner author={this.props.postData.author} specialty={this.props.postData.specialty}/>
        {!this.state.editPostMode ?
          <div>
            <i className={"typeIcon sb-text-brown fa " + postType(this.props.postData.type)} aria-hidden="true"></i>
            <h4 className="sb-text-brown">{this.props.postData.title}</h4>
            <p className="sb-text-brown">
              {this.props.postData.content}
            </p>
          </div>:
          <div className="editPost">
            <input name="title" placeholder="Title" value={this.state.editablePostData.title} onChange={this.handleChange} />
            <textarea name="content" placeholder="Content" value={this.state.editablePostData.content} onChange={this.handleChange} />

            {showKnowledgeSpecialities &&
              <select name="specialty_id" value={this.state.specialtyId}  onChange={this.handleChange} >
                {this.state.specialties.map(specialtyField =>{
                  return(
                  <optgroup key={specialtyField.name} label={specialtyField.name}>
                    {specialtyField.specialties.map(specialty =>{
                      return(
                        <option key={specialty.id} value={specialty.id}>{specialty.name}</option>
                      )
                    })}
                  </optgroup>
                )})}
              </select>
            }

            <input name="tags_string" placeholder="Tags" value={this.state.editablePostData.tags_string} onChange={this.handleChange} />

            <div className="spread">
              <button onClick={this.savePost}>Save</button>
            </div>
          </div>
         }
         <Tags tags={this.props.postData.tags}/>
        <PostIcons
          upvotes={this.props.postData.votes_up}
          downvotes={this.props.postData.votes_down}
          hasVoted={this.props.postData.has_voted}
          postId={this.props.postData.id}
          updatePosts={this.props.updatePosts}
          postTime={this.props.postData.created_at}
          authorId={this.props.postData.author.id}
          onEdit={this.toggleEditPost}
        />
        <Comments comments={this.props.postData.comments} updatePosts={this.props.updatePosts} postId={this.props.postData.id}/>
      </div>
    );
  }
}
TextPost.propTypes = {
  postData: PropTypes.object,
}

export default TextPost;
