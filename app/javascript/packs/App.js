import React, { Component } from 'react';
import Registration from './Views/Registration'
import Confirmation from './Views/Confirmation'
import ForgetPassword from './Views/ForgetPassword'
import Login from  './Views/Login'
import ProfilePage from './Views/ProfilePage'
import EditProfilePage from './Views/EditProfilePage'
import Homepage from './Views/Homepage'
import SinglePost from './Views/SinglePost'
import ResearchPage from './Views/ResearchPage'
import ResearchPageDefault from './Views/ResearchPageDefault'
import DiscussionPage from './Views/DiscussionPage'
import './App.css';
import './Assets/icons/fontello.css'
import {
  BrowserRouter as Router,
  Route,
  Redirect
} from 'react-router-dom'
import axios from 'axios'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    localStorage.getItem("access-token") && localStorage.getItem("client") && localStorage.getItem("uid") ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }}/>
    )
  )}/>
)

const LoggedRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    localStorage.getItem("access-token") && localStorage.getItem("client") && localStorage.getItem("uid") ? (
      <Redirect to={{
        pathname: '/my_beehive',
        state: { from: props.location }
      }}/>
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }}/>
    )
  )}/>
)

class App extends Component {

  componentDidMount() {
    axios.interceptors.response.use(undefined, error => {
      if(error.response && error.response.status === 401) {
        localStorage.clear();
        window.location.reload();
      }
    })
  }
  
  render() {
    return (
      <Router>
        <div className="App">
          <LoggedRoute exact path="/" component={Login}/>
          <Route path="/registration" component={Registration}/>
          <Route path="/login" component={Login}/>
          <Route path="/confirmation" component={Confirmation}/>
          <Route path="/ForgetPassword" component={ForgetPassword}/>
          <PrivateRoute path="/profile/:username" component={ProfilePage}/>
          <PrivateRoute path="/editprofile/" component={EditProfilePage}/>
          <PrivateRoute path="/my_beehive" component={Homepage}/>
          <PrivateRoute path="/local_beehive" component={Homepage}/>
          <PrivateRoute path="/specialty_beehive" component={Homepage}/>
          <PrivateRoute path="/global_beehive" component={Homepage}/>
          <PrivateRoute path="/posts/:id" component={SinglePost}/>
          <PrivateRoute exact path="/research_teams/:id" component={ResearchPage}/>
          <PrivateRoute exact path="/research_teams/" component={ResearchPageDefault}/>
          <PrivateRoute path="/discussion/:id?" component={DiscussionPage}/>
        </div>
      </Router>
    );
  }
}

export default App;
