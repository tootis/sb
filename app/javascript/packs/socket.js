import actionCable from 'actioncable'

const client = localStorage.getItem("client");
const accessToken = localStorage.getItem("access-token");
const uid = localStorage.getItem("uid");
const socketProtocol = window.location.protocol == "https:" ? "wss" : "ws";

const ActionCable = {
    cable: actionCable.createConsumer(`${socketProtocol}://${window.location.host}/cable?uid=${uid}&access-token=${accessToken}&client=${client}`),
    subscribeToChannel: (channelParams, events) => {
        return this.default.cable.subscriptions.create(channelParams, events);
    }
}

export default ActionCable;
