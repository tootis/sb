import React, { Component } from 'react';
import axios from 'axios'
import InputField from './../Components/InputField'
import AlertContainer from 'react-alert'
import './../Assets/css/Views/ForgetPassword.css';
import Logo from './../Assets/images/Views/InputFieldLogo.svg'

class ForgetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSend = this.onSend.bind(this);
    this.errorAlert = this.errorAlert.bind(this)
    this.successAlert = this.successAlert.bind(this)
  }

  alertOptions = {
    offset: 14,
    position: 'bottom left',
    theme: 'light',
    time: 5000,
    transition: 'scale'
  }
  errorAlert = (errors) => {
    if(Array.isArray(errors)){
      for(let error of errors){
        this.msg.show(error, {
          time: 5000,
          type: 'error',
          icon: <i className="icon-error errorRed" aria-hidden="true"></i>
        })
      }
    }
    else{
      this.msg.show('Something goes wrong!', {
        time: 5000,
        type: 'error',
        icon: <i className="icon-error errorRed" aria-hidden="true"></i>
      })
    }
  }
  successAlert = (updatedItem) => {
    this.msg.show(updatedItem + ' successfully updated!', {
      time: 5000,
      type: 'success',
      icon: <i className="icon-success successGreen" aria-hidden="true"></i>
    })
  }
  handleChange(event) {
    this.setState({email: event.target.value});
  }
  onSend(){
    axios.get('/api/v1/auth/password',{
      email:this.state.email
    })
    .then((response) => {
      window.location.href = '/login'
    })
    .catch((error) => {
      if (Array.isArray(error.response)) {
        this.errorAlert(error.response.data.errors.full_messages);
      }
      else {
        this.errorAlert();
      }
    });
  }
render() {
  return (
    <div className="ForgetPassword">
        <div className="logo">
          <img src={Logo} alt="SB-Logo"/>
        </div>
        <div className="messageContainer">
          <InputField label="Email" inputType="text" name="email" value={this.state.email} onChange={this.handleChange}/>
          <button onClick={this.onSend}>Send</button>
        </div>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
    </div>
  );
}
}

export default ForgetPassword;
