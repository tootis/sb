import React, { Component } from 'react';
import axios from 'axios'
import AlertContainer from 'react-alert'
import Navbar from './../Components/Navbar'
import Sidebar from './../Components/Sidebar'
import SpecialtyRequest from './../Components/SpecialityRequest'
import SpecialtySelect from './../Components/SpecialtySelect'
import './../Assets/css/Views/EditProfilePage.css';


const PersonalInfo = ({
  userData,
  handleFieldChange,
  onSubmit,
  selected_field,
  specialties,
  countries,
  onShowSpecialityRequest
}) =>
<div>
  <h2>Change Personal Info</h2>
  <hr/>
  <div className='dataLine'>
    <label>First Name:</label>
    <input value={userData.first_name} onChange={(e) => handleFieldChange(e,'first_name')} type="text"/>
  </div>
  <div className='dataLine'>
    <label>Last Name:</label>
    <input value={userData.last_name} onChange={(e) => handleFieldChange(e,'last_name')} type="text"/>
  </div>
  <div className='dataLine'>
    <label>Username:</label>
    <input value={userData.username} onChange={(e) => handleFieldChange(e,'username')} type="text"/>
  </div>
  <div className='dataLine'>
    <label>Country:</label>
    <select value={userData.country.value} onChange={(e) => handleFieldChange(e,'country')} >
      {
        countries.map(country =>{
          return(
            <option key={country.value} value={country.value}>{country.name}</option>
          )
        })
      }
    </select>
  </div>
  
  <SpecialtySelect showLabels onChange={handleFieldChange} selectedSpecialty={userData.specialty} />
  <div className='dataLine'>
    <a className="sb-text-brown requestLink" onClick={onShowSpecialityRequest}>Request new specialty</a>
  </div>

  <div className='dataLine'>
    <label>Description:</label>
    <textarea value={userData.description} onChange={(e) => handleFieldChange(e,'description')} type="text"/>
  </div>
  <div className='dataLine' >
    <label>Title:</label>
    <input value={userData.title || ""} onChange={(e) => handleFieldChange(e,'title')} type="text"/>
  </div>
  {/* <div className='dataLine'  >
    <label>Current password:</label>
    <input type="password" onChange={(e) => handleFieldChange(e,'password')} />
  </div> */}
  <div className="btnEdit">
    <button onClick={onSubmit}>Update Info</button>
  </div>
</div>

const ChangePassword = ({
  handlePasswordChange,
  onSubmit
}) =>
<div>
  <h2>Change Personal Info</h2>
  <hr/>
  <div className='dataLine'>
    <label>Password:</label>
    <input onChange={(e) => handlePasswordChange(e,'new') } type="password"/>
  </div>
  <div className='dataLine'>
    <label >Password Confirmation:</label>
    <input onChange={(e) => handlePasswordChange(e,'confirm')}  type="password"/>
  </div>
  {/* <div className='dataLine'>
    <label>Current password:</label>
    <input type="password" />
  </div> */}
  <div className="btnEdit">
    <button onClick={onSubmit}>Update Password</button>
  </div>
</div>

class EditProfilePage extends Component {
  constructor(props){
    super(props);
    this.state = {
      userData:{
        first_name:'',
        last_name:'',
        username:'',
        country:'',
        specialty:'',
        description:'',
        title:'',
      },
      specialties: {},
      field: '',
      countries: [],
      password:{new:'', confirm:''},
      showSpecialityRequestPopup: false
    }
    this.handleFieldChange = this.handleFieldChange.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
    this.updateInfo = this.updateInfo.bind(this)
    this.updatePassword = this.updatePassword.bind(this)
    this.errorAlert = this.errorAlert.bind(this)
    this.successAlert = this.successAlert.bind(this)
  }
  alertOptions = {
    offset: 14,
    position: 'bottom left',
    theme: 'light',
    time: 5000,
    transition: 'scale'
  }
  errorAlert = (errors) => {
    if(Array.isArray(errors)){
      for(let error of errors){
        this.msg.show(error, {
          time: 5000,
          type: 'error',
          icon: <i className="icon-error errorRed" aria-hidden="true"></i>
        })
      }
    }
    else{
      this.msg.show('Something goes wrong!', {
        time: 5000,
        type: 'error',
        icon: <i className="icon-error errorRed" aria-hidden="true"></i>
      })
    }
  }
  successAlert = (msg) => {
    this.msg.show(msg , {
      time: 5000,
      type: 'success',
      icon: <i className="icon-success successGreen" aria-hidden="true"></i>
    })
  }

  componentDidMount(){
    axios.defaults.headers.common['client'] = localStorage.getItem("client")
    axios.defaults.headers.common['access-token'] = localStorage.getItem("access-token")
    axios.defaults.headers.common['uid'] = localStorage.getItem("uid")
    axios.get('/api/v1/users/' + localStorage.getItem("userId"))
    .then((response) => {
      let responseData = response.data;
      axios.get('/api/v1/countries/')
      .then((countryResponse) => {
        let countryValue = countryResponse.data.find((countryName) => {
          return countryName.name === response.data.country;
        })
        this.setState({countries: countryResponse.data})
        // responseData.specialty = localStorage.getItem("specialty_id")
        responseData.country = countryValue ? countryValue : countryResponse.data[1]
        this.setState({userData: responseData})
      })
      .catch((error) => {
        if (error.response) {
          this.errorAlert(error.response.data.errors.full_messages);
        }
        else {
          this.errorAlert("Something went wrong!");
        }
      });

      axios.get('/api/v1/fields/')
      .then((response) => {
        let fields = {};
        response.data.forEach(field => {
          fields[field.name] = field.specialties;

          field.specialties.forEach(specialty => {
            if(specialty.id === responseData.specialty.id)
              this.setState({field: field.name});
          })
        });

        if(this.state.field === '' && response.data.length > 0)
          this.setState({field: response.data[0].name});

        this.setState({specialties: fields})
      })
      .catch((error) => {
        if (error.response) {
          this.errorAlert(error.response.data.errors.full_messages);
        }
        else {
          this.errorAlert(error.response.data.errors.full_messages);
        }
      });
    })
    .catch((error) => {
      if (error.response) {
        this.errorAlert(error.response.data.errors.full_messages);
      }
      else {
        this.errorAlert(error.response.data.errors.full_messages);
      }
    });
    

  }
  handleFieldChange(event, type){
    if(type === "field")
      this.setState({field: event.target.value})
    else
      this.setState({ userData: { ...this.state.userData, [type]: event.target.value } });
  }
  handlePasswordChange(event, type){
    this.setState({ password: { ...this.state.password, [type]: event.target.value } });
  }

  validate() {
    let errors = [];
    if(this.state.userData.first_name.length < 2)
      errors.push("First name must be more than 2 characters");
    if(this.state.userData.last_name.length < 2)
      errors.push("Last name must be more than 2 characters");
    if(this.state.userData.username.length < 5)
      errors.push("username must be more than 5 characters");
    

    let name_re = new RegExp(/^[a-zA-Z]+$/);
    if(!name_re.test(this.state.userData.first_name))
      errors.push("Only alphabetical characters are allowed in First name");
    if(!name_re.test(this.state.userData.last_name))
      errors.push("Only alphabetical characters are allowed in Last name");
      
    let username_re = new RegExp(/^[a-zA-Z0-9_.]+$/);
    if(!username_re.test(this.state.userData.username))
      errors.push("Invalid username, only alphabets, numbers, _ and dots are allowed");
    
    return errors;
  }

  updateInfo(){
    let errors = this.validate();    
    if(errors.length > 0) {
      this.errorAlert(errors);
    } else {
      axios.put('/api/v1/auth/',{
          'first_name': this.state.userData.first_name,
          'last_name': this.state.userData.last_name,
          'username':this.state.userData.username,
          'country':this.state.userData.country,
          'specialty_id':this.state.userData.specialty,
          'description': this.state.userData.description,
          'title': this.state.userData.title
      })
      .then((response) => {
        this.successAlert("Info successfully updated.");
      })
      .catch((error) => {
        if (error.response) {
          this.errorAlert(error.response.data.errors.full_messages);
        }
        else {
          this.errorAlert(error.response.data.errors.full_messages);
        }
      });
    }
  }
  updatePassword(){
    axios.put('/api/v1/auth/password/',{
        password: this.state.password.new,
        password_confirmation: this.state.password.confirm,
    })
    .then((response) => {
      this.successAlert("Password successfully updated.");
    })
    .catch((error) => {
      if (error.response.data.errors.full_messages) {
        this.errorAlert(error.response.data.errors.full_messages);
      }
      else {
        this.errorAlert();
      }
    });
  }

  showPopup = () => {
    console.log("show popup")
    this.setState({showSpecialityRequestPopup: true});
  }

  closePopup = () => {
    this.setState({showSpecialityRequestPopup: false});
  }
  
  render() {
    return (
      <div className="EditProfilePage">
        <div className="navContainer">
          <Navbar />
        </div>
        <div className="contentContainer">
          <Sidebar />
          <div className="editProfileContainer">
            <PersonalInfo
              userData={this.state.userData}
              handleFieldChange={this.handleFieldChange}
              selected_field={this.state.field}
              specialties={this.state.specialties}
              countries={this.state.countries}
              onShowSpecialityRequest={this.showPopup}
              onSubmit={this.updateInfo}
            />
            <ChangePassword onSubmit={this.updatePassword} handlePasswordChange={this.handlePasswordChange}/>
          </div>
        </div>
       <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />

       {this.state.showSpecialityRequestPopup && 
        <SpecialtyRequest onClose={this.closePopup} onSuccess={this.successAlert} onError={this.errorAlert} />
      }
      </div>
    );
  }
}


export default EditProfilePage;
