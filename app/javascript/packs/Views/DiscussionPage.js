import React, { Component } from 'react';
import axios from 'axios'
import AlertContainer from 'react-alert'
import ActionCable from './../socket'
import Navbar from './../Components/Navbar'
import Sidebar from './../Components/Sidebar'
import Conversation from './../Components/Conversation'
import DiscussionsList from './../Components/DiscussionsList'
import NewDiscussionPopup from './../Components/NewDiscussionPopup'
import AddMembersPopup from './../Components/AddMembersPopup'
import DiscussionMembers from './../Components/DiscussionMembers'
import './../Assets/css/Views/DiscussionPage.css';

const alertOptions = {
    offset: 14,
    position: 'bottom left',
    theme: 'light',
    time: 5000,
    transition: 'scale'
}

class DiscussionPage extends Component {
    state = {
        active: {},
        newDiscussionPop: false,
        addMembersPopup: false,
        showMembers: false,
        discussions: [],
        width: window.innerWidth
    };

    componentWillMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowSizeChange);
        this.unsubscribeFromRoom();
        this.unsubscribeFromChatNotifications();
    }
    
    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };

    componentDidMount() {
        axios.get('/api/v1/chat_rooms/')
        .then(response => {
            this.setState({discussions: response.data})
            if(Array.isArray(response.data) && response.data.length > 0)
                if(this.props.match.params.id) {
                    this.didChangeDiscussion(this.props.match.params.id);
                } else {
                    this.didChangeDiscussion(response.data[0].id);                    
                }
        })
        .catch(error => {
            console.log(error);
        });

        this.subscribeToChatNotifications();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({showMembers: false});
        
        if(nextProps.match.params.id)
            this.didChangeDiscussion(nextProps.match.params.id);
    }

    didChangeDiscussion = (id) => {
        axios.get('/api/v1/chat_rooms/' + id)
        .then(response => {
            this.setState({active: response.data});
            if(this.chatRoomChannel) {
                this.unsubscribeFromRoom();
            }

            this.subscribeToRoom(id);
        })
        .catch(error => {
            console.log(error);
        })
    }

    subscribeToRoom = (id) => {
        this.chatRoomChannel = ActionCable.subscribeToChannel({
            channel: "Api::V1::ChatRoomsChannel",
            chat_room_id: id
        }, {
            connected: () => {},
            disconnected: () => this.unsubscribeFromRoom(),
            received: this.onMessageReceived
        })
    }

    unsubscribeFromRoom = () => {
        this.chatRoomChannel.unsubscribe();
        // this.chatRoomChannel = undefined;
    }

    subscribeToChatNotifications = () => {
        this.chatRoomNotifications = ActionCable.subscribeToChannel({
            channel: "Api::V1::ChatNotificationsChannel"
        }, {
            connected: () => {},
            disconnected: () => this.unsubscribeFromChatNotifications(),
            received: this.onNotificationReceived
        })
    }

    unsubscribeFromChatNotifications = () => {
        this.chatRoomNotifications.unsubscribe();
        // this.chatRoomNotifications = undefined;
    }

    newDiscussionHandler = () => {
        this.setState({newDiscussionPop: true});
    }

    newDiscussionAdded = (discussion) => {
        this.setState((prevState, props) => {
            return {
                discussions: [discussion, ...prevState.discussions],
                active: discussion
            };
        })
    }

    onMessageReceived = (data) => {
        const msg = JSON.parse(data.message);

        this.setState((prevState, props) => {
            let discussion = Object.assign({}, prevState.active);
            discussion.messages = [msg, ...discussion.messages];

            return {
                active: discussion,
            };
        })
    }

    onMessageSend = (message) => {
        let timestamp = new Date().getTime();

        this.chatRoomChannel.perform('send_message', {
            chat_room_id: this.state.active.id,
            message_body: message,
            timestamp: timestamp
        });
    }

    onNotificationReceived = (data) => {
        const msg = JSON.parse(data.message);

        console.log(msg);
        this.setState((prevState) => {
            let notification_discussion = null;
            let discussions = prevState.discussions.slice();
            discussions = discussions.filter((discussion) => {
                if(discussion.id === msg.chat_room.id) {
                    notification_discussion = discussion;
                    return false;
                }
                return true;
            })

            notification_discussion.messages = msg;
            
            return {
                discussions: [notification_discussion, ...discussions]
            };
        })
    }

    onNewMember = () => {
        this.setState({addMembersPopup: true});
    }

    onMembersAdded = (members) => {
        axios.post("/api/v1/chat_rooms/" + this.state.active.id + "/participants", {
            user_id: members[0].id
        })
        .then(response => {
            this.setState((prevState, props) => {
                let discussion = Object.assign({}, prevState.active);
                discussion.participants = [...discussion.participants, ...members]            
    
                return {
                    active: discussion
                };
            })
        })
        .catch(error => {
            console.log(error.response);
        })
    }

    onRemoveMember = (id) => {
        if(this.state.active.participants.length <= 4) {
            this.showAlert("Discussion rooms should have at least 4 members, please add more members to be able to delete.", "error");
            return;
        }

        axios.delete("/api/v1/chat_rooms/" + this.state.active.id + "/participants", {
            params: {
                user_id: id
            }
        })
        .then(response => {
            this.setState((prevState, props) => {
                let discussion = Object.assign({}, prevState.active);
                discussion.participants = discussion.participants.filter((member) => member.id !== id);
    
                return {
                    active: discussion,
                };
            })
        })
        .catch(error => {
            console.log(error);
        })
    }
    
    onPopupClose = () => {
        this.setState({newDiscussionPop: false, addMembersPopup: false});
    }

    showMembers = () => {
        this.setState({showMembers: true})
    }

    hideMembers = () => {
        this.setState({showMembers: false})
    }

    showAlert = (text, messageType) => {
        this.msg.show(text, {
          type: messageType
        })
    }

    render() {
        const isMobile = this.state.width <= 768;
        const paramId = this.props.match.params.id;
        const showMembers = this.state.showMembers;
        
        return (
            <div className="DiscussionPage">
                <div className="navContainer">
                    <Navbar />
                </div>
                <div className={isMobile ? "contentContainer mobile" : "contentContainer"}>
                    <Sidebar />

                    {((isMobile && !paramId) || !isMobile) &&
                        <DiscussionsList
                            discussions={this.state.discussions}
                            active={this.state.active}
                            onNewDiscussion={this.newDiscussionHandler}
                        />
                    }

                    {((isMobile && paramId && !showMembers) || !isMobile) &&
                        <div className="middle-section">
                            <div className="title">
                                <label>{this.state.active.title}</label>

                                {isMobile &&
                                    <i className="icon-ellipsis-vert" onClick={this.showMembers} />
                                }
                            </div>
                            
                            <Conversation discussion={this.state.active} newMessageHandler={this.onMessageSend} />
                        </div>
                    }

                    {((isMobile && paramId && showMembers) || !isMobile) &&
                        <DiscussionMembers
                            discussion={this.state.active}
                            removeMemberHandler={this.onRemoveMember}
                            newMemberHandler={this.onNewMember}
                            showBack={isMobile}
                            onBack={this.hideMembers}
                        />
                    }
                </div>

                {this.state.newDiscussionPop &&
                    <NewDiscussionPopup
                        onNewDiscussionAdded={this.newDiscussionAdded}
                        onClose={this.onPopupClose}
                        showAlert={this.showAlert}
                    />
                }

                {this.state.addMembersPopup &&
                    <AddMembersPopup
                        members={this.state.active.participants}
                        max={1}
                        onClose={this.onPopupClose}
                        onSave={this.onMembersAdded}
                        showAlert={this.showAlert}
                    />
                }

                <AlertContainer {...alertOptions} ref={a => this.msg = a} />                
            </div>
        );
    }
}

export default DiscussionPage