import React, { Component } from 'react';
import axios from 'axios'
import Navbar from './../Components/Navbar'
import Sidebar from './../Components/Sidebar'
import NewsfeedArea from './../Components/NewsfeedArea'
import News from './../Components/News'
import './../Assets/css/Views/Homepage.css';
class Homepage extends Component {
  constructor(props){
    super(props);
    this.state = {
      posts: []
    }
  }
  componentDidMount(){
    axios.defaults.headers.common['client'] = localStorage.getItem("client")
    axios.defaults.headers.common['access-token'] = localStorage.getItem("access-token")
    axios.defaults.headers.common['uid'] = localStorage.getItem("uid")
    axios.get('/api/v1/posts/' + window.location.href.split('/').pop())
    .then((response) => {
      let postsTemp = [];
      postsTemp.push(response.data)
      this.setState({
        posts: postsTemp
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  componentWillReceiveProps(nextProps){
    this.updatePosts();
  }
  updatePosts(){
    axios.get('/api/v1/posts/' + window.location.href.split('/').pop())
    .then((response) => {
      let postsTemp = [];
      postsTemp.push(response.data)
      this.setState({
        posts: postsTemp
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  render() {
    return (
      <div className="Homepage">
        <div className="navContainer">
          <Navbar />
        </div>
        <div className="contentContainer">
          <Sidebar />
          <div className="postsContainer">
            <NewsfeedArea updatePosts={this.updatePosts} posts={this.state.posts}/>
          </div>
          <News />
        </div>
      </div>
    );
  }
}

export default Homepage;
