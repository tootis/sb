import React, { Component } from 'react';
import axios from 'axios'
import AlertContainer from 'react-alert'
import Navbar from './../Components/Navbar'
import Sidebar from './../Components/Sidebar'
import ProfileDetails from './../Components/ProfileDetails'
import ProfileDescription from './../Components/ProfileDescription'
import TextPost from './../Components/TextPost'
import './../Assets/css/Views/ProfilePage.css';


class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {},
      userPosts:[]
    };
    this.errorAlert = this.errorAlert.bind(this)
    this.successAlert = this.successAlert.bind(this)
  }
  alertOptions = {
    offset: 14,
    position: 'bottom left',
    theme: 'light',
    time: 5000,
    transition: 'scale'
  }
  errorAlert = (errors) => {
    if(Array.isArray(errors)){
      for(let error of errors){
        this.msg.show(error, {
          time: 5000,
          type: 'error',
          icon: <i className="icon-error errorRed" aria-hidden="true"></i>
        })
      }
    }
    else{
      this.msg.show('Something goes wrong!', {
        time: 5000,
        type: 'error',
        icon: <i className="icon-error errorRed" aria-hidden="true"></i>
      })
    }
  }
  successAlert = (updatedItem) => {
    this.msg.show(updatedItem + ' successfully updated!', {
      time: 5000,
      type: 'success',
      icon: <i className="icon-success successGreen" aria-hidden="true"></i>
    })
  }
  componentDidMount(){
    axios.defaults.headers.common['client'] = localStorage.getItem("client")
    axios.defaults.headers.common['access-token'] = localStorage.getItem("access-token")
    axios.defaults.headers.common['uid'] = localStorage.getItem("uid")
    axios.get('/api/v1/users/' + window.location.href.split('/').pop())
    .then((response) => {
      this.setState({userData: response.data})
    })
    .catch(function (error) {
      this.errorAlert();
    });

    this.fetchPosts();
  }

  fetchPosts = () => {
    axios.get('/api/v1/users/' + window.location.href.split('/').pop() + '/posts')
    .then((response) => {
      this.setState({userPosts: response.data})
    })
    .catch(function (error) {
      this.errorAlert();
    });
  }
  
  render() {
    return (
      <div className="ProfilePage">
        <div className="navContainer">
          <Navbar />
        </div>
        <div className="contentContainer">
          <Sidebar />
          <div className="profileContainer">
            <ProfileDetails data={this.state.userData}/>
            <ProfileDescription description={this.state.userData.description}/>
            {this.state.userPosts.map((post,index) =>
              <TextPost key={index} updatePosts={this.fetchPosts} postData={post} onError={this.errorAlert}/>
            )}
          </div>
        </div>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
      </div>
    );
  }
}


export default ProfilePage;
