import React, { Component } from 'react';
import axios from 'axios'
import Navbar from './../Components/Navbar'
import AlertContainer from 'react-alert'
import Sidebar from './../Components/Sidebar'
import NewsfeedArea from './../Components/NewsfeedArea'
import PostingTools from './../Components/PostingTools'
import ResearchTools from './../Components/ResearchTools'
import './../Assets/css/Views/ResearchPage.css';
class ResearchPage extends Component {
  constructor(props){
    super(props);
    this.state = {
      posts: [],
      teams: [],
      selectedTeam:{},
      width: window.innerWidth
    }
    this.updatePosts = this.updatePosts.bind(this)
    this.updateTeam = this.updateTeam.bind(this)
    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this)
    this.onPost = this.onPost.bind(this)
  }
  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };
  componentDidMount(){
    axios.defaults.headers.common['client'] = localStorage.getItem("client")
    axios.defaults.headers.common['access-token'] = localStorage.getItem("access-token")
    axios.defaults.headers.common['uid'] = localStorage.getItem("uid")
    axios.get('/api/v1/research_teams/')
    .then((response) => {
      this.setState({
        teams: response.data
      })
    })
    .catch(function (error) {
      console.log(error);
    });
    axios.get('/api/v1/research_teams/' + window.location.href.split('/').pop() + '/posts')
    .then((response) => {
      this.setState({
        posts: response.data
      })
    })
    .catch(function (error) {
      console.log(error);
    });
    axios.get('/api/v1/research_teams/' + window.location.href.split('/').pop())
    .then((response) => {
      this.setState({
        selectedTeam: response.data
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  componentWillReceiveProps(nextProps){
    this.updatePosts();
    this.updateTeam();
  }
  updatePosts(){
      axios.get('/api/v1/research_teams/' + window.location.href.split('/').pop() + '/posts')
      .then((response) => {
        this.setState({
          posts: response.data
        })
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  updateTeam(){
    axios.get('/api/v1/research_teams/' + window.location.href.split('/').pop())
    .then((response) => {
      this.setState({
        selectedTeam: response.data
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  errorAlert = (errors) => {
    if(Array.isArray(errors)){
      for(let error of errors){
        this.msg.show(error, {
          time: 5000,
          type: 'error',
          icon: <i className="icon-error errorRed" aria-hidden="true"></i>
        })
      }
    }
    else{
      this.msg.show('Something goes wrong!', {
        time: 5000,
        type: 'error',
        icon: <i className="icon-error errorRed" aria-hidden="true"></i>
      })
    }
  }

  successAlert = (msg) => {
    this.msg.show(msg , {
      time: 5000,
      type: 'success',
      icon: <i className="icon-success successGreen" aria-hidden="true"></i>
    })
  }

  onPost(title, type, content, tags_string, specialtyId){
    axios.post('/api/v1/research_teams/' + this.state.selectedTeam.id + '/posts/',{
      post:{
        title: title,
        type: type,
        content: content,
        specialty_id: specialtyId,
        tags_string: tags_string
      }
    })
    .then((response) => {
      this.updatePosts();
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  alertOptions = {
    offset: 14,
    position: 'bottom left',
    theme: 'light',
    time: 5000,
    transition: 'scale'
  }

  render() {
    const { width } = this.state;
    const isMobile = width <= 768;
    if(isMobile){
      return (
        <div className="ResearchPage">
          <div className="navContainer">
            <Navbar />
          </div>
          <div className="contentContainer">
            <Sidebar />
            <div className="researchContentResponsive">
              <ResearchTools updateTeam={this.updateTeam} selectedTeam={this.state.selectedTeam} teams={this.state.teams}/>
              <div className="postsContainer">
                <PostingTools updatePosts={this.updatePosts} onPost={this.onPost} onError={this.errorAlert}/>
                <NewsfeedArea updatePosts={this.updatePosts} posts={this.state.posts} onError={this.errorAlert}/>
              </div>
            </div>
          </div>
          
          <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />        
        </div>
      );
    }
    else{
      return (
        <div className="ResearchPage">
          <div className="navContainer">
            <Navbar />
          </div>
          <div className="contentContainer">
            <Sidebar />
            <div className="postsContainer">
                <PostingTools updatePosts={this.updatePosts} onPost={this.onPost} onError={this.errorAlert} />
                <NewsfeedArea updatePosts={this.updatePosts} posts={this.state.posts} onError={this.errorAlert}/>
            </div>
            <ResearchTools updateTeam={this.updateTeam} selectedTeam={this.state.selectedTeam} teams={this.state.teams}/>
          </div>

          
          <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />        
        </div>
      );
    }
  }
}

export default ResearchPage;
