import React, { Component } from 'react';
import axios from 'axios'
import Navbar from './../Components/Navbar'
import Sidebar from './../Components/Sidebar'
import ResearchTools from './../Components/ResearchTools'
import './../Assets/css/Views/ResearchPage.css';
class ResearchPageDefault extends Component {
  constructor(props){
    super(props);
    this.state = {
      teams: [],
      width: window.innerWidth
    }
    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);
  }
  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };
  componentDidMount(){
    axios.defaults.headers.common['client'] = localStorage.getItem("client")
    axios.defaults.headers.common['access-token'] = localStorage.getItem("access-token")
    axios.defaults.headers.common['uid'] = localStorage.getItem("uid")
    axios.get('/api/v1/research_teams/')
    .then((response) => {
      this.setState({
        teams: response.data
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  render() {
    const { width } = this.state;
    const isMobile = width <= 768;
    if(isMobile){
      return (
        <div className="ResearchPage">
          <div className="navContainer">
            <Navbar />
          </div>
          <div className="contentContainer">
            <Sidebar />
            <div className="researchContentResponsive">
              <ResearchTools teams={this.state.teams}/>
              <div className="postsContainer">
                  <p className="selectTeamMsg">Please Select Research Team</p>
              </div>
            </div>
          </div>
        </div>
      );
    }
    else{
      return (
        <div className="ResearchPage">
          <div className="navContainer">
            <Navbar />
          </div>
          <div className="contentContainer">
            <Sidebar />
            <div className="postsContainer">
                <p className="selectTeamMsg">Please Select Research Team</p>
            </div>
            <ResearchTools teams={this.state.teams}/>
          </div>
        </div>
      );
    }
  }
}

export default ResearchPageDefault;
