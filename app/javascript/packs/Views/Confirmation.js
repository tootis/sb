import React, { Component } from 'react';
import './../Assets/css/Components/Confirmation.css';
import Logo from './../Assets/images/Views/InputFieldLogo.svg'

class Confirmation extends Component {
componentDidMount() {
  setTimeout(() => window.location.href = '/', 3000)
}
render() {
  return (
    <div className="Confirmation">
        <div className="logo">
          <img src={Logo} alt="SB-Logo"/>
        </div>
        <div className="messageContainer">
            <h4 className="confirmationMessage">Thanks for registering to Science Beehive, a confirmation mail has been sent to you, please check your mail and follow the instructions.</h4>
        </div>
    </div>
  );
}
}

export default Confirmation;
