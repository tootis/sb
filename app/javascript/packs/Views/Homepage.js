import React, { Component } from 'react';
import axios from 'axios'
import AlertContainer from 'react-alert'
import Navbar from './../Components/Navbar'
import Sidebar from './../Components/Sidebar'
import NewsfeedArea from './../Components/NewsfeedArea'
import PostingTools from './../Components/PostingTools'
import News from './../Components/News'
import './../Assets/css/Views/Homepage.css';
class Homepage extends Component {
  constructor(props){
    super(props);
    this.state = {
      posts: []
    }
    this.updatePosts = this.updatePosts.bind(this)
    this.onPost = this.onPost.bind(this)
  }
  componentDidMount(){
    axios.defaults.headers.common['client'] = localStorage.getItem("client")
    axios.defaults.headers.common['access-token'] = localStorage.getItem("access-token")
    axios.defaults.headers.common['uid'] = localStorage.getItem("uid")
    axios.get('/api/v1/posts/' + window.location.href.split('/').pop())
    .then((response) => {
      this.setState({
        posts: response.data
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  updatePosts(){
    axios.get('/api/v1/posts/' + window.location.href.split('/').pop())
    .then((response) => {
      this.setState({
        posts: response.data
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  errorAlert = (errors) => {
    if(Array.isArray(errors)){
      for(let error of errors){
        this.msg.show(error, {
          time: 5000,
          type: 'error',
          icon: <i className="icon-error errorRed" aria-hidden="true"></i>
        })
      }
    }
    else{
      this.msg.show('Something goes wrong!', {
        time: 5000,
        type: 'error',
        icon: <i className="icon-error errorRed" aria-hidden="true"></i>
      })
    }
  }

  successAlert = (msg) => {
    this.msg.show(msg , {
      time: 5000,
      type: 'success',
      icon: <i className="icon-success successGreen" aria-hidden="true"></i>
    })
  }
  
  onPost(title, type, content, tags_string,specialtyId){
    if(type == 'knowledge'){
      let email = localStorage.getItem('uid');
      let re = new RegExp(/^.*@sciencebeehive.com$/);
      if(!re.test(email))
        specialtyId = localStorage.getItem('specialty_id');
    }
    axios.post('/api/v1/posts/',{
      post:{
        title: title,
        type: type,
        content: content,
        specialty_id: specialtyId,
        tags_string: tags_string
      }
    })
    .then((response) => {
      this.updatePosts();
      this.successAlert("Post added successfully.");
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  alertOptions = {
    offset: 14,
    position: 'bottom left',
    theme: 'light',
    time: 5000,
    transition: 'scale'
  }

  render() {
    return (
      <div className="Homepage">
        <div className="navContainer">
          <Navbar />
        </div>
        <div className="contentContainer">
          <Sidebar />
          <div className="postsContainer">
            <PostingTools updatePosts={this.updatePosts} onPost={this.onPost} onError={this.errorAlert} />
            <NewsfeedArea updatePosts={this.updatePosts} posts={this.state.posts} onError={this.errorAlert} />
          </div>
          <News />
        </div>

       <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />        
      </div>
    );
  }
}

export default Homepage;
