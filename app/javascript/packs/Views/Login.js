import React, { Component } from 'react';
import axios from 'axios'
import AlertContainer from 'react-alert'
import InputField from './../Components/InputField'
import './../Assets/css/Views/Login.css';
import Logo from './../Assets/images/Views/InputFieldLogo.svg'
import uniBg from './../Assets/images/uni-bg.svg'
import Bee from './../Assets/images/bee.svg'
import cloud1 from './../Assets/images/clouds/cloud1.svg'
import cloud2 from './../Assets/images/clouds/cloud2.svg'
import cloud3 from './../Assets/images/clouds/cloud3.svg'
import cloud4 from './../Assets/images/clouds/cloud4.svg'
import cloud5 from './../Assets/images/clouds/cloud5.svg'
import cloud6 from './../Assets/images/clouds/cloud6.svg'
import { Link } from 'react-router-dom'

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password:'',
      bees:[]
    };
    this.handleChange = this.handleChange.bind(this);
    this.login = this.login.bind(this);
    this.register = this.register.bind(this);
    this.errorAlert = this.errorAlert.bind(this)
    this.successAlert = this.successAlert.bind(this)
  }

  alertOptions = {
    offset: 14,
    position: 'bottom left',
    theme: 'light',
    time: 5000,
    transition: 'scale'
  }
  errorAlert = (errors) => {
    if(Array.isArray(errors)){
      for(let error of errors){
        this.msg.show(error, {
          time: 5000,
          type: 'error',
          icon: <i className="icon-error errorRed" aria-hidden="true"></i>
        })
      }
    }
    else{
      this.msg.show('Something goes wrong!', {
        time: 5000,
        type: 'error',
        icon: <i className="icon-error errorRed" aria-hidden="true"></i>
      })
    }
  }
  successAlert = (updatedItem) => {
    this.msg.show(updatedItem + ' successfully updated!', {
      time: 5000,
      type: 'success',
      icon: <i className="icon-success successGreen" aria-hidden="true"></i>
    })
  }
  componentWillMount(){
    let numberOfBees = Math.floor((Math.random() * 15) + 10);
    let beesArr = this.state.bees;
    for(let i = 0; i < numberOfBees; i++){
      let bee = {
        key: i,
        size: Math.floor((Math.random() * 20) + 10),
        top: Math.floor((Math.random() * window.innerHeight) + 1),
        left: Math.floor((Math.random() * window.innerWidth) + 1),
        rotate: Math.floor((Math.random() * 45) + 1),
        flip: Math.random() < 0.5 ? -1 : 1
      }
      beesArr.push(bee);
    }
    this.setState({
      bees:beesArr
    })
  }
  handleChange(event) {
    this.setState({[event.target.name]: event.target.value});
  }

  login(event){
    event.preventDefault();
    axios.post('/api/v1/auth/sign_in',{
      email: this.state.email,
      password: this.state.password
    })
    .then(function (response) {
      localStorage.setItem("access-token", response.headers['access-token']);
      localStorage.setItem("client", response.headers.client);
      localStorage.setItem("uid", response.headers.uid);
      localStorage.setItem("userId", response.data.data.id);
      localStorage.setItem("username", response.data.data.username);
      localStorage.setItem("specialty_id", response.data.data.specialty_id);
      window.location.href = '/profile/' + response.data.data.id
    })
    .catch((error) => {
      if (error.response.data.errors) {
        this.errorAlert(error.response.data.errors);
      }
      else {
        this.errorAlert();
      }
    });
  }

  register(){
    window.location.href = '/registration'
  }

render() {
  return (
    <div className="Login">
      <div className="loginForm">
        <div className="logo">
          <img src={Logo} alt="SB-Logo"/>
        </div>
        <form onSubmit={this.login}>
        <InputField label="Username" inputType="text" name="email" value={this.state.email} onChange={this.handleChange}/>
        <InputField label="Password" inputType="password" name="password" value={this.state.password} onChange={this.handleChange}/>
        <p className="sb-text-brown"><Link to="/ForgetPassword">Forgot Password?</Link></p>
        <div className="loginbtn">
          <button type='submit' className="btn-brown">Login</button>
          <button className="btn-brown" onClick={this.register}>Sign up</button>
        </div>
        </form>
      </div>
      <img src={uniBg} alt="uni-bg"/>
      <div className="bees-bg">
        {this.state.bees.map(bee =>{
          return(
          <img key={bee.key}
               src={Bee}
               width={bee.size + "px"}
               style={{top: bee.top + "px", left: bee.left + "px", transform: "scaleX("+ bee.flip +") rotate(" + bee.rotate + "deg)"}}
               alt="bee"/>
      )})}
      <img className="cloud1" src={cloud1} alt="cloud1"/>
      <img className="cloud2" src={cloud2} alt="cloud2"/>
      <img className="cloud3" src={cloud3} alt="cloud3"/>
      <img className="cloud4" src={cloud4} alt="cloud4"/>
      <img className="cloud5" src={cloud5} alt="cloud5"/>
      <img className="cloud6" src={cloud6} alt="cloud6"/>
      </div>
      <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
    </div>
  );
}
}

export default Login;
