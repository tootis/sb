//#region Imports
import React, { Component } from 'react';
import axios from 'axios'
import InputField from './../Components/InputField'
import './../Assets/css/Views/Registration.css';
import Logo from './../Assets/images/Views/InputFieldLogo.svg'
import AlertContainer from 'react-alert'
import uniBg from './../Assets/images/uni-bg.svg'
import Bee from './../Assets/images/bee.svg'
import cloud1 from './../Assets/images/clouds/cloud1.svg'
import cloud2 from './../Assets/images/clouds/cloud2.svg'
import cloud3 from './../Assets/images/clouds/cloud3.svg'
import cloud4 from './../Assets/images/clouds/cloud4.svg'
import cloud5 from './../Assets/images/clouds/cloud5.svg'
import cloud6 from './../Assets/images/clouds/cloud6.svg'
//#endregion

class Registration extends Component {
  //#region Constructor
  constructor(props) {
    super(props);
    this.state = {
      first_name:'',
      last_name:'',
      email: '',
      password:'',
      password_confirmation:'',
      full_messages:'',
      bees:[]
    };
    this.handleChange = this.handleChange.bind(this);
    this.validate = this.validate.bind(this);
    this.Register = this.Register.bind(this);
    this.showAlert = this.showAlert.bind(this);
    this.mailCheck = this.mailCheck.bind(this);
  }
  //#endregion
  componentWillMount(){
    let numberOfBees = Math.floor((Math.random() * 15) + 10);
    let beesArr = this.state.bees;
    for(let i = 0; i < numberOfBees; i++){
      let bee = {
        key: i,
        size: Math.floor((Math.random() * 20) + 10),
        top: Math.floor((Math.random() * window.innerHeight) + 1),
        left: Math.floor((Math.random() * window.innerWidth) + 1),
        rotate: Math.floor((Math.random() * 45) + 1),
        flip: Math.random() < 0.5 ? -1 : 1
      }
      beesArr.push(bee);
    }
    this.setState({
      bees:beesArr
    })
  }
  //#region Binded Functions
  showAlert = (text, messageType) => {
    this.msg.show(text, {
      time: 5000,
      type: messageType
    })
  }

  handleChange(event) {
    this.setState({[event.target.name]: event.target.value});
  }

  validate() {
    let errors = [];
    if(this.state.password.length < 8)
      errors.push("Password shouldn't be less than 8 characters long.");
    if(this.state.password !== this.state.password_confirmation)
      errors.push("Passwords don't match.");
    if(this.state.first_name.length < 2)
      errors.push("First name must be more than 2 characters");
    if(this.state.last_name.length < 2)
      errors.push("Last name must be more than 2 characters");

    let re = new RegExp(/^[a-zA-Z]+$/);
    if(!re.test(this.state.first_name))
      errors.push("Only alphabetical characters are allowed in First name");
    if(!re.test(this.state.last_name))
      errors.push("Only alphabetical characters are allowed in Last name");
    return errors;
  }

  Register(event){
    event.preventDefault();
    let errors = this.validate();
    if(errors.length > 0) {
      errors.forEach(err => {
        this.showAlert(err ,'error');
      });
    } else {
      axios.post('/api/v1/auth',{
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        email: this.state.email,
        password: this.state.password,
        password_confirmation: this.state.password_confirmation
      })
      .then((response)=>{
        localStorage.setItem("access-token", response.headers['access-token']);
        localStorage.setItem("client", response.headers.client);
        localStorage.setItem("uid", response.headers.uid);
        localStorage.setItem("userId", response.data.data.id);
        localStorage.setItem("username", response.data.data.username);
        localStorage.setItem("specialty_id", response.data.data.specialty_id);
        window.location.href = '/editprofile'
      }
      )
      .catch((error)=> {
        console.log(error.response.data.errors.full_messages[0])
        if(error.response.data)
          this.showAlert(error.response.data.errors.full_messages[0] ,'error');
      });
    }
  }

  mailCheck(response){
    window.location.href = '/confirmation'
  }
  //#endregion

//#region Render Function

render() {
  return (
    <div className="registration">
      <div className="registrationForm">
        <div className="logo">
          <img src={Logo} alt="SB-Logo"/>
        </div>
        <form onSubmit={this.Register}>
        <InputField label="First Name" inputType="text" name="first_name" value={this.state.first_name} onChange={this.handleChange}/>
        <InputField label="Last Name" inputType="text" name="last_name" value={this.state.last_name} onChange={this.handleChange}/>
        <InputField label="E-mail" inputType="text" name="email" value={this.state.email} onChange={this.handleChange}/>
        <InputField label="Password" inputType="password" name="password" value={this.state.password} onChange={this.handleChange}/>
        <InputField label="Re-enter Password" inputType="password" name="password_confirmation" value={this.state.password_confirmation}
        onChange={this.handleChange}/>
        <div className="registrationbtn">
          <AlertContainer ref={a => this.msg = a} {...this.alertOptions} offset={14} position='bottom left' theme='dark' time={4000}
           transition = 'scale'/>
          <button type='submit' className="btn-brown" >Sign Up</button>
        </div>
        </form>
      </div>
      <img src={uniBg} alt="uni-bg"/>
      <div className="bees-bg">
        {this.state.bees.map(bee =>{
          return(
          <img key={bee.key}
               src={Bee}
               width={bee.size + "px"}
               style={{top: bee.top + "px", left: bee.left + "px", transform: "scaleX("+ bee.flip +") rotate(" + bee.rotate + "deg)"}}
               alt="bee"/>
      )})}
      <img className="cloud1" src={cloud1} alt="cloud1"/>
      <img className="cloud2" src={cloud2} alt="cloud2"/>
      <img className="cloud3" src={cloud3} alt="cloud3"/>
      <img className="cloud4" src={cloud4} alt="cloud4"/>
      <img className="cloud5" src={cloud5} alt="cloud5"/>
      <img className="cloud6" src={cloud6} alt="cloud6"/>
      </div>
    </div>
  );
}
//#endregion
}

export default Registration;
