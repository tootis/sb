json.(post, :id, :title, :type, :content, :created_at)

json.author do
  json.partial! 'api/v1/users/common_info', user: post.author
end

json.specialty do
  json.partial! 'api/v1/specialties/specialty', specialty: post.specialty
end

json.research_team_id post.research_team_id if post.research_team

json.partial! 'api/v1/shared/votable', votable: post

json.tags post.tags, :id, :name

json.comments_count post.comments.count

json.comments post.comments.paginate(page: 1), partial: 'api/v1/comments/comment', as: :comment
