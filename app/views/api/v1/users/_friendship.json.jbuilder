json.is_friend current_user.friend_with?(user)
json.is_friend_request_sent current_user.sent_friend_request_to?(user)
json.is_friend_request_received current_user.received_friend_request_from?(user)
