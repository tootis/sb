json.partial! 'common_info', user: @user
json.description @user.description

if @user.specialty
  json.specialty do
    json.partial! 'api/v1/specialties/specialty', specialty: @user.specialty
  end
else
  json.specialty { json.id nil }
end

json.country ISO3166::Country[@user.country].try :name
json.followers_count @user.followers.count
json.votes_count @user.posts.map{|p| p.votes_for.up.size}.reduce(:+) # TODO: cache this

if current_user != @user
  json.partial! 'friendship', user: @user
  json.partial! 'followship', user: @user
end
