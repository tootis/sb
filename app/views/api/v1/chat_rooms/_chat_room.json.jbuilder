json.partial! 'chat_room_info', chat_room: chat_room
json.messages @messages || chat_room.messages.first, partial: 'message', as: :message
