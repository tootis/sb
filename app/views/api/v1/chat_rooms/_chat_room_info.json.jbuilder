json.(chat_room, :id, :title, :created_at, :updated_at)
json.participants chat_room.participants, partial: 'api/v1/users/common_info', as: :user
