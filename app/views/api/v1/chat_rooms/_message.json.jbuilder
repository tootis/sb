json.(message, :id, :body, :created_at)
json.chat_room message.chat_room, partial: 'chat_room_info', as: :chat_room unless @dont_include_chat_room_in_message_json
json.sender message.sender, partial: 'api/v1/users/common_info', as: :user
