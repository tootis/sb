json.has_voted current_user.voted_as_when_voted_for votable
json.votes_up votable.votes_for.up.size
json.votes_down votable.votes_for.down.size
