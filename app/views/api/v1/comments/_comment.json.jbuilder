json.(comment, :id, :created_at)
json.content comment.content.html_safe
json.author do
  json.partial! 'api/v1/users/common_info', user: comment.author
end
json.partial! 'api/v1/shared/votable', votable: comment
