json.(research_team, :id, :name, :description, :created_at)

json.specialty do
  json.partial! 'api/v1/specialties/specialty', specialty: research_team.specialty
end

json.member_count research_team.members.count

json.members research_team.members.paginate(page: 1), partial: 'api/v1/users/common_info', as: :user
