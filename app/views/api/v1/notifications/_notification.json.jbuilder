json.(notification, :id, :text, :created_at)

json.owner do
  json.partial! 'api/v1/users/common_info', user: notification.owner
end
json.seen (notification.created_at != notification.updated_at)
json.path notification.parameters[:path]
json.resource notification.parameters[:resource]
