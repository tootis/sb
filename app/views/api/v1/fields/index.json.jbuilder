json.array! @fields do |field|
  json.name field.name
  json.specialties field.specialties, :id, :name
end
