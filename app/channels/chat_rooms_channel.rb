class ChatRoomsChannel < ApplicationCable::Channel
  def subscribed
  	if current_user.currently_active_chat_rooms.where(id: params['chat_room_id']).exists?
    	stream_from "chat_rooms_#{params['chat_room_id']}_channel"
    end
  end

  def unsubscribed
  end

  def send_message(data)
      current_user.messages.create!(body: data['message'], chat_room_id: data['chat_room_id'])
  end
end