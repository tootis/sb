class Api::V1::ChatNotificationsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "chat_notifications_#{current_user.id}_channel"
  end

  def unsubscribed
  end

  def send_message(data)
  end
end
