class Api::V1::ChatRoomsChannel < ApplicationCable::Channel
  def subscribed
    chat_room = current_user.chat_rooms.find_by(id: params['chat_room_id'])
    if chat_room.present?
      stream_from chat_room_channel_name(chat_room)
    end
  end

  def unsubscribed
  end

  def send_message(data)
    chat_room = current_user.chat_rooms.find_by(id: data['chat_room_id'])
    if chat_room.present? && chat_room.messagable?
      message = chat_room.messages.new(body: data['message_body'], sender: current_user)
      if message.save
        message_json = render_message(message)
        ActionCable.server.broadcast(
          chat_room_channel_name(chat_room),
          {
            message: message_json,
            timestamp: data['timestamp']
          }
        )
        Api::V1::ChatNotificationJob.perform_later(chat_room.participants.pluck(:id), message_json)
      end
    end
  end

  private

  def chat_room_channel_name(chat_room)
    "chat_rooms_#{chat_room.id}_channel"
  end

  def render_message(message)
    Api::V1::ChatRoomsController.render(
      partial: 'message',
      locals: { message: message }
    )
  end
end
