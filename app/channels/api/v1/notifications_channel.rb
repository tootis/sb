class Api::V1::NotificationsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "notifications_#{current_user.id}_channel"
  end

  def unsubscribed
  end

  def mark_as_read(data)
    PublicActivity::Activity.find_by(
      id: data['notification_id'],
      recipient: current_user
    ).try(:touch)
  end
end
