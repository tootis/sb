module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      puts request.params
      #puts request.headers.to_h

      self.current_user = User.find_by(uid: request.params['uid'])
      #logger.add_tags 'ActionCable', current_user.email
    end

    protected

    def find_verified_user # this checks whether a user is authenticated with devise
      user = User.find_by(uid: request.params['uid'])
      if user && user.valid_token?(request.params['access-token'], request.params['client'])
        puts user
        user
      else
        puts "USER REJECTED"
        reject_unauthorized_connection
      end
    end
  end
end
