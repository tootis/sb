class Field < ApplicationRecord
  #------------------------------ Associations --------------------------------

  has_many :specialties
  accepts_nested_attributes_for :specialties

  #------------------------------- Validations --------------------------------

  validates :name, presence: true, uniqueness: { case_sensitive: false },
                    length: { minimum: 4 }
end
