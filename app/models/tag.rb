class Tag < ApplicationRecord
  #------------------------------- Validations --------------------------------

  validates :name, length: { minimum: 3 }, uniqueness: true,
                   format: /\A[\w-]+\Z/

  #------------------------------ Associations --------------------------------

  has_and_belongs_to_many :posts

  #--------------------------------- Methods ----------------------------------

  def self.sanitize_and_create_or_find(tags_string)
    tags = []

    tags_string.gsub(/[^\w-]+/, ' ').split(" ").each do |n|
      tags << find_or_create_by(name: n)
    end

    tags
  end
end
