class ResearchTeamMembership < ApplicationRecord
  include PublicActivity::Model

  enum status: {enrolled: 0 , requested: 1, invited: 2} ## changing the order of these will affect the research_teams index.html.erb.

  belongs_to :research_team
  belongs_to :user

  private

  def create_notification
    return unless self.enrolled?
  end
end
