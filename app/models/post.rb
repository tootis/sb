class Post < ApplicationRecord
  #-------------------------------- Plugins -----------------------------------

  self.inheritance_column = nil
  self.per_page = 10
  acts_as_votable
  act_as_mentioner

  attr_accessor :tags_string
  #attr_accessor :research_team_id
  attr_accessor :mentionees_ids

  #---------------------------------- Enums -----------------------------------

  enum type: [:knowledge, :question]

  #------------------------------ Associations --------------------------------

  has_and_belongs_to_many :tags

  has_many :comments, dependent: :destroy

  belongs_to :author, class_name: 'User'
  belongs_to :specialty

  belongs_to :research_team, optional: true

  #---------------------------------- Scopes ----------------------------------

  def self.default_scope
    where(deleted: false).order(created_at: :desc)
  end

  #-------------------------------- Callbacks ---------------------------------

  before_validation :set_specialty_like_author, if: -> { knowledge? && !author.sb_member? }
  before_save :add_tags, if: -> { tags_string.present? }
  after_create :process_post_mentions

  #------------------------------- Validations --------------------------------

  validates_presence_of :author, :specialty, :content, :title

  validates :specialty, inclusion: { in: Specialty.all }, if: :question?
  validates :content, length: { minimum: 15 }, allow_blank: true
  validates :title, length: { minimum: 3 }, format: /\A[\w\s\?]+\Z/,
                    allow_blank: true

  validate :check_research_team_membership, if: -> { research_team.present? }

  #--------------------------------- Methods ----------------------------------

  private

  def set_specialty_like_author
    self.specialty = author.specialty
  end

  def add_tags
    tags_objs = Tag.sanitize_and_create_or_find(tags_string)
    self.tags << tags_objs.reject { |t| self.tags.include? t }.uniq
  end

  def process_post_mentions
    if(mentionees_ids)
      NotificationMultipleBroadcastJob.perform_later("Tagged you in a Post",
      Rails.application.routes.url_helpers.post_path(self.id), mentionees_ids, self.author, Time.now.to_s)
     end
     mentionees_ids = nil
  end

  def check_research_team_membership
    errors.add(:research_team, "Can not post in this research team") unless
      self.research_team.members.include?(self.author)
  end

  def process_possible_images

        page = Nokogiri::HTML  self.content
        page.css('img').each do |img_node|
            image_style =  img_node[:style]
            if (image_style)
                width = image_style[/width:(.*?)px;/m, 1]
                height = image_style[/height:(.*?)px;/m, 1]
                img_node[:style] = "width:#{width}px;height:#{height}px;max-width:100%;max-height:100%;"
            else
                img_node[:style] = "max-width:100%;max-height:100%;"
            end

            ## Enforcing https to avoid mixed content
            image_src = img_node[:src]


            ## These commented lines will be very useful if we decide to host images on our own server
            ## They will relate Images with the posts they belong to for security reasons.
            ## If we use a third party storage, anyone with the url might see the image.

            #image_id = image_src.match /\/([^\/]+)(?=\/[^\/]+\/?\Z)/
            #pic = Ckeditor::Picture.find(image_id[1].to_i)
            #pic.update_attribute(:post_id, self.id)

            if(image_src)
               image_src.gsub! 'http://', 'https://'
               img_node[:src] = image_src
            end

          set = Nokogiri::XML::NodeSet.new(page)
          set.push(img_node)
          set.wrap("<a href = '#{img_node[:src]}'> </a>")

        end
        self.content = page
  end
end
