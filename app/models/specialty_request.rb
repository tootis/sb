class SpecialtyRequest < ApplicationRecord
  enum status: [ :pending, :accepted, :rejected ]
  belongs_to :user

  def accept
    return unless self.pending?
    self.accepted!
    field = Field.find_or_create_by(name: self.field_name)
    specialty = Specialty.find_or_create_by(field: field, name: self.specialty_name)
    user.specialty = specialty
    user.save
  end

  def reject
    return unless self.pending?
    self.rejected!
  end
end
