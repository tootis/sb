class ChatRoom < ApplicationRecord

  attr_accessor :participants_ids
#------------------------------ Associations --------------------------------

  belongs_to :creator, class_name: 'User'
  has_many :chat_room_memberships, dependent: :destroy
  has_many :participants, through: :chat_room_memberships, source: :user
  has_many :chat_room_active_memberships, dependent: :destroy
  has_many :currently_active_users, through: :chat_room_active_memberships, source: :user
  has_many :messages, dependent: :destroy

  #--------- Scopes -------------

  def self.default_scope
    order(updated_at: :desc)
  end

  #-------------------------------- Callbacks ---------------------------------

  before_validation :parse_participants_ids
  before_create :add_creator_as_participant

  #------------------------------- Validations --------------------------------

  validate :validate_creator_participants_friendship
  validate :validate_number_of_participants, if: :new_record?

  # -------------------------- Methods -------------------------------------

  def messagable?
    participants.count >= 4
  end

  def add_participant(user)
    participants << user unless participants.include? user
  end

  def remove_participant(user)
    participants.delete(user)
  end

  private

  def parse_participants_ids
    participants << User.where(id: participants_ids)
  end

  def validate_creator_participants_friendship
    errors.add(:participants, "Must be friends") if creator.friends.include?(participants)
  end

  def validate_number_of_participants
      errors.add(:participants, "Must be 3 or more") if participants.size < 3
  end

  def add_creator_as_participant
    self.participants << self.creator
  end
end
