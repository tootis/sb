class User < ApplicationRecord
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable#, :confirmable

  include DeviseTokenAuth::Concerns::User
  include PublicActivity::Model
  #-------------------------------- Plugins -----------------------------------

  acts_as_voter
  act_as_mentionee

  #------------------------------ Associations --------------------------------

  ## Followings

  has_many :followings, foreign_key: :follower_id, dependent: :destroy

  has_many :followees, through: :followings, source: :followee

  has_many :inverse_followings, foreign_key: :followee_id, dependent: :destroy,
                                class_name: 'Following'

  has_many :followers, through: :inverse_followings, source: :follower

  ## Friendships

  has_many :friendships, foreign_key: :user_id, dependent: :destroy

  has_many :added_friends, -> { where friendships: { status: :accepted } },
           through: :friendships, source: :friend

  has_many :pending_friends, -> { where friendships: { status: :pending } },
           through: :friendships, source: :friend

  has_many :inverse_friendships, foreign_key: :friend_id, dependent: :destroy,
                                 class_name: 'Friendship'

  has_many :inverse_added_friends,
           -> { where friendships: { status: :accepted } },
           through: :inverse_friendships, source: :user

  has_many :inverse_pending_friends,
           -> { where friendships: { status: :pending } },
           through: :inverse_friendships,
           source: :user

  ## EmailMessages
  has_many :sent_emails ,foreign_key: :sender_id, dependent: :destroy, class_name: 'EmailMessage'
  has_many :received_emails, foreign_key: :receiver_id, dependent: :destroy, class_name: 'EmailMessage'

  ## Posts

  has_many :posts, foreign_key: :author_id, dependent: :destroy

  ## Comments

  has_many :comments, dependent: :destroy

  ## Specialty

  belongs_to :specialty, optional: true

  ## Research Teams

  has_many :owned_research_teams, foreign_key: :creator_id,
                                  class_name: 'ResearchTeam'

  has_many :research_team_memberships, dependent: :destroy

  has_many :enrolled_research_teams,
           -> { where research_team_memberships: { status: :enrolled } },
           through: :research_team_memberships, source: :research_team

  has_many :research_teams, through: :research_team_memberships

  ## Notification Log

  has_one :notification_log, dependent: :destroy

  ## Chat Rooms

  has_many :created_chat_rooms, foreign_key: :creator_id, class_name: 'ChatRoom', dependent: :destroy
  has_many :chat_room_memberships, dependent: :destroy
  has_many :chat_rooms, through: :chat_room_memberships
  has_many :chat_room_active_memberships, dependent: :destroy
  has_many :currently_active_chat_rooms, source: :chat_room, through: :chat_room_active_memberships

  ## Messages

  has_many :messages, dependent: :destroy

  ## Specialty request

  has_one :specialty_request

  #-------------------------------- Callbacks ---------------------------------

  before_validation :generate_username, on: :create, if: -> { username.blank? }
  #before_validation :generate_description, on: :update, if: -> { description.blank? }
  before_validation :generate_title, on: :update, if: -> { title.blank? }
  #before_validation :generate_specialty, on: :update, if: -> { specialty.blank? }
  after_create :create_notification_log

  #------------------------------- Validations --------------------------------

  validates_presence_of :first_name, :last_name

  validates_presence_of :username, on: :update

  #validates_presence_of :title, on: :update

  #validates_presence_of :description, on: :update

  #validates_presence_of :specialty, on: :update

  validates :first_name, length: { minimum: 2, maximum: 20 },
                         format: /\A[a-zA-Z]+\Z/, allow_blank: true

  validates :last_name, length: { minimum: 2, maximum: 20 },
                        format: /\A[a-zA-Z]+\Z/, allow_blank: true

  validates :username, uniqueness: { case_sensitive: false },
                       length: { minimum: 3, maximum: 30 },
                       format: /\A[a-z0-9\-_]+\Z/, allow_blank: true

  validate :country_inclusion, if: -> { country.present? }

  validate :fullname_not_sb, if: -> { first_name.present? && last_name.present? && !sb_member? }

  #--------------------------------- Methods ----------------------------------

  def to_s
    email
  end

  def sb_member?
    true if email.match(/.+@sciencebeehive.com/)
  end

  ## Following

  def follow(followee)
    unless following? followee
        followees << followee
         #NotificationSingleBroadcastJob.perform_later(
         #"Followed You !", Rails.application.routes.url_helpers.user_path(self.id),
         #followee, self, Time.now.to_s)
    end
  end

  def following?(followee)
    followee.in? followees
  end

  def unfollow(followee)
    followees.delete followee if following? followee
  end

  ## Friendship

  def friends
    added_friends + inverse_added_friends
  end

  def friend_with?(friend)
    friend.in? friends
  end

  def add_friend(friend)
    return false if friend_with?(friend) || friend.in?(pending_friends)
    pending_friends << friend
    follow(friend)
    return true
  end

  def received_friend_request_from?(friend)
    return friend.in? inverse_pending_friends
  end

  def sent_friend_request_to?(friend)
    return friend.in? pending_friends
  end

  def accept_friend_request(friend)
    return unless friend.in? inverse_pending_friends
    inverse_friendships.find_by(user: friend).accepted!
    follow(friend)

    #NotificationSingleBroadcastJob.perform_later(
      #"Accepted Your Friend Request !",
      #Rails.application.routes.url_helpers.user_path(self.id),
      #friend,
      #self,
      #Time.now.to_s
    #)
  end

  def cancel_friend_request(friend)
    pending_friends.delete friend if friend.in? pending_friends
  end

  def unfriend(friend)
    added_friends.delete(friend) && inverse_added_friends.delete(friend) if
      friend.in? friends

    unfollow(friend)
  end

  def fullname
    "#{first_name} #{last_name}"
  end

  def search_display_form
    "#{self.first_name} (#{self.username})"
  end

  def has_unseen_messages_in?(chat_room)
      if(ChatRoomActiveMembership.find_by(user_id: self.id, chat_room_id: chat_room.id))
        return false
      else
        last_seen = ChatRoomMembership.find_by(user_id: self.id, chat_room_id: chat_room.id).last_seen
          if(last_seen)
           return last_seen < chat_room.updated_at
          elsif (chat_room.messages.count > 0)
            return true
          else
           return false
         end
      end
  end

  def update_last_seen (chat_room_id)
    ChatRoomMembership.find_by(user_id: self.id,
     chat_room_id: chat_room_id).update_attribute(:last_seen, DateTime.current)
  end

  def has_unseen_rooms?
    ChatRoomMembership.where(user_id: self.id).order(updated_at: :desc).pluck(:chat_room_id).each do |cr_id|
      if self.has_unseen_messages_in?(ChatRoom.find(cr_id))
        return true
      end
    end
    return false
  end

  def to_s
    "#{first_name} #{last_name}"
  end

  private

  def generate_username
    loop do
      generated_username =
        "#{first_name}_#{last_name}#{rand(1..9999)}".downcase
      next if User.exists?(username: generated_username)
      self.username = generated_username
      break
    end
  end

  def generate_title
    self.title = "Specialized in #{self.specialty.name}." if self.specialty.present?
  end

  def generate_description
    self.description = "I am #{self.fullname} from #{self.country}."
  end

  def generate_specialty
    f = Field.find_or_initialize_by(name:"Unspecified Field")
    f.save!
    s = Specialty.find_or_initialize_by(name: "Unspecified Specialty")
    s.field = f
    s.save!
    self.specialty = s
  end

  def country_inclusion
    # TODO: Localize
    errors.add(:country, 'Country does not exist') unless ISO3166::Country[country]
  end

  def fullname_not_sb
    if fullname.downcase.match(/\s*science\s*beehvive\s*/)
      errors.add(:first_name, 'Invalid name')
      errors.add(:last_name, 'Invalid name')
    end
  end

  def create_notification_log
    self.notification_log = NotificationLog.create
  end
end
