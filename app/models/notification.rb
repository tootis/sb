class Notification < ApplicationRecord
  belongs_to :notification_log
  after_create :increment_unread_count


private 
  def increment_unread_count
  	prev = self.notification_log.unread_count
  	self.notification_log.update_attribute(:unread_count , prev+1)
  end
end
