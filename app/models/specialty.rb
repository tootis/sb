class Specialty < ApplicationRecord
  #------------------------------ Associations --------------------------------

  has_many :users

  belongs_to :field

  #------------------------------- Validations --------------------------------

  validates :name, presence: true, uniqueness: { case_sensitive: false },
                   length: { minimum: 4 }

  #--------------------------------- Methods ----------------------------------

  def to_s
    n = "#{name}".humanize
  end

  def aggregate_with_field
    n = "#{name}".humanize
    "#{n} (#{field.name})"
  end
end
