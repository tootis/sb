class Message < ApplicationRecord
  #------------------------------ Associations --------------------------------

  belongs_to :user
  alias_attribute :sender, :user
  belongs_to :chat_room

  #--------------------------------- Scopes ----------------------------------

  def self.default_scope
    order(created_at: :desc)
  end

  #------------------------------ Validations --------------------------------

  validates :body, presence: true, length: { minimum: 1 }

  #------------------------------- Callbacks ---------------------------------

  after_create_commit { self.chat_room.touch }
end
