class Comment < ApplicationRecord
  include PublicActivity::Model

  #-------------------------------- Plugins -----------------------------------
  self.per_page = 10
	acts_as_votable
  act_as_mentioner

  # Accessors

  attr_accessor :mentionees_ids

  #------------------------------ CallBacks --------------------------------

  after_create :process_comment_mentions

	#after_create_commit {
    #NotificationSingleBroadcastJob.perform_later(
      #"commented on your post ",
      #Rails.application.routes.url_helpers.post_path(self.post.id),
      #self.post.author,
      #self.user,
      #Time.now.to_s
    #)
  #}

  #------------------------------ Associations --------------------------------

  belongs_to :post
  belongs_to :user
  alias_attribute :author, :user

  #---------------------------------- Scopes ----------------------------------

  def self.default_scope
    where(deleted: false).order(created_at: :desc)
  end

  #------------------------------- Validations --------------------------------

  validates_presence_of :content, :user, :post

  validates :content, length: { minimum: 1 }, allow_blank: true

  private

  def process_comment_mentions
    if(mentionees_ids)
      NotificationMultipleBroadcastJob.perform_later(
        "Mentioned you in a comment",
        Rails.application.routes.url_helpers.post_path(self.post.id),
        mentionees_ids,
        self.user,
        Time.now.to_s
      )
     end

     mentionees_ids = nil
  end
end

