class ResearchTeam < ApplicationRecord

  enum privacy: { visible: 0, secret: 1 } # visible must be 0 and secret must be 1. changing this order will destroy some postgresql queries.
  alias_attribute :deleted, :soft_deleted

  #------------------------------ Associations --------------------------------

  has_many :posts, dependent: :destroy

  has_many :research_team_memberships, dependent: :destroy

  has_many :members, through: :research_team_memberships, source: :user

  has_many :enrolled_members, -> { where research_team_memberships: { status: :enrolled } },
           through: :research_team_memberships, source: :user

  has_many :invited_members, -> { where research_team_memberships: { status: :invited } },
           through: :research_team_memberships, source: :user

  has_many :requesting_members, -> { where research_team_memberships: { status: :requested } },
           through: :research_team_memberships, source: :user

  has_many :admins, -> { where research_team_memberships: { admin: true } },
           through: :research_team_memberships, source: :user

  belongs_to :creator, class_name: 'User'

  belongs_to :specialty

  #---------------------------------- Scopes ----------------------------------

  def self.default_scope
    where(deleted: false)
  end

  #------------------------------- Validations --------------------------------

  validates_presence_of :creator, :specialty, :description, :name

  validates :specialty, inclusion: { in: Specialty.all }
  validates :description, length: { minimum: 15 }, allow_blank: true
  validates :name, length: { minimum: 3 }, format: /\A[\-\w\s\?]+\Z/,
                    allow_blank: true

  #-------------------------------- Callbacks ---------------------------------

  after_create :add_creator_as_admin_member

  private

  def add_creator_as_admin_member
    self.admins << self.creator
  end
end
