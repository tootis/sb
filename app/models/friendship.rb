class Friendship < ApplicationRecord
  # Enum
  enum status: [:pending , :accepted]

  #------------------------------ Associations --------------------------------

  belongs_to :user
  belongs_to :friend, class_name: 'User'

  #------------------------------- Validations --------------------------------

  validates_presence_of :user, :friend

  validates_uniqueness_of :friend_id, scope: :user_id

  validate :user_and_friend_cant_be_same
  validate :uniqueness_the_other_way

  #--------------------------------- Methods ----------------------------------

  private

  def user_and_friend_cant_be_same
    return if user.blank? || friend.blank?
    errors.add :base, :user_and_friend_same if friend == user
  end

  def uniqueness_the_other_way
    return if user.blank? || friend.blank?
    errors.add :base, :already_friends if
      Friendship.exists?(user: friend, friend: user)
  end
end
