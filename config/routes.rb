Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  # API routes

  mount ActionCable.server => '/cable'
  devise_for :users
  namespace :api, defaults: { format: :json } do
    namespace :v1 do

      mount_devise_token_auth_for 'User', at: 'auth'

      concern :votable do
        member do
          post :toggle_upvote
          post :toggle_downvote
        end
      end

      resources :countries, only: [:index]
      resources :specialties, only: [:index]
      resources :specialty_requests, only: [:create]
      resources :fields, only: [:index]
      resources :notifications, only: [:index]

      resources :users, only: [:show] do
        resources :posts, only: [:index]

        collection do
          get :search
        end

        member do
          post :add_friend
          post :follow

          put :accept_friend_request

          delete :cancel_friend_request
          delete :unfriend
          delete :unfollow
        end
      end

      resources :posts, only: [:create, :show, :update, :destroy], concerns: :votable do
        resources :comments, only: [:index, :create]

        collection do
          get 'global_beehive'
          get 'local_beehive'
          get 'specialty_beehive'
          get 'my_beehive'
        end
      end

      resources :comments, only: [:update, :destroy], concerns: :votable

      resources :chat_rooms, only: [:index, :create, :show, :update, :destroy] do
        member do
          post :participants, to: 'chat_rooms#add_participant'

          delete :participants, to: 'chat_rooms#remove_participant'
          delete :leave
        end
      end

      resources :research_teams, only: [:index, :show, :create, :update, :destroy] do
        post :posts, to: 'posts#create_post_for_research_team'

        collection do
          get :search
        end

        member do
          get :posts
          get :members, to: 'research_teams#list_members'

          post :members, to: 'research_teams#add_member'
          post :join

          delete :members, to: 'research_teams#remove_member'
          delete :leave
        end
      end
    end
  end

  scope constraints: -> (request) { request.format != :json } do
    # this matches ALL paths to bootstrapper action
    root to: 'react#bootstrapper'
    get '*path', to: 'react#bootstrapper'
  end
end
