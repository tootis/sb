Rails.application.routes.draw do
  root to: 'landing#index'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  mount Ckeditor::Engine => '/ckeditor'

  mount ActionCable.server => '/cable'

  concern :votable do
    member do
      post :toggle_upvote
      post :toggle_downvote
    end
  end

  get 'home(/:beehive)', to: 'home#index'

  devise_for :users

  get 'users/relationships', to: 'users#relationships'
  get 'users/autocomplete_user_username'
  get 'users/show', to: 'users#show'

  resources :users, only: [:show] do
    member do
      %i(add_friend unfriend cancel_friend_request
         accept_friend_request follow unfollow).each do |m|
        get m
      end
      post :request_specialty
    end

    collection do
      get :active_chat_rooms
      get :remove_inactive_chat_room
      get :get_mentionees
      get :increment_no_unseen_rooms
      get :get_user_info
    end
  end

  resources :users do
     get :autocomplete_user_username, :on => :collection
  end


  resources :posts, only: [:create, :show], concerns: :votable do
    resources :comments, only: [:create, :destroy] do
      collection do
        get 'paginate'
      end
    end
  end

  resources :email_messages, only: [:create]


  resources :comments, only: [], concerns: :votable
  resources :research_teams do
    member do
      get 'available_friends'
      post 'add_friend'
      get 'team_members'
      post 'accept_member'
      post 'cancel_join_request'
      post 'accept_invitation'
      post 'request_membership'
      get 'show_requesting_members'
      post 'invite_friend'
      post 'remove_member'
      post 'cancel_invitation'
    end
  end

  resources :notification_logs, only: [:show] do
     get :clear_unread_count, :on => :collection
  end

  resources :chat_rooms, only: [:new, :create, :show, :index] do
    collection do
      get :render_new_room
    end
  end

  # API routes

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      mount_devise_token_auth_for 'User', at: 'auth'

      resources :countries, only: [:index]
      resources :specialties, only: [:index]
      resources :fields, only: [:index]
      resources :notifications, only: [:index]

      resources :users, only: [:show] do
        resources :posts, only: [:index]

        collection do
          get :search
        end

        member do
          post :add_friend
          post :follow

          put :accept_friend_request

          delete :cancel_friend_request
          delete :unfriend
          delete :unfollow
        end
      end

      resources :posts, only: [:create, :show, :update, :destroy], concerns: :votable do
        resources :comments, only: [:index, :create]

        collection do
          get 'global_beehive'
          get 'local_beehive'
          get 'specialty_beehive'
          get 'my_beehive'
        end
      end

      resources :comments, only: [:update, :destroy], concerns: :votable

      resources :research_teams, only: [:index, :show, :create, :update, :destroy] do
        post :posts, to: 'posts#create_post_for_research_team'

        collection do
          get :search
        end

        member do
          get :posts
          get :members, to: 'research_teams#list_members'

          post :members, to: 'research_teams#add_member'
          post :join

          delete :members, to: 'research_teams#remove_member'
          delete :leave
        end
      end
    end
  end
end
