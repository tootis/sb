require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)


module Sb
  class Application < Rails::Application
    config.generators do |g|
      g.template_engine = :haml
    end

    config.autoload_paths << Rails.root.join('lib')

    #config.assets.precompile += Ckeditor.assets
    #config.assets.precompile += %w( ckeditor/* )
    #config.autoload_paths += %W(#{config.root}/app/models/ckeditor)

    config.active_job.queue_adapter = :sidekiq

    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :put, :delete, :options], expose: ["client", "access-token", "uid"]
      end
    end
  end
end
