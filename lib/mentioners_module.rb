class CustomMentionProcessor < MentionSystem::MentionProcessor

  def extract_mentioner_content(mentioner)
    mentioner.content
  end

  def find_mentionees_by_handles(*handles)
    User.where(username: handles)
  end
end

module MentionersModule
  def handle_mentions(mentioner, rt = nil)
    all_mentioned_users = CustomMentionProcessor.new.process_mentions(mentioner)
    valid_mentioned_users = []
    if(all_mentioned_users)
      if(rt)
        all_mentioned_users.each do|mentioned|
          if (rt.members.include? mentioned)
            valid_mentioned_users << mentioned
          end
        end
      else
        all_mentioned_users.each do|mentioned|
          if (mentioner.author.friends.include? mentioned)
            valid_mentioned_users << mentioned
          end
        end
      end
      mentioner.mentionees_ids = valid_mentioned_users.pluck(:id)
      valid_mentioned_users.each do |user|
        mentioner.content.sub!(/@#{user.username}/,view_context.link_to( "@#{user.fullname}",  user_path(user.id)))
      end
    end
  end
end
