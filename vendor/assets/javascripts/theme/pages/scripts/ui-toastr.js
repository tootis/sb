toastr.options = {
  "closeButton": true,
  "debug": false,
  "positionClass": "toast-bottom-left",
  "onclick": null,
  "showDuration": "1000",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

toastr.alert = function(message, title, optionsOverride) {
  toastr.error(message, title, optionsOverride);
}

toastr.notice = function(message, title, optionsOverride) {
  toastr.info(message, title, optionsOverride);
}
