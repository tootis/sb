class CreateEmailMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :email_messages do |t|
      t.belongs_to :sender
      t.belongs_to :receiver
      t.string :title, null: false, default: ''
      t.text :content, null: false
      t.timestamps
    end
  end
end
