class RemoveLimitOfPostContent < ActiveRecord::Migration[5.0]
  def up
  	change_column :posts, :content, :text, :limit => 1000000000
  end

  def down
  	change_column :posts, :content, :text, :limit => 65535
  end

end
