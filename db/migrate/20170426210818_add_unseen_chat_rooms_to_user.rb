class AddUnseenChatRoomsToUser < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :unseen_chat_rooms, :integer , default:0
  	User.all.each do |u|
  		u.update_attribute(:unseen_chat_rooms,0)
  	end
  end
end
