class CreateResearchGroupsUsersJoinTable < ActiveRecord::Migration[5.0]
  def change
  	create_join_table :research_groups, :users
  end
end
