class AddResearchGroupNameColumn < ActiveRecord::Migration[5.0]
  def change
  	add_column :research_groups , :name , :string , :null =>false , :uniq =>true
  	add_index  :research_groups, :name
  end
end
