class AddNoUnseenMessagesToUserChatRoomsMembership < ActiveRecord::Migration[5.0]
  def change
  	add_column :chat_room_memberships , :last_seen , :timestamp
  	ChatRoomMembership.all.each do |crm|
  		crm.update_attribute(:last_seen, DateTime.current)
  	end
  end
end
