class AddSoftDeletedAttributeToResearchTeam < ActiveRecord::Migration[5.0]
  def change
  	add_column :research_teams, :soft_deleted, :boolean, :default => false

if(ResearchTeam.all.size > 0)
  	ResearchTeam.all.each do|research_team|
  		research_team.soft_deleted = false
  	end
  end
  end
end
