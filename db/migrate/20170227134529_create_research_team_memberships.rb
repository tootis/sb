class CreateResearchTeamMemberships < ActiveRecord::Migration[5.0]
  def change
    create_table :research_team_memberships do |t|
      t.belongs_to :research_team, foreign_key: true
      t.belongs_to :user, foreign_key: true
      t.boolean :admin, default: false

      t.timestamps
    end
  end
end
