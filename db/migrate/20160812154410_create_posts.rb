class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.belongs_to :author, index: true
      t.belongs_to :specialty, foreign_key: true
      t.string :title, null: false, default: ''
      t.text :content, null: false
      t.integer :type, default: 0
      t.boolean :deleted, default: false
      t.timestamps
    end
  end
end
