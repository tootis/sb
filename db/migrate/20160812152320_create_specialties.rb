class CreateSpecialties < ActiveRecord::Migration[5.0]
  def change
    create_table :specialties do |t|
      t.string :name
      t.belongs_to :field, foreign_key: true

      t.timestamps
    end
  end
end
