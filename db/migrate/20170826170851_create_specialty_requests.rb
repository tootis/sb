class CreateSpecialtyRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :specialty_requests do |t|
      t.belongs_to :user, foreign_key: true
      t.string :field_name
      t.string :specialty_name

      t.timestamps
    end
  end
end
