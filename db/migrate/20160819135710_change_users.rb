class ChangeUsers < ActiveRecord::Migration[5.0]
  def change
    change_table :users do |t|
      t.belongs_to :specialty, foreign_key: true
      t.string :country, index: true
    end
  end
end
