class AddResearchTeamIdToPosts < ActiveRecord::Migration[5.0]
  def change
    change_table :posts do |t|
      t.belongs_to :research_team
    end
  end
end
