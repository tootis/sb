class AddUniqueIndexToChatRoomActiveMembership < ActiveRecord::Migration[5.0]
  def change
  	add_index :chat_room_active_memberships, [:user_id, :chat_room_id], unique: true
  end
end
