class UpdateFullnameColumnOnPreviousUsers < ActiveRecord::Migration[5.0]
  def change
  	if(User.all.size >0)
	  	User.all.each do |user|
	  		user.update_attribute :fullname, "#{user.first_name} #{user.last_name}"
  	end
  end
  end
end
