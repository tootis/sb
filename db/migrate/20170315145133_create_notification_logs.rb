class CreateNotificationLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :notification_logs do |t|
      t.references :user, foreign_key: true
      t.timestamps
    end
    if(User.all.size > 0)
      User.all.each do |user|
        if(!user.notification_log)
      		user.notification_log = NotificationLog.create
      	end
    end
    end
  end
end
