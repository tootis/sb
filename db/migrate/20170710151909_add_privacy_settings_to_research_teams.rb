class AddPrivacySettingsToResearchTeams < ActiveRecord::Migration[5.0]
  def change
  	add_column :research_teams, :privacy, :integer ,default: 0 
  	 ResearchTeam.all.each do |rt|
  	 	rt.update_attribute :privacy,0
  	 end
  end
end
