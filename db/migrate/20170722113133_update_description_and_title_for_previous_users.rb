class UpdateDescriptionAndTitleForPreviousUsers < ActiveRecord::Migration[5.0]
  def change

  	User.find_each do |user| #This is for performance reasons.

  		if user.title.blank?
  			user.update_attribute(:title, "Specialized in #{user.specialty.name}.") 
  		end
  		if user.description.blank?
  			user.update_attribute(:description, "I am #{user.fullname} from #{user.country}. I am specialized in #{user.specialty.name}, in the general field of #{user.specialty.field.name}.") 
  		end

	end
  end
end
