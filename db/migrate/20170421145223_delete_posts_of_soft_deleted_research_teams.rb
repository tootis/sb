class DeletePostsOfSoftDeletedResearchTeams < ActiveRecord::Migration[5.0]
  def change
  	if(ResearchTeam.all.size > 0)
  	ResearchTeam.all.each do|rt|
  		if(rt.soft_deleted)
  			rt.posts.each do|post|
  				post.update_attribute(:deleted, true)
  			end
  		end
  	end
  	end
  end
end
