class AddTitleToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :title, :string
    #User.in_batches.update_all(title: ' NO TITLE YET ') # leave this for reference only
    User.find_each do |user| #This is for performance reasons.
    	if user.specialty.blank?
    		#user.touch touch doesnot call vaidations. I need this because I added  a before validation for specialty.
    		    f = Field.find_or_initialize_by(name:"Unspecified Field")
			    f.save!
			    s = Specialty.find_or_initialize_by(name: "Unspecified Specialty")
			    s.field = f
			    s.save!
				user.update_attribute(:specialty, s)
    	end
  		user.update_attribute(:title, "Specialized in #{user.specialty.name}")
	end
  end
end
