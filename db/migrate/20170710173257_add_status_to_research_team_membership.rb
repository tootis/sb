class AddStatusToResearchTeamMembership < ActiveRecord::Migration[5.0]
  def change
  	add_column :research_team_memberships, :status, :integer , default:0
  	ResearchTeamMembership.all.each do |rtm|
  		rtm.update_attribute :status,0
  	end
  end
end
