class AddReferenceFromPostToResearchGroup < ActiveRecord::Migration[5.0]
  def change
  	add_column(:posts,:research_group_id, :integer, :unique=>true, foreign_key: true)
  	add_index :posts, :research_group_id
  end
end
