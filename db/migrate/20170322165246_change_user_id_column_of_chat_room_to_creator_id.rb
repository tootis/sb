class ChangeUserIdColumnOfChatRoomToCreatorId < ActiveRecord::Migration[5.0]
  def change
  	rename_column :chat_rooms, :user_id, :creator_id
  end

end
