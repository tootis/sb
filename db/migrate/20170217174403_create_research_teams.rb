class CreateResearchTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :research_teams do |t|
      t.belongs_to :creator
      t.belongs_to :specialty
      t.string :name, null: false, default: ""
      t.text :description, null: false, defualt: ""
      t.timestamps
    end
  end
end
