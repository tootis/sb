class AddUniqueIndexToResearchGroupUsers < ActiveRecord::Migration[5.0]
  def change
  	  	add_index :research_groups_users, ["research_group_id", "user_id"], :unique => true
  end
end
