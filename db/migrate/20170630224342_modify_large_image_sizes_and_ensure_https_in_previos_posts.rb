class ModifyLargeImageSizesAndEnsureHttpsInPreviosPosts < ActiveRecord::Migration[5.0]
  def up  		
  		Post.all.each do |mePost|
  			page = Nokogiri::HTML  mePost.content
	        page.css('img').each do |img_node|
	            image_style =  img_node[:style]
	            if (image_style)
	                width = image_style[/width:(.*?)px;/m, 1]
	                height = image_style[/height:(.*?)px;/m, 1]
	                img_node[:style] = "width:#{width}px;height:#{height}px;max-width:100%;max-height:100%;"
	            else
	                img_node[:style] = "max-width:100%;max-height:100%;"
	            end

		        ## Enforcing https to avoid mixed content
	            image_src = img_node[:src]
	            if(image_src)
	               image_src.gsub! 'http://', 'https://'
	               img_node[:src] = image_src
	            end
		        mePost.update_attribute(:content,page)    
		        
	        end

  		end
  end

  def down

  end
end
