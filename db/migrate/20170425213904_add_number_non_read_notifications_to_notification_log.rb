class AddNumberNonReadNotificationsToNotificationLog < ActiveRecord::Migration[5.0]
  def change
  	add_column :notification_logs , :unread_count , :integer , default:0

  	if(NotificationLog.all.size > 0)
  	NotificationLog.all.each do |nl|
  		nl.unread_count = 0
  	end
  end
  end
end
