FactoryBot.define do
  factory :research_team_membership do
    research_team nil
    user nil
    admin false
  end
end
