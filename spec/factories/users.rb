FactoryBot.define do
  factory :user do
    sequence(:id) { |i| i }
    sequence(:email) { |i| "tester#{i}@example.com" }
    first_name 'John'
    last_name 'Isner'
    sequence(:username) { |i| "john_doe#{i}" }
    password 'SectretPasswordHueHue'
  end
end
