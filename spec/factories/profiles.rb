FactoryBot.define do
  factory :profile do
    user nil
    date_of_birth "2016-08-12"
    specialty nil
    country "MyString"
  end
end
