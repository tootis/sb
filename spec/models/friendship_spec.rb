require 'rails_helper'

RSpec.describe Friendship, type: :model do
  let(:user) { create :user }
  let(:friend) { create :user }
  let!(:friendship) { build :friendship, user: user, friend: friend }
  subject { friendship }

  context 'Association' do
    it { is_expected.to belong_to :user }
    it { is_expected.to belong_to :friend }
  end

  context 'Validation' do
    it { is_expected.to validate_presence_of :user }
    it { is_expected.to validate_presence_of :friend }

    it { is_expected.to validate_uniqueness_of(:friend_id).scoped_to(:user_id) }

    it 'Must have different user and friend' do
      subject.friend = subject.user
      expect(subject).not_to be_valid
      expect(subject.errors[:base]).to contain_exactly(
        I18n.t('activerecord.errors.models.friendship.user_and_friend_same')
      )
    end

    it 'Must be unique relationship from the other way too' do
      create :friendship, user: friend, friend: user
      expect(subject).not_to be_valid
      expect(subject.errors[:base]).to contain_exactly(
        I18n.t('activerecord.errors.models.friendship.already_friends')
      )
    end
  end
end
