require 'rails_helper'

RSpec.describe Following, type: :model do
  let(:follower) { create :user }
  let(:followee) { create :user }
  let!(:following) { build :following, followee: followee, follower: follower }
  subject { following }

  context 'Association' do
    it { is_expected.to belong_to :follower }
    it { is_expected.to belong_to :followee }
  end

  context 'Validations' do
    it { is_expected.to validate_presence_of :follower }
    it { is_expected.to validate_presence_of :followee }

    it 'Must be unique scpoed to follower' do
      subject.save
      f = build :following, followee: followee, follower: follower
      expect(f).not_to be_valid
    end

    it 'Must allow the other way of following ' do
      subject.save
      f = build :following, followee: follower, follower: followee
      expect(f).to be_valid
    end
  end
end
