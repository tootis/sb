require 'rails_helper'

RSpec.describe User, type: :model do
  it { expect(build(:user)).to be_valid }

  let!(:user) { build(:user) }
  subject { user }

  context 'Associations' do
    it { is_expected.to have_many :followers }
    it { is_expected.to have_many :followees }

    it { is_expected.to have_many :added_friends }
    it { is_expected.to have_many :inverse_added_friends }
    it { is_expected.to have_many :pending_friends }
    it { is_expected.to have_many :inverse_pending_friends }
  end

  context 'Validations' do
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:username).on(:update) }
    it { is_expected.to validate_presence_of(:password) }

    it { is_expected.to validate_uniqueness_of(:username) }

    it do
      is_expected.to validate_length_of(:username).is_at_least(3).is_at_most(30)
    end

    it do
      is_expected.to validate_length_of(
        :first_name
      ).is_at_least(2).is_at_most(20)
    end

    it do
      is_expected.to validate_length_of(
        :last_name
      ).is_at_least(2).is_at_most(20)
    end

    '@.$*&()\/:*?"<>|#'.split('').each do |sym|
      it { is_expected.not_to allow_value("john#{sym}doe").for(:username) }
    end

    it { is_expected.to allow_value('john_doe').for(:username) }
    it { is_expected.to allow_value('john-doe').for(:username) }
  end

  context 'Callbacks' do
    it 'Must generate_username if there is none before validation' do
      subject.username = nil
      expect(subject).to receive(:generate_username)
      subject.valid?
    end

    it 'Must generate username username if generate_username was called' do
      subject.username = nil
      expect { subject.send(:generate_username) }.to change {
        subject.username
      }.from(nil).to(
        /#{subject.first_name.downcase}_#{subject.last_name.downcase}\d{1,4}/
      )
    end
  end

  context 'Followings' do
    it { is_expected.to respond_to :followees }
    it { is_expected.to respond_to :followers }

    context 'Following other user' do
      let(:followee) { create(:user) }

      before do
        subject.save
        create :following, follower: subject, followee: followee
      end

      it { expect(subject.followees).to contain_exactly followee }
      it { expect(subject.followers).to be_empty }
      it { expect(followee.followers).to contain_exactly subject }
      it { expect(followee.followees).to be_empty }

      context 'Following methods' do
        let(:other_followee) { create :user }

        it { is_expected.to respond_to :follow }
        it { is_expected.to respond_to :following? }
        it { is_expected.to respond_to :unfollow }

        it 'Must add a followee when calling follow' do
          expect { subject.follow other_followee }.to change {
            subject.followees.count
          }.from(1).to(2)
        end

        it 'Must refuse duplicates if calling follow with existing followee' do
          expect { subject.follow followee }.not_to change {
            subject.followees.to_a
          }.from [followee]
        end

        it 'Must remove followee from list if unfollow was called with it' do
          expect { subject.unfollow followee }.to change {
            subject.followees.to_a
          }.from([followee]).to []
        end

        it 'Must do nothing if unfollow called with non existing followee' do
          expect { subject.unfollow other_followee }.not_to change {
            subject.followees.to_a
          }.from [followee]
        end
      end
    end
  end

  context 'Friendship' do
    let(:accpted_friend) { create :user }
    let(:pending_friend) { create :user }

    before do
      subject.save
      create :friendship, user: subject, friend: accpted_friend,
                          status: :accepted
      create :friendship, user: subject, friend: pending_friend,
                          status: :pending
    end

    it { expect(subject.added_friends).to contain_exactly accpted_friend }

    it do
      expect(accpted_friend.inverse_added_friends).to contain_exactly subject
    end

    it { expect(subject.pending_friends).to contain_exactly pending_friend }

    it do
      expect(pending_friend.inverse_pending_friends).to contain_exactly subject
    end

    context 'Friendship methods' do
      it { is_expected.to respond_to :friends }
      it { is_expected.to respond_to :add_friend }
      it { is_expected.to respond_to :accept_friend }
      it { is_expected.to respond_to :unfriend }
      it { is_expected.to respond_to :cancel_friend_request }

      it 'Must return all friends when calling #friends' do
        u = create :user
        create :friendship, user: u, friend: subject, status: :accepted
        expect(subject.friends).to contain_exactly(accpted_friend, u)
      end

      it 'Must add a pending friend when calling #add_friend' do
        u = create :user
        expect { subject.add_friend u }.to change {
          subject.pending_friends.to_a
        }.from([pending_friend]).to [pending_friend, u]
      end

      it 'Must not duplicate friends when calling #add_friend' do
        expect { subject.add_friend pending_friend }.not_to change {
          subject.pending_friends.to_a
        }.from [pending_friend]
      end

      it 'Must add friend to accpted_friends when calling #accept_friend' do
        expect do
          pending_friend.accept_friend subject
          subject.added_friends.reload
        end.to change {
          subject.added_friends.to_a
        }.from([accpted_friend]).to [accpted_friend, pending_friend]
      end

      it 'Must remove friend to pending_friends when calling #accept_friend' do
        expect do
          pending_friend.accept_friend subject
          subject.pending_friends.reload
        end.to change {
          subject.pending_friends.to_a
        }.from([pending_friend]).to []
      end

      it 'Must remove accepted friend when calling #unfriend on it' do
        expect { subject.unfriend accpted_friend }.to change {
          subject.added_friends.to_a
        }.from([accpted_friend]).to []
      end

      it 'Must remove pending friend when calling #cancel_friend_request on it' do
        expect { subject.cancel_friend_request pending_friend }.to change {
          subject.pending_friends.to_a
        }.from([pending_friend]).to []
      end
    end
  end
end
